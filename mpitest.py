from __future__ import print_function
import imp

if __name__ == "__main__":
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    nprocs = comm.Get_size()
    proc_id = comm.Get_rank()
    import argparse
    import os
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('-lammpsthesis', dest='lth',nargs='*')
    parser.add_argument('-lammps', dest='l',nargs='*')
    parser.add_argument('-debug', dest='debug', action='store_true', default=False)
    arglist = parser.parse_args()
    if arglist.lth is not [] and arglist.lth is not None:
        localpath = os.path.dirname(os.path.realpath(__file__))+'/lammps/python'
        if localpath not in sys.path:
            sys.path.append(localpath)
        lammps = imp.load_source('lammps',str(localpath)+'/lammps.py')
        for thing in arglist.lth:
            if arglist.debug is True:
                print('Loading ' + str(thing) + ' from ' + str(localpath) + '/lammps.py')
            #try:
            lmp_mpi = lammps.lammps(thing,output=False)
            lmp_mpi.command('variable test equal 1')
            lmp_mpi.command('print ${test}')
            lmp_mpi.close()
            #except:
            #    pass
    if arglist.l is not [] and arglist.l is not None:
        from lammps import lammps
        for thing in arglist.l:
            try:
                lmp_mpi = lammps(thing)
                lmp_mpi.command('variable test equal 1')
                lmp_mpi.command('print ${test}')
                lmp_mpi.close()
            except:
                pass
