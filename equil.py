#! /usr/bin/env python
from __future__ import print_function
# Import custom LAMMPS distribution 
import sys
import os
import imp
# Add python file and libraries to path in case this wasn't done by install
"""try:
    os.environ['LD_LIBRARY_PATH'] += (':' + os.path.dirname((os.path.realpath(sys.argv[0]))) + '/lammps/src/')
except KeyError:
    os.environ['LD_LIBRARY_PATH'] = os.path.dirname((os.path.realpath(sys.argv[0]))) + '/lammps/src/'
    
    try:
        os.environ['PYTHONPATH'] += (':' + os.path.dirname(os.path.realpath(sys.argv[0])) + '/lammps/python/')
    except KeyError:
        os.environ['PYTHONPATH'] = os.path.dirname(os.path.realpath(sys.argv[0])) + '/lammps/python/'"""

sys.path.append(os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/')
# print(os.environ['LD_LIBRARY_PATH'])
# print (sys.path)

try:
    from mpi4py import MPI

    # An object which communicates between processes (comm), the total number of process and this process id

    comm = MPI.COMM_WORLD
    nprocs = comm.Get_size()
    proc_id = comm.Get_rank()
    # We don't nedd MPI stuff if we only run on 1 process anyway

except ImportError:
    print ('Script running on 1 processor as mpi4py module could not be found/loaded')

    # If we don't have any MPI stuff going on, set our process id to 0

    comm = None
    nprocs = 1
    proc_id = 0

try:
    lammps = imp.load_source('lammps',os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/lammps.py')
    if proc_id == 0:
        print ('Loading Downloaded LAMMPS Version')
# Or use an existing one 
except:
    import lammps
    if proc_id == 0:
        print ('Loading Pre-Existing LAMMPS Version')

from matplotlib import pyplot
import numpy as np

import argparse
import time
import utility

# For parallel runs make use of python MPI capabilities. While the non-LAMMPS parts of the script are not parallelised
# in any way, any LAMMPS command must be executed simultaneously by all processes.

# Read in things from the command line

if proc_id == 0:
    parser = argparse.ArgumentParser('Equilibrate a LAMMPS simulation')
else:
    parser = argparse.ArgumentParser('Equilibrate a LAMMPS simulation', add_help=False)
    parser.add_argument('-h', nargs=0, action=utility.QuitArgparse)
    parser.add_argument('--help', nargs=0, action=utility.QuitArgparse)

parser.add_argument('-rg', dest='rg', action=utility.GraphAction, help='Display a graph showing Radius of Gyration')
parser.add_argument('-in', dest='input', type=str, help='The name of the input atom datafile',
                    default=os.path.dirname(os.path.realpath(__file__)) + '/Equil/equil.initial')
parser.add_argument('-t', dest='t', action=utility.GraphAction, help='Display a graph showing Temperature')
parser.add_argument('-d', dest='dumpfile', nargs=2, type=str, help='Filename of dumpfile for simulation and'
                    'output frequency. Use: -d dumpfile.location 10000', default=None)
parser.add_argument('-show', dest='show_dumpfile', action='store_true', help='Show the dumpfile trajectory in vmd')
parser.add_argument('-e', dest='energies', action=utility.GraphAction, help='Display a graph of average '
                    'bond and angle energies')
parser.add_argument('-s', dest='steps', nargs=2, type=int, help='Run for arg1 * arg2 timesteps. Default setting is 100 '
                                                                'steps of size 10000', default=[100, 10000])
parser.add_argument('-a', dest='angle_coeff', type=float, help='Set the coefficient for the cosine angle potential.'
                    'The persistence length is angle_coeff * sigma.', default=20.0)
parser.add_argument('-ainit', dest='angle_init', type=float, help='Set the initial value coefficient for the cosine angle potential.'
                    'This will be gradually reduced to the value for -a during pre-equilibration', default=20.0)
parser.add_argument('-lib', dest='lammps_library', type=str, help='Loads library named liblammps_{lib}.so',
                    default=None)
parser.add_argument('-eqsoft', dest='pre_equil_time', type=int, help='Runs a pre equilibration with soft atomic potential and harmonic bonds',default=0)
args = parser.parse_args()

# print (args)

if nprocs == 1:
    comm = None


# Send our initial conditions to any other processes

if comm is not None:
    args = comm.bcast(args, 0)

# Create a pointer to our LAMMPS instance

if args.lammps_library is None and nprocs == 1:
    print ('Loaded default LAMMPS library')
    lmp = lammps.lammps()
    
elif args.lammps_library is None and nprocs != 1:
    print ('Loaded default multiprocess LAMMPS library')
    lmp = lammps.lammps('g++_openmpi')
    
else:
    lmp = lammps.lammps(args.lammps_library)
    if proc_id == 0:
        print('Loaded library: '+str(args.lammps_library))

# Create a random seed number for use in equil.lmp, and save it as the LAMMPS variable ${seed}. As with our initial
# conditions, do this on process 0 and send it to the others. This also makes sure all processes have the same seed!

if proc_id == 0:
    seed_no = np.random.randint(0, 10000000)
else:
    seed_no = None

if comm is not None:
    seed_no = comm.bcast(seed_no, 0)

lmp.command('variable seed equal {0:d}'.format(seed_no))

# Create a LAMMPS variable pointing to our input data file

if not os.path.isfile(args.input) and proc_id == 0:
    raise IOError('Input file not found at {0:s}'.format(args.input))
lmp.command('variable inputfile string {0:s}'.format(args.input))

# Create a lammps variable with the coefficient for the angle potential

lmp.command('variable anglecoeff equal {0:f}'.format(args.angle_coeff))
lmp.command('variable eqsoft equal {}'.format(args.pre_equil_time))

# Run a script with basic settings for lammps

lmp.file(os.path.dirname(os.path.realpath(__file__)) + '/Equil/equil.lmp')

# Reduce the angle coeff in steps until the target value is reached
if abs(args.angle_init - args.angle_coeff) > 0.0001:

    astep = (- args.angle_init + args.angle_coeff)/20.0

    for angle in np.arange(args.angle_init,args.angle_coeff,astep):
        lmp.command('variable anglecoeff equal {0:f}'.format(angle))
        lmp.command('run ${eqsoft}')

# Write to a output dumpfile every n timesteps

if args.dumpfile is not None:
    lmp.command('dump 1 all custom {0:d} {1:s} id type mol x y z ix iy iz'.format(int(args.dumpfile[1]),
                                                                                  args.dumpfile[0]))

# Create lists to store data for graphs, and set up computes to calculate the data

rg_data = []
t_data = []
energy_data = []

if args.rg.calculate is True:
    lmp.command('compute rg all gyration')
if args.t.calculate is True:
    lmp.command('compute temp all temp')
if args.energies.calculate is True:
    lmp.command('compute pair all pair lj/cut')
    lmp.command('compute bond all bond/local engpot')
    lmp.command('compute angle all angle/local eng')

natoms = lmp.get_natoms()
lmp.command('run 0')

for step in range(args.steps[0]):

    # Get radius of gyration

    if args.rg.calculate is True:
        rg_squared = lmp.extract_compute('rg', 0, 0)
        rg_data.append(rg_squared**0.5)

    # Get temperature

    if args.t.calculate is True:
        t = lmp.extract_compute('temp', 0, 0)
        t_data.append(t)

    # Get energies
    if args.energies.calculate is True:
        e_pair = lmp.extract_compute('pair', 0, 0)
        e_bond = lmp.extract_compute('bond', 2, 1)
        e_angle = lmp.extract_compute('angle', 2, 1)
        energy_data.append([e_pair, sum(e_bond[0:natoms - 1]) / (natoms - 1),
                            sum(e_angle[0:natoms - 2]) / (natoms - 2)])

    lmp.command('run {0:d}'.format(args.steps[1]))

# As a test, switch to an attractive potential after equilibrating

# lmp.command('pair_coeff 1 1 3.0 1.0 1.8')
# lmp.command('run 1000000')

# Get data from the final timestep

if args.rg.calculate is True:
    rg_squared = lmp.extract_compute('rg', 0, 0)
    rg_data.append(rg_squared**0.5)

if args.t.calculate is True:
    t = lmp.extract_compute('temp', 0, 0)
    t_data.append(t)

if args.energies.calculate is True:
    e_pair = lmp.extract_compute('pair', 0, 0)
    e_bond = lmp.extract_compute('bond', 2, 1)
    e_angle = lmp.extract_compute('angle', 2, 1)
    energy_data.append([e_pair, sum(e_bond[0:natoms - 1]) / (natoms - 1), sum(e_angle[0:natoms - 2]) / (natoms - 2)])

# Print some graphs - but only on our root process!

if proc_id == 0:

    if args.rg.calculate is True or args.t.calculate is True:
        fig1 = pyplot.figure(1)
        ax1 = pyplot.axes()
        ax2 = pyplot.axes()

        # Radius of Gyration

        if args.rg.calculate is True:
            ax1.set_xlabel('Timesteps')
            ax1.set_ylabel('Radius of Gyration', color='b')
            ax1.plot(range(0, len(rg_data) * args.steps[1], args.steps[1]), rg_data, 'b-')
            fig1.add_axes(ax1)
            fig1.tight_layout()

        # Shared x axis if 2 plots

        if args.rg.calculate is True and args.t.calculate is True:
            ax2 = ax1.twinx()

        # Temperature

        if args.t.calculate is True:
            ax2.set_ylabel('Temp', color='r')
            ax2.set_xlabel('Timesteps')
            ax2.plot(range(0, len(rg_data) * args.steps[1], args.steps[1]), t_data, 'r-')
            fig1.add_axes(ax2)
            fig1.tight_layout()

        if args.t.save is True:
            fig1.savefig(args.t.saveAt)

        if args.rg.save is True:
            fig1.savefig(args.rg.saveAt)

    if args.energies.calculate is True:
        fig2 = pyplot.figure(2)
        ax3 = pyplot.axes()
        ax4 = pyplot.axes()

        # Bond Energy Graph

        ax3.set_xlabel('Timesteps')
        ax3.set_ylabel('Bond Energy(kT)', color='b')
        ax3.plot(range(0, len(energy_data) * args.steps[1], args.steps[1]), zip(*energy_data)[1], 'b-')
        fig2.add_axes(ax3)

        # Angle Energy Graph

        ax4 = ax3.twinx()

        ax4.set_ylabel('Angle Energy(kT)', color='r')
        ax4.plot(range(0, len(energy_data) * args.steps[1], args.steps[1]), zip(*energy_data)[2], 'r-')
        fig2.add_axes(ax4)
        fig2.tight_layout()

    # Save graphs to file if requested

    if args.energies.save is True:
        fig2.savefig(args.energies.saveAt)


    # If we have something to show, show it!

    if args.energies.show is True or args.t.show is True or args.rg.show is True:
        print ('Close graph to continue..')
        pyplot.show()

    # Show the trajectory in vmd

    if args.show_dumpfile and args.dumpfile is not None:
        utility.launch_vmd(args.dumpfile[0], os.path.dirname(os.path.realpath(__file__)) + '/Equil/vmd.equil.in')

# Wait for all our processes to be done, and report that things went ok (or at least we didn't crash)

if comm is not None:
    comm.Barrier()
    for proc_match in range(nprocs):
        if proc_id == proc_match:
            print ('Process id {0:d} exiting ({1:d} total processes)\n'.format(proc_id, nprocs))
            time.sleep(0.1)
        comm.Barrier()
