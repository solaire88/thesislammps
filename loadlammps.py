from __future__ import print_function
import sys
import os
import imp
python_target = sys.argv[1]
if python_target == 'other':
    from lammps import lammps
if python_target == 'thesis':
    localpath = os.path.dirname(os.path.realpath(__file__))+'/lammps/python'
    if localpath not in sys.path:
        sys.path.append(localpath)
    lammps = imp.load_source('lammps',str(localpath)+'/lammps.py')
library = sys.argv[2]
if python_target == 'thesis':
    lmp = lammps.lammps(library)
else:
    lmp = lammps(library)
lmp.command('variable i equal 1')
lmp.command('print ${i}')
lmp.close()
