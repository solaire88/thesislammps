import subprocess
from argparse import Action
from distutils.spawn import find_executable

"""Just a few  functions or classes which get used across other scripts"""

def find_command(command):
    result = find_executable(command)
    if result is not None:
        return True
    else:
        return False

def launch_vmd(dumpfile_loc, startup_loc):
    """Launch vmd with settings and trajectory from given files"""
    try:
        subprocess.check_call(['vmd', '-lammpstrj', '{0:s}'.format(dumpfile_loc), '-startup',
                               '{0:s}'.format(startup_loc)])
    except OSError:
        print ('Couldn\'t find vmd in path (o_O)')


def recursive_length(our_list):
    """How many elements are in a nested list"""
    if type(our_list) is list:
        return sum(recursive_length(our_sublist) for our_sublist in our_list)
    else:
        return 1

def flatten_list(input_list,out_list = []):
    for item in input_list:
        if type(item) is list:
            out_list = flatten_list(item, out_list)
        else:
            out_list.append(item)
    return out_list


def get_atom_data(lmp_inst, min_id=None, max_id=None, match_types=None):
    """ Get position data for any atoms with id betwwen min_id and max_id, or any atoms with the specified id. Returns a
     dictionary with keys - atom ids and values - positions."""

    # Number of atoms in this instance

    inst_natoms = lmp_inst.get_natoms()

    # Create dict to store atom positions
    xyz = {}

    # Get our ctypes array of positions, ids and types from lammps

    lmp_xyz = lmp_inst.gather_atoms("x", 1, 3)
    lmp_id = lmp_inst.gather_atoms("id", 0, 1)
    lmp_type = lmp_inst.gather_atoms("type", 0, 1)

    if min_id is None:
        min_id = 1

    if max_id is None:
        max_id = inst_natoms

    # Copy data from the 1D ctypes array to our inst_natoms*3 python array

    if match_types is None:
        for i, atom_id in zip(range(3*min_id, 3*max_id, 3), range(min_id, max_id + 1)):
            xyz[atom_id] = [lmp_xyz[i], lmp_xyz[i+1], lmp_xyz[i+2]]
        return xyz
    else:
        # Input must be a list to use the 'in' check
        if type(match_types) is int:
            match_types = [match_types]

        # Find which atoms correspond to the types we selected
        for x_index, id_index in zip(range(0, 3*inst_natoms, 3), range(0, inst_natoms)):
            if lmp_type[id_index] in match_types:
                xyz[lmp_id[id_index]] = [lmp_xyz[x_index], lmp_xyz[x_index+1], lmp_xyz[x_index+2]]
        return xyz


def dist(atom1_xyz, atom2_xyz, boundaries):
    """Input (as list) [x,y,z] of both atoms and simulation boundaries. Returns distance using minimum image conv."""
    dists = [0.0, 0.0, 0.0]
    box_lengths = [boundaries[i + 1] - boundaries[i] for i in range(0, 6, 2)]
    for i in range(3):
        dists[i] = min(abs(atom1_xyz[i] - atom2_xyz[i]), abs(atom1_xyz[i] - atom2_xyz[i] + box_lengths[i]),
                       abs(atom1_xyz[i] - atom2_xyz[i] - box_lengths[i]))
    distance = (dists[0] ** 2 + dists[1] ** 2 + dists[2] ** 2) ** 0.5
    return distance


class RewindableIterator(object):
    """An iterator which can go backwards,forwards or jump to a particular iterable.
    Takes a regular iterable as argument"""

    def __init__(self, iterable):
        self.iterable = iterable
        self.index = 0

    def __iter__(self):
        """Start iterating"""
        self.index = 0
        return self

    def next(self):
        """Return this item, increment index counter so further next() calls return next items"""
        try:
            value = self.iterable[self.index]
            self.index += 1
            return value
        except IndexError:
            raise StopIteration

    def prev(self):
        """Return previous item on next iteration"""
        if self.index == 1:
            self.index = 0
        else:
            self.seek(-1)

    def repeat(self):
        """Return current item on next iteration"""
        self.seek(0)

    def seek(self, shift, absolute=False):
        """Move either to particular index with absolute=True, or shift index up/down with absolute=False (default).
        Does not return an item. Shift=0,absolute=False gets same item again on next iteration"""
        if type(shift) is not int:
            raise TypeError

        if absolute:
            self.index = shift
        else:
            self.index += shift - 1

        if self.index < 0:
            self.index = 0
            # warnings.warn("Tried to get item with index <0. Returned item at index 0.")

    def update(self, new_value, index='current'):
        """Replace a value of the iterator with a new value. Index points to the next
        index to be read, not the one currently in use"""
        if index == 'current':
            index = self.index - 1
        self.iterable[index] = new_value

    def getindex(self, index):
        """Return the object at index in the iterable"""
        return self.iterable[index]


def ImportMPI():
    try:
        from mpi4py import MPI

        # An object which communicates between processes (comm), the total number of process and this process id

        comm = MPI.COMM_WORLD
        nprocs = comm.Get_size()
        proc_id = comm.Get_rank()

    except ImportError:
        print ('Script running on 1 processor as mpi4py module could not be found/loaded')

        # If we don't have any MPI stuff going on, set our process id to 0

        MPI = None
        comm = None
        nprocs = 1
        proc_id = 0

    return MPI,comm,nprocs,proc_id


class QuitArgparse(Action):
    """A custom action so a command line parser which isn't on our root process (0) will just exit when given the -h flag.
    Otherwise they would all print help info, which is fine! But a bit overkill when we only need 1 help printout."""

    def __init__(self, option_strings, dest, **kwargs):
        super(QuitArgparse, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, localparser, namespace, values, option_string=None):
        localparser.exit(status=0, message=None)
        quit()

class GraphInstruct(object):
    """Object to encapsulate the three things we can get for a graph input - Nothing, Show At End, Save"""

    def __init__(self,value = False):
        if value == False:
            self.calculate = False
            self.show = False
            self.save = False
            self.saveAt = None
        elif value is None:
            self.calculate = True
            self.show = True
            self.save = False
            self.saveAt = None
        else:
            self.calculate = True
            self.show = False
            self.save = True
            self.saveAt = value

class GraphAction(Action):
    """A custom action which sets a value to true and returns arguments given with the flag"""

    def __init__(self, option_strings, dest, default=GraphInstruct(), nargs='?', **kwargs):
        super(GraphAction, self).__init__(option_strings, dest, default=default, nargs=nargs, **kwargs)

    def __call__(self, localparser, namespace, value, option_string=None):
        setattr(namespace,self.dest,GraphInstruct(value=value))

