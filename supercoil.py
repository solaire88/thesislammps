#! /usr/bin/env python
from __future__ import print_function
# Import custom LAMMPS distribution
import sys
import os
import imp

sys.path.append(os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/')
# print(os.environ['LD_LIBRARY_PATH'])
# print (sys.path)

try:
    from mpi4py import MPI

    # An object which communicates between processes (comm), the total number of process and this process id

    comm = MPI.COMM_WORLD
    nprocs = comm.Get_size()
    proc_id = comm.Get_rank()
    # We don't nedd MPI stuff if we only run on 1 process anyway

except ImportError:
    print ('Script running on 1 processor as mpi4py module could not be found/loaded')

    # If we don't have any MPI stuff going on, set our process id to 0

    comm = None
    nprocs = 1
    proc_id = 0

try:
    lammps = imp.load_source('lammps',os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/lammps.py')
    if proc_id == 0:
        print ('Loading Downloaded LAMMPS Version')
# Or use an existing one
except:
    import lammps
    if proc_id == 0:
        print ('Loading Pre-Existing LAMMPS Version')

from matplotlib import pyplot
import numpy as np

import argparse
import time
import utility

# For parallel runs make use of python MPI capabilities. While the non-LAMMPS parts of the script are not parallelised
# in any way, any LAMMPS command must be executed simultaneously by all processes.

# Read in things from the command line

if proc_id == 0:
    parser = argparse.ArgumentParser('Equilibrate a LAMMPS simulation')
else:
    parser = argparse.ArgumentParser('Equilibrate a LAMMPS simulation', add_help=False)
    parser.add_argument('-h', nargs=0, action=utility.QuitArgparse)
    parser.add_argument('--help', nargs=0, action=utility.QuitArgparse)

parser.add_argument('-rg', dest='rg', action=utility.GraphAction, help='Display a graph showing Radius of Gyration')
parser.add_argument('-in', dest='input', type=str, help='The name of the input atom datafile',
                    default=os.path.dirname(os.path.realpath(__file__)) + '/Equil/equil.initial')
parser.add_argument('-d', dest='dumpfile', nargs=2, type=str, help='Filename of dumpfile for simulation and'
                    'output frequency. Use: -d dumpfile.location 10000', default=None)
parser.add_argument('-show', dest='show_dumpfile', action='store_true', help='Show the dumpfile trajectory in vmd')

parser.add_argument('-s', dest='steps', nargs=2, type=int, help='Run for arg1 * arg2 timesteps. Default setting is 100 '
                                                                'steps of size 10000', default=[100, 10000])
parser.add_argument('-a', dest='angle_coeff', type=float, help='Set the coefficient for the cosine angle potential.'
                    'The persistence length is angle_coeff * sigma.', default=20.0)
parser.add_argument('-lib', dest='lammps_library', type=str, help='Loads library named liblammps_{lib}.so',
                    default=None)
parser.add_argument('-eqsoft', dest='pre_equil_time', type=int, help='Runs a pre equilibration with soft atomic potential and harmonic bonds',default=0)
args = parser.parse_args()

if nprocs == 1:
    comm = None

# Send our initial conditions to any other processes

if comm is not None:
    args = comm.bcast(args, 0)

# Create a pointer to our LAMMPS instance

if args.lammps_library is None and nprocs == 1:
    print ('Loaded default LAMMPS library')
    lmp = lammps.lammps()

elif args.lammps_library is None and nprocs != 1:
    print ('Loaded default multiprocess LAMMPS library')
    lmp = lammps.lammps('g++_openmpi')

else:
    lmp = lammps.lammps(args.lammps_library)
    if proc_id == 0:
        print('Loaded library: ' + str(args.lammps_library))

# Create a random seed number for use in equil.lmp, and save it as the LAMMPS variable ${seed}. As with our initial
# conditions, do this on process 0 and send it to the others. This also makes sure all processes have the same seed!

if proc_id == 0:
    seed_no = np.random.randint(0, 10000000)
else:
    seed_no = None

if comm is not None:
    seed_no = comm.bcast(seed_no, 0)

lmp.command('variable seed equal {0:d}'.format(seed_no))

# Create a LAMMPS variable pointing to our input data file

if not os.path.isfile(args.input) and proc_id == 0:
    raise IOError('Input file not found at {0:s}'.format(args.input))
lmp.command('variable inputfile string {0:s}'.format(args.input))



# Create a lammps variable with the coefficient for the angle potential

lmp.command('variable anglecoeff equal {0:f}'.format(args.angle_coeff))
lmp.command('variable eqsoft equal {}'.format(args.pre_equil_time))

# Run a script with basic settings for lammps

lmp.file(os.path.dirname(os.path.realpath(__file__)) + '/Supercoil/supercoil_pt1.lmp')

# Write to a output dumpfile every n timesteps

if args.dumpfile is not None:
    lmp.command('dump 1 all custom {0:d} {1:s} id type mol x y z ix iy iz'.format(int(args.dumpfile[1]),
                                                                                  args.dumpfile[0]))
polytorsionend = False

if args.pre_equil_time is not 0:
    lmp.command('angle_style hybrid cosine polytorsion polytorsionend')
    lmp.command('angle_coeff 1 cosine ${anglecoeff}')
    if polytorsionend is True:
        lmp.command('angle_coeff 2 polytorsion 80.0 29.68')
        lmp.command('angle_coeff 3 polytorsionend 80.0 29.68')
    else:
        lmp.command('angle_coeff 2 polytorsion 80.0 29.68')

    lmp.command('run ${eqsoft}')

lmp.file(os.path.dirname(os.path.realpath(__file__)) + '/Supercoil/supercoil_pt2.lmp')

# Create lists to store data for graphs, and set up computes to calculate the data

rg_data = []

if args.rg.calculate is True:
    lmp.command('compute rg all gyration')

natoms = lmp.get_natoms()
lmp.command('run 0')

for step in range(args.steps[0]):

    # Get radius of gyration

    if args.rg.calculate is True:
        rg_squared = lmp.extract_compute('rg', 0, 0)
        rg_data.append(rg_squared**0.5)

    lmp.command('run {0:d}'.format(args.steps[1]))
