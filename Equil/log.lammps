LAMMPS (17 Nov 2016)
variable seed equal 6163721
variable inputfile string equil.initial
variable anglecoeff equal 20.000000
### LAMMPS initialisation for an equilibration run with just DNA ###

#####
# For runs with >1 processor, set how lammps assigns which atoms belong to which processor. The 'brick' style splits
# the simulation area into a regular 3D grid of bricks and atoms in each processor's area are assigned to it.
# The 'tiled' setting allows this grid of bricks to be any (rectangular) shape, which is useful if a simulation
# has a higher density of atoms in some regions of the simulation area.
#####

comm_style tiled

#####
# Sets LAMMPS to use the Lennard-Jones unit style
#####

units lj

#####
# Use an atom style which lets us set an angle potential between groups of 3 atoms
#####

atom_style angle

#####
# Periodic Boundary Conditions
#####

boundary p p p

#####
# Create neighbour lists which set atoms to be neighbours if they are within 1.9 + force cutoff range of each other
# Update these lists every timestep, if any atoms have moved far enough
#####

neighbor 1.9 bin
neigh_modify every 1 delay 1 check yes

#####
# Read in initial atom positions, velocities, bonds between atoms and angles between atoms froma datafile
#####

read_data ${inputfile}
read_data equil.initial
  orthogonal box = (-75 -75 -75) to (75 75 75)
  1 by 1 by 2 MPI processor grid
  reading atoms ...
  1000 atoms
  reading velocities ...
  1000 velocities
  scanning bonds ...
  1 = max bonds/atom
  scanning angles ...
  1 = max angles/atom
  reading bonds ...
  999 bonds
  reading angles ...
  998 angles
  2 = max # of 1-2 neighbors
  2 = max # of 1-3 neighbors
  4 = max # of 1-4 neighbors
  6 = max # of special neighbors

#####
# Set a Lennard-Jones potential between atoms, with a cutoff at 2^(1/6), the potential minimum. For energy calculations
# shift the potential so E=0 at the potential minimum
#####
pair_style      lj/cut 1.12246152962189
pair_modify     shift yes

#####
# For the L-J potential set epsilon to 1 and sigma to 1, between atoms of type 1 (DNA). For DNA, 1.0 in simulation units
# corresponds to 2.5 nm
#####

pair_coeff 1 1 1.0 1.0 1.12246152962189

#####
# Set the FENE+LJ potential between neighbouring (bonded) atoms in the DNA chain. i.e. 1-2 40-41 etc.
#####

bond_style fene

#####
# The special bonds command prevents the LJ potential from being applied twice (i.e. once from pair interaction, once
# from bond interaction
#####

special_bonds fene
  2 = max # of 1-2 neighbors
  2 = max # of special neighbors

#####
# Set K to 30.0, max bond length to 1.6, epsilon to 1, sigma to 1
#####

bond_coeff 1 30.0 1.6 1.0 1.0

#####
# Set a cosine angle potential with A = 20. Lp = A * sigma
#####

angle_style cosine
angle_coeff 1 ${anglecoeff}
angle_coeff 1 20

#####
# Set up a constant NVE time-inegrator, a langevin style random force and update processor tiles for multiproc runs
#####

fix 1 all nve
fix 2 all langevin 1.0 1.0 1.0 ${seed}
fix 2 all langevin 1.0 1.0 1.0 6163721
fix 3 all balance 10000 1.1 rcb

#####
# Output thermodynamic data every 10000 timesteps (Timestep, Temp, Avg. Pair Energy, Simulation Volume)
#####

thermo 10000
thermo_style custom step temp epair vol

#####
# Set a scaling factor for dt. The smaller this is the slower the simulations will run, though setting large values
# makes crashes due to unrealistic interactions more likely e.g. two atoms occupying almost the same space
#####

timestep 0.01
run 0
Neighbor list info ...
  1 neighbor list requests
  update every 1 steps, delay 1 steps, check yes
  max neighbors/atom: 2000, page size: 100000
  master list distance cutoff = 3.02246
  ghost atom cutoff = 3.02246
  binsize = 1.51123 -> bins = 100 100 100
Memory usage per processor = 7.1104 Mbytes
Step Temp E_pair Volume 
       0            0 0.0002164716      3375000 
Loop time of 2.38419e-06 on 2 procs for 0 steps with 1000 atoms

0.0% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0          | 0          | 0          |   0.0 |  0.00
Bond    | 0          | 0          | 0          |   0.0 |  0.00
Neigh   | 0          | 0          | 0          |   0.0 |  0.00
Comm    | 0          | 0          | 0          |   0.0 |  0.00
Output  | 0          | 0          | 0          |   0.0 |  0.00
Modify  | 0          | 0          | 0          |   0.0 |  0.00
Other   |            | 2.384e-06  |            |       |100.00

Nlocal:    500 ave 501 max 499 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    106.5 ave 112 max 101 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    1041 ave 1047 max 1035 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 2082
Ave neighs/atom = 2.082
Ave special neighs/atom = 1.998
Neighbor list builds = 0
Dangerous builds = 0
run 10000
Memory usage per processor = 7.1104 Mbytes
Step Temp E_pair Volume 
       0            0 0.0002164716      3375000 
   10000   0.96191134            0      3375000 
Loop time of 1.06846 on 2 procs for 10000 steps with 1000 atoms

Performance: 8086397.608 tau/day, 9359.256 timesteps/s
99.2% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.072659   | 0.077008   | 0.081357   |   1.6 |  7.21
Bond    | 0.39049    | 0.39119    | 0.3919     |   0.1 | 36.61
Neigh   | 0.15545    | 0.15548    | 0.15552    |   0.0 | 14.55
Comm    | 0.067227   | 0.071401   | 0.075575   |   1.6 |  6.68
Output  | 1.9073e-05 | 2.6584e-05 | 3.4094e-05 |   0.1 |  0.00
Modify  | 0.31853    | 0.32271    | 0.32688    |   0.7 | 30.20
Other   |            | 0.05064    |            |       |  4.74

Nlocal:    500 ave 513 max 487 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    89 ave 97 max 81 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    1005 ave 1034 max 976 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 2010
Ave neighs/atom = 2.01
Ave special neighs/atom = 1.998
Neighbor list builds = 240
Dangerous builds = 0
run 10000
Memory usage per processor = 7.1104 Mbytes
Step Temp E_pair Volume 
   10000   0.96191134            0      3375000 
   20000   0.98044761 0.00024409388      3375000 
Loop time of 1.10288 on 2 procs for 10000 steps with 1000 atoms

Performance: 7834027.746 tau/day, 9067.162 timesteps/s
98.5% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.078086   | 0.079204   | 0.080323   |   0.4 |  7.18
Bond    | 0.38274    | 0.39753    | 0.41233    |   2.3 | 36.04
Neigh   | 0.16338    | 0.16341    | 0.16344    |   0.0 | 14.82
Comm    | 0.065945   | 0.079799   | 0.093652   |   4.9 |  7.24
Output  | 2.3127e-05 | 2.7537e-05 | 3.1948e-05 |   0.1 |  0.00
Modify  | 0.31807    | 0.32771    | 0.33734    |   1.7 | 29.71
Other   |            | 0.0552     |            |       |  5.01

Nlocal:    500 ave 515 max 485 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    76.5 ave 105 max 48 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    1054 ave 1075 max 1033 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 2108
Ave neighs/atom = 2.108
Ave special neighs/atom = 1.998
Neighbor list builds = 240
Dangerous builds = 0
run 10000
Memory usage per processor = 7.1104 Mbytes
Step Temp E_pair Volume 
   20000   0.98044761 0.00024409388      3375000 
   30000   0.99060098            0      3375000 
Loop time of 1.07502 on 2 procs for 10000 steps with 1000 atoms

Performance: 8037026.241 tau/day, 9302.114 timesteps/s
99.5% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.07655    | 0.078091   | 0.079633   |   0.6 |  7.26
Bond    | 0.37569    | 0.39275    | 0.4098     |   2.7 | 36.53
Neigh   | 0.15776    | 0.15779    | 0.15783    |   0.0 | 14.68
Comm    | 0.057486   | 0.072841   | 0.088196   |   5.7 |  6.78
Output  | 2.0981e-05 | 2.5988e-05 | 3.0994e-05 |   0.1 |  0.00
Modify  | 0.31282    | 0.32331    | 0.33381    |   1.8 | 30.08
Other   |            | 0.05021    |            |       |  4.67

Nlocal:    500 ave 521 max 479 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    80 ave 94 max 66 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    1008.5 ave 1039 max 978 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 2017
Ave neighs/atom = 2.017
Ave special neighs/atom = 1.998
Neighbor list builds = 239
Dangerous builds = 0
run 10000
Memory usage per processor = 7.1104 Mbytes
Step Temp E_pair Volume 
   30000   0.99060098            0      3375000 
   40000   0.99608007            0      3375000 
Loop time of 1.0963 on 2 procs for 10000 steps with 1000 atoms

Performance: 7881043.273 tau/day, 9121.578 timesteps/s
97.8% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.076151   | 0.077237   | 0.078322   |   0.4 |  7.05
Bond    | 0.36501    | 0.39213    | 0.41925    |   4.3 | 35.77
Neigh   | 0.15827    | 0.15831    | 0.15835    |   0.0 | 14.44
Comm    | 0.059022   | 0.086898   | 0.11477    |   9.5 |  7.93
Output  | 1.9073e-05 | 2.5988e-05 | 3.2902e-05 |   0.1 |  0.00
Modify  | 0.30416    | 0.32306    | 0.34197    |   3.3 | 29.47
Other   |            | 0.05864    |            |       |  5.35

Nlocal:    500 ave 501 max 499 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    97 ave 118 max 76 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    991.5 ave 995 max 988 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 1983
Ave neighs/atom = 1.983
Ave special neighs/atom = 1.998
Neighbor list builds = 237
Dangerous builds = 0
run 10000
Memory usage per processor = 7.18476 Mbytes
Step Temp E_pair Volume 
   40000   0.99608007            0      3375000 
   50000      0.97017            0      3375000 
Loop time of 1.10827 on 2 procs for 10000 steps with 1000 atoms

Performance: 7795919.603 tau/day, 9023.055 timesteps/s
98.2% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.076295   | 0.076736   | 0.077176   |   0.2 |  6.92
Bond    | 0.37079    | 0.39245    | 0.41412    |   3.5 | 35.41
Neigh   | 0.17713    | 0.17717    | 0.17722    |   0.0 | 15.99
Comm    | 0.063142   | 0.084545   | 0.10595    |   7.4 |  7.63
Output  | 2.5034e-05 | 3.3498e-05 | 4.1962e-05 |   0.1 |  0.00
Modify  | 0.30796    | 0.3226     | 0.33723    |   2.6 | 29.11
Other   |            | 0.05474    |            |       |  4.94

Nlocal:    500 ave 527 max 473 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    109.5 ave 110 max 109 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    1024.5 ave 1063 max 986 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 2049
Ave neighs/atom = 2.049
Ave special neighs/atom = 1.998
Neighbor list builds = 238
Dangerous builds = 0
run 10000
Memory usage per processor = 7.18476 Mbytes
Step Temp E_pair Volume 
   50000      0.97017            0      3375000 
   60000    1.0266286            0      3375000 
Loop time of 1.11763 on 2 procs for 10000 steps with 1000 atoms

Performance: 7730657.469 tau/day, 8947.520 timesteps/s
96.5% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.07663    | 0.076903   | 0.077176   |   0.1 |  6.88
Bond    | 0.37583    | 0.39407    | 0.41231    |   2.9 | 35.26
Neigh   | 0.18544    | 0.18555    | 0.18566    |   0.0 | 16.60
Comm    | 0.066373   | 0.083845   | 0.10132    |   6.0 |  7.50
Output  | 2.0027e-05 | 2.5988e-05 | 3.1948e-05 |   0.1 |  0.00
Modify  | 0.31291    | 0.32433    | 0.33575    |   2.0 | 29.02
Other   |            | 0.0529     |            |       |  4.73

Nlocal:    500 ave 502 max 498 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    112 ave 121 max 103 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    991 ave 998 max 984 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 1982
Ave neighs/atom = 1.982
Ave special neighs/atom = 1.998
Neighbor list builds = 244
Dangerous builds = 0
run 10000
Memory usage per processor = 7.18476 Mbytes
Step Temp E_pair Volume 
   60000    1.0266286            0      3375000 
   70000    1.0535015            0      3375000 
Loop time of 1.09414 on 2 procs for 10000 steps with 1000 atoms

Performance: 7896605.601 tau/day, 9139.590 timesteps/s
98.0% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.072003   | 0.076987   | 0.081972   |   1.8 |  7.04
Bond    | 0.38697    | 0.39432    | 0.40168    |   1.2 | 36.04
Neigh   | 0.15914    | 0.15918    | 0.15922    |   0.0 | 14.55
Comm    | 0.069885   | 0.0826     | 0.095314   |   4.4 |  7.55
Output  | 1.7881e-05 | 2.5511e-05 | 3.314e-05  |   0.2 |  0.00
Modify  | 0.31427    | 0.32433    | 0.33438    |   1.8 | 29.64
Other   |            | 0.05669    |            |       |  5.18

Nlocal:    500 ave 513 max 487 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    122 ave 127 max 117 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    995 ave 1014 max 976 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 1990
Ave neighs/atom = 1.99
Ave special neighs/atom = 1.998
Neighbor list builds = 242
Dangerous builds = 0
run 10000
Memory usage per processor = 7.18476 Mbytes
Step Temp E_pair Volume 
   70000    1.0535015            0      3375000 
   80000    1.0255985            0      3375000 
Loop time of 1.09115 on 2 procs for 10000 steps with 1000 atoms

Performance: 7918228.565 tau/day, 9164.616 timesteps/s
99.2% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.072163   | 0.077589   | 0.083014   |   1.9 |  7.11
Bond    | 0.3821     | 0.39346    | 0.40482    |   1.8 | 36.06
Neigh   | 0.15788    | 0.15788    | 0.15789    |   0.0 | 14.47
Comm    | 0.063149   | 0.080792   | 0.098435   |   6.2 |  7.40
Output  | 2.0981e-05 | 2.6941e-05 | 3.2902e-05 |   0.1 |  0.00
Modify  | 0.31104    | 0.32383    | 0.33663    |   2.2 | 29.68
Other   |            | 0.05757    |            |       |  5.28

Nlocal:    500 ave 500 max 500 min
Histogram: 2 0 0 0 0 0 0 0 0 0
Nghost:    110.5 ave 111 max 110 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    988.5 ave 989 max 988 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 1977
Ave neighs/atom = 1.977
Ave special neighs/atom = 1.998
Neighbor list builds = 242
Dangerous builds = 0
run 10000
Memory usage per processor = 7.18476 Mbytes
Step Temp E_pair Volume 
   80000    1.0255985            0      3375000 
   90000   0.98972646            0      3375000 
Loop time of 1.08972 on 2 procs for 10000 steps with 1000 atoms

Performance: 7928631.734 tau/day, 9176.657 timesteps/s
98.6% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.077796   | 0.07787    | 0.077945   |   0.0 |  7.15
Bond    | 0.37448    | 0.39472    | 0.41496    |   3.2 | 36.22
Neigh   | 0.15757    | 0.15767    | 0.15776    |   0.0 | 14.47
Comm    | 0.060729   | 0.079931   | 0.099133   |   6.8 |  7.33
Output  | 2.0027e-05 | 2.5511e-05 | 3.0994e-05 |   0.1 |  0.00
Modify  | 0.31259    | 0.32499    | 0.33738    |   2.2 | 29.82
Other   |            | 0.05452    |            |       |  5.00

Nlocal:    500 ave 509 max 491 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    88 ave 97 max 79 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    989.5 ave 1006 max 973 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 1979
Ave neighs/atom = 1.979
Ave special neighs/atom = 1.998
Neighbor list builds = 237
Dangerous builds = 0
run 10000
Memory usage per processor = 7.18476 Mbytes
Step Temp E_pair Volume 
   90000   0.98972646            0      3375000 
  100000   0.98567435 0.00046662995      3375000 
Loop time of 1.0929 on 2 procs for 10000 steps with 1000 atoms

Performance: 7905558.260 tau/day, 9149.952 timesteps/s
99.0% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.075207   | 0.078596   | 0.081984   |   1.2 |  7.19
Bond    | 0.39473    | 0.39711    | 0.39949    |   0.4 | 36.34
Neigh   | 0.16725    | 0.1673     | 0.16734    |   0.0 | 15.31
Comm    | 0.068546   | 0.070236   | 0.071926   |   0.6 |  6.43
Output  | 2.9802e-05 | 2.9922e-05 | 3.0041e-05 |   0.0 |  0.00
Modify  | 0.32472    | 0.32674    | 0.32877    |   0.4 | 29.90
Other   |            | 0.05289    |            |       |  4.84

Nlocal:    500 ave 500 max 500 min
Histogram: 2 0 0 0 0 0 0 0 0 0
Nghost:    68.5 ave 75 max 62 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    1002.5 ave 1017 max 988 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 2005
Ave neighs/atom = 2.005
Ave special neighs/atom = 1.998
Neighbor list builds = 240
Dangerous builds = 0
run 10000
Memory usage per processor = 7.18476 Mbytes
Step Temp E_pair Volume 
  100000   0.98567435 0.00046662995      3375000 
  110000     1.014088            0      3375000 
Loop time of 1.06989 on 2 procs for 10000 steps with 1000 atoms

Performance: 8075572.994 tau/day, 9346.728 timesteps/s
98.1% CPU use with 2 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0.076334   | 0.077572   | 0.078809   |   0.4 |  7.25
Bond    | 0.37808    | 0.39213    | 0.40619    |   2.2 | 36.65
Neigh   | 0.15878    | 0.15881    | 0.15884    |   0.0 | 14.84
Comm    | 0.057135   | 0.069808   | 0.082481   |   4.8 |  6.52
Output  | 2.0027e-05 | 2.5511e-05 | 3.0994e-05 |   0.1 |  0.00
Modify  | 0.3158     | 0.32306    | 0.33031    |   1.3 | 30.20
Other   |            | 0.04849    |            |       |  4.53

Nlocal:    500 ave 516 max 484 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Nghost:    54 ave 64 max 44 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Neighs:    998 ave 1031 max 965 min
Histogram: 1 0 0 0 0 0 0 0 0 1

Total # of neighbors = 1996
Ave neighs/atom = 1.996
Ave special neighs/atom = 1.998
Neighbor list builds = 240
Dangerous builds = 0
