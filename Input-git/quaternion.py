import vector
import math
import numpy as np

class quaternion(object):
    
    
    def __init__(self,q0,q1,q2,q3):
        self.q0=q0
        self.q1=q1
        self.q2=q2
        self.q3=q3
        
    @staticmethod
    def axes_transform_quaternion_eigenvector(lst1,lst2,matchlist=None):
        """Rotate from vectors lst1 to lst2. Finds eigenvalue/eigenvector closest to 1, which corresponds to the required quaternion"""
        for i in range(len(lst1)):
            lst1[i]=lst1[i].to_list()
        for i in range(len(lst2)):
            lst2[i]=lst2[i].to_list()
        if not matchlist:
            matchlist=range(len(lst1))
        M=np.matrix([[0,0,0],[0,0,0],[0,0,0]])

        for i,coord1 in enumerate(lst1):
            x=np.matrix(np.outer(coord1,lst2[matchlist[i]]))
            M=M+x

        N11=float(M[0][:,0]+M[1][:,1]+M[2][:,2])
        N22=float(M[0][:,0]-M[1][:,1]-M[2][:,2])
        N33=float(-M[0][:,0]+M[1][:,1]-M[2][:,2])
        N44=float(-M[0][:,0]-M[1][:,1]+M[2][:,2])
        N12=float(M[1][:,2]-M[2][:,1])
        N13=float(M[2][:,0]-M[0][:,2])
        N14=float(M[0][:,1]-M[1][:,0])
        N21=float(N12)
        N23=float(M[0][:,1]+M[1][:,0])
        N24=float(M[2][:,0]+M[0][:,2])
        N31=float(N13)
        N32=float(N23)
        N34=float(M[1][:,2]+M[2][:,1])
        N41=float(N14)
        N42=float(N24)
        N43=float(N34)
        N=np.matrix([[N11,N12,N13,N14],
              [N21,N22,N23,N24],
              [N31,N32,N33,N34],
              [N41,N42,N43,N44]])

        values,vectors=np.linalg.eig(N)
        w=list(values)
        mw=max(w)
        quat= vectors[:,w.index(mw)]
        quat=np.array(quat).reshape(-1,).tolist()
        quat=quaternion(quat[0].real,quat[1].real,quat[2].real,quat[3].real)
        return quat
    
    @staticmethod
    def axes_transform_quaternion_rotation(vectorlistold,vectorlistnew):
        """Finds quaternion to rotate old z-axis to new z-axis, then finds quaternion which transforms old x and y axes to new
        x and y axes by rotating about new z axis. Returns the quaternion that performs this transformation in 1 step (needed in lammps input file)."""
        #map z old to z new
        dotprod=vector.vector.dot(vectorlistold[2],vectorlistnew[2])
        angle=math.acos(dotprod)
        axis=vector.vector.cross(vectorlistold[2],vectorlistnew[2])
        rq=quaternion.rotation_quaternion(axis,angle)
        rotated_vectors=[]
        for i in range(len(vectorlistold)):
            rotated_vec=rq.rotate_vector(vectorlistold[i])
            rotated_vec=vector.vector(rotated_vec.q1,rotated_vec.q2,rotated_vec.q3)
            rotated_vectors.append(rotated_vec)
        #map rotated x old to x new    
        dotprod=vector.vector.dot(rotated_vectors[0],vectorlistnew[0])
        angle=math.acos(dotprod)
        axis=vector.vector.cross(rotated_vectors[0],vectorlistnew[0])
        rq2=quaternion.rotation_quaternion(axis,angle)
        combined_quat=quaternion.multiplycls(rq2,rq)
        return combined_quat
    
    def euleralpha(self):
        """Euler angle alpha of transformation"""
        x=2*(self.q0*self.q1+self.q2*self.q3)
        y=1-2(self.q1*self.q1+self.q2*self.q2)
        return math.atan2(x,y)
    
    def eulerbeta(self):
        """Euler angle beta of transformation"""
        return math.asin(2(self.q0*self.q2-self.q1*self.q3))
    
    def eulergamma(self):
        """Euler angle gamma of transformation"""
        x=2*(q0*q3+q1*q2)
        y=1-2(self.q2*self.q2+self.q3*self.q3)
        return math.atan2(x,y)
        
    def inverse(self):
        q0=self.q0
        q1=-self.q1
        q2=-self.q2
        q3=-self.q3
        return quaternion(q0,q1,q2,q3)
    
    @staticmethod
    def multiplycls(quat1,quat2):
        """Multiply two quaternions and return result"""
        q0=quat1.q0*quat2.q0-quat1.q1*quat2.q1-quat1.q2*quat2.q2-quat1.q3*quat2.q3
        q1=quat1.q0*quat2.q1+quat1.q1*quat2.q0+quat1.q2*quat2.q3-quat1.q3*quat2.q2
        q2=quat1.q0*quat2.q2-quat1.q1*quat2.q3+quat1.q2*quat2.q0+quat1.q3*quat2.q1
        q3=quat1.q0*quat2.q3+quat1.q1*quat2.q2-quat1.q2*quat2.q1+quat1.q3*quat2.q0
        return quaternion(q0,q1,q2,q3)

    def multiplyinst(self,qmult):
        """Multiply this quaternion by qmult and update current quaternion with result"""
        q0=self.q0*qmult.q0-self.q1*qmult.q1-self.q2*qmult.q2-self.q3*qmult.q3
        q1=self.q0*qmult.q1+self.q1*qmult.q0+self.q2*qmult.q3-self.q3*qmult.q2
        q2=self.q0*qmult.q2-self.q1*qmult.q3+self.q2*qmult.q0+self.q3*qmult.q1
        q3=self.q0*qmult.q3+self.q1*qmult.q2-self.q2*qmult.q1+self.q3*qmult.q0
        self.q0=q0
        self.q1=q1
        self.q2=q2
        self.q3=q3

    def norm(self):
        """Length of this quaternion"""
        return math.sqrt(self.q0*self.q0+self.q1*self.q1+self.q2*self.q2+self.q3*self.q3)

    @staticmethod
    def rotation_quaternion(axis,angle):
        """Return the quaternion representing a clockwise rotation about axis by angle""" 
        sinangle=math.sin(0.5*angle)
        cosangle=math.cos(0.5*angle)
        rotation_quat=quaternion(cosangle,axis.x*sinangle,axis.y*sinangle,axis.z*sinangle)
        return rotation_quat
    
    def rotate_vector(self,vector):
        """Rotate given vector using current quaternion and return result"""
        q_vector=quaternion(0,vector.x,vector.y,vector.z)
        inverse_q=self.inverse()
        qv=quaternion.multiplycls(self,q_vector)
        qvq_inv=quaternion.multiplycls(qv,inverse_q)
        return qvq_inv

    def __str__(self):
        return str(self.q0)+' '+str(self.q1)+' '+str(self.q2)+' '+str(self.q3)
    
    def zaxis(self):
        """Result of transforming (0,0,1) by this quaternion"""
        x=2*q1*q3+2*q0*q2
        y=2*q2*q3-2*q0*q1
        z=q0*q0-q1*q1-q2*q2+q3*q3
        return(vector.vector(x,y,z))
