from mpi4py import MPI
from lammps import lammps
import vector,numpy,math
import argparse,os
import copy

lmp = lammps('ompi_g++_clean')
lmp.file('initial_test.lammps')

motor_file = open('cohesin_simulation_results.dat', 'w')

types_dict = {'dna':[1,2],'prot':[],'ctcf':[],'cohesin':[3],'motor':[4]}

steps = 100

def dist(atom1_xyz, atom2_xyz, boundaries):
    """Input (as list) [x,y,z] of both atoms and simulation boundaries. Returns distance using minimum image conv."""
    dists = [0.0, 0.0, 0.0]
    box_lengths = [boundaries[i + 1] - boundaries[i] for i in range(0, 6, 2)]
    for i in range(3):
        dists[i] = min(abs(atom1_xyz[i] - atom2_xyz[i]), abs(atom1_xyz[i] - atom2_xyz[i] + box_lengths[i]),
                       abs(atom1_xyz[i] - atom2_xyz[i] - box_lengths[i]))
    dist = (dists[0] ** 2 + dists[1] ** 2 + dists[2] ** 2) ** 0.5
    return dist


def update_cohesin_info(dna_list, motor_list, cohesin_list, boundaries):
    """ Return information about cohesin locations, do not write to file """
    motor_dna = get_cohesin_info(dna_list, motor_list, cohesin_list, boundaries)
    return motor_dna


def write_motor_file(dna_list, motor_list, cohesin_list, boundaries, motor_file, timestep):
    """ Return information about cohesin locations and write to file """
    motor_dna = get_cohesin_info(dna_list, motor_list, cohesin_list, boundaries, motor_file, timestep)
    return motor_dna


def get_cohesin_info(dna_list, motor_list, cohesin_list, boundaries, motor_file=None, timestep=None):
    """Write to a file the dna bead closest to motor for each cohesin, along with the size of the loop
    extruded by the cohesin pair."""
    # Only write on proc 1

    motor_dna = []
    for motor in motor_list:
        min_dist = [max([boundaries[i + 1] - boundaries[i] for i in range(0, 6, 2)]), -1]
        for dna in dna_list:
            d = dist([motor[2], motor[3], motor[4]], [dna[2], dna[3], dna[4]], boundaries)
            if d < min_dist[0]:
                # [distance, dna id, motor mol. no]
                min_dist = [d, dna[0], motor[1]]
        motor_dna.append(min_dist)
    if motor_file is not None:
        motor_file.write('Timestep: {0:d}\n'.format(timestep))
        motor_file.write('Motors:\n')
        for m in motor_dna:
            motor_file.write(str(m) + '\n')
        motor_file.write('Cohesin No and Loop Size:\n')

    # Sort motor dna list by molecule no.
    sorted_motor_dna = sorted(motor_dna, key=lambda a: a[2])

    # Molecule numbers 2*n and 2*n+1 are always pairs
    for i in range(0, len(sorted_motor_dna), 2):
        loop_size = abs(sorted_motor_dna[i][1] - sorted_motor_dna[i + 1][1])
        cohesin_no = sorted_motor_dna[i][2] / 2
        if motor_file is not None:
            motor_file.write('Coh ' + str(cohesin_no) + ' ' + str(loop_size) + '\n')

    if motor_file is not None:
        motor_file.flush()

    return sorted_motor_dna


def populate_cohesin_lists(atom_types, atom_positions, atom_mol_nos, max_id, types_dict):
    """Return a list of cohesin atoms, dna atoms, ctcf atoms and two dicts with {cohesin no:mol nos.} and {cohesin no: atom id start,atom id end for all current cohesins"""
    current_dna_list = []
    current_cohesin_list = []
    current_ctcf_list = []
    current_motor_list = []
    for i in range(1, max_id + 1):
        atom_data = [i, atom_mol_nos[i], atom_positions[i][0], atom_positions[i][1], atom_positions[i][2]]

        # DNA Types
        if atom_types[i] in types_dict['dna']:
            current_dna_list.append(atom_data)
            if atom_types[i] in types_dict['ctcf']:
                current_ctcf_list.append(atom_data)
        # Cohesin Types
        if atom_types[i] in types_dict['cohesin']:
            current_cohesin_list.append(atom_data)
        # Motor Types
        if atom_types[i] in types_dict['motor']:
            current_motor_list.append(atom_data)

    return current_dna_list, current_cohesin_list, current_ctcf_list, current_motor_list


def populate_system_data():

    """ Method to retrieve useful system data from a lammps instance, extra = False returns only properties with
        original lammps distribution"""

    atom_no = lmp.get_natoms()
    atom_types_proc = lmp.extract_atom('type', 0)
    atom_positions_proc = lmp.extract_atom('x', 3)
    atom_mol_nos_proc = lmp.extract_atom('molecule', 0)
    atom_id_proc = lmp.extract_atom('id', 0)
    nlocal = lmp.extract_global('nlocal', 0)

    max_id = numpy.array([max([atom_id_proc[k] for k in range(nlocal)])], dtype=int)

    atom_types_proc_array = numpy.array([0 for i in range(max_id + 1)], dtype=int)
    atom_positions_proc_array = numpy.array([[0, 0, 0] for i in range(max_id + 1)], dtype=float)
    atom_mol_nos_proc_array = numpy.array([0 for i in range(max_id + 1)], dtype=int)

    for j in range(nlocal):
        atom_types_proc_array[atom_id_proc[j]] = atom_types_proc[j]
        atom_positions_proc_array[atom_id_proc[j]][0] = atom_positions_proc[j][0]
        atom_positions_proc_array[atom_id_proc[j]][1] = atom_positions_proc[j][1]
        atom_positions_proc_array[atom_id_proc[j]][2] = atom_positions_proc[j][2]
        atom_mol_nos_proc_array[atom_id_proc[j]] = atom_mol_nos_proc[j]

    atom_types = atom_types_proc_array
    atom_positions = atom_positions_proc_array
    atom_mol_nos = atom_mol_nos_proc_array

    boundaries = [lmp.extract_global('boxxlo', 1), lmp.extract_global('boxxhi', 1),
                  lmp.extract_global('boxylo', 1), lmp.extract_global('boxyhi', 1),
                  lmp.extract_global('boxzlo', 1), lmp.extract_global('boxzhi', 1)]

    return atom_types, atom_positions, atom_mol_nos, atom_no, boundaries, int(max_id)

for i in range(steps):
    atom_types, atom_positions, atom_mol_nos, atom_no, boundaries, max_id = populate_system_data()
    dna_list, cohesin_list, ctcf_list, motor_list = populate_cohesin_lists(atom_types, atom_positions,
                                                                                atom_mol_nos, max_id, types_dict)
    motor_dna = update_cohesin_info(dna_list, motor_list, cohesin_list, boundaries)
    write_motor_file(dna_list, motor_list, cohesin_list, boundaries, motor_file, 1000*i)

    lmp.command('run 1000')



