from mpi4py import MPI
import numpy
####
#
#Pass array size for this to work. Must Bcast to matching size array
#
####
comm=MPI.Comm.Get_parent()
size=comm.Get_size()
rank=comm.Get_rank()
test_array_size=numpy.array([1],dtype='i')
comm.Bcast([test_array_size,MPI.INT],root=0)
#print test_array_size,rank,size
test_array=numpy.array([[0.0,0.0,0.0] for i in range(test_array_size)],dtype='d')
comm.Bcast([test_array,MPI.DOUBLE],root=0)
#placed_array=numpy.array(0.0,'d')
#comm.Bcast([placed_array,MPI.DOUBLE],root=0)
