import Dnalinear
import vector
import quaternion
import copy
import math


class Dnalineartwist(Dnalinear.Dnalinear):
    def __init__(self, atom_no, boundaries, boundary_style=None, other_atoms=[], atom_type=1, switch_data=[],
                 start_point=None, start_point_minus_one=None, atom_diameter=1, section_length=1, connect=False,
                 twists=0,in_bounds=True, overlap=False, style='lineartwist', name='dnalineartwist', extra_bonds=None ,
                 supercoil_boundary = [0,0],bond_type=1,angle_type=1):
        super(Dnalineartwist, self).__init__(atom_no, boundaries, boundary_style, other_atoms, atom_type, switch_data,
                                           start_point, start_point_minus_one, atom_diameter, section_length, connect,
                                           in_bounds, overlap, style, name,
                                           extra_bonds,bond_type,angle_type)

        self.atomdict = {'id': 0, 'type': 1, 'x': 2, 'y': 3, 'z': 4, 'mol': 5, 'ellflag': 6, 'density': 7}
        self.elldict = {'id': 0, 'q0': 1, 'q1': 2, 'q2': 3, 'q3': 4, }
        self.ellipsoids = []
        self.twists = twists

        quaternions = self.new_quaternions()
        quaternions = self.add_twist(quaternions, twists)
        self.more_angles(supercoil_boundary,connect)
        self.reformat_atoms()
        self.add_angular_vel()
        self.quat_to_ellipse(quaternions)

    def add_angular_vel(self):
        """Add the values for wx wy wz"""
        for i in range(len(self.velocities)):
            for j in range(3):
                self.velocities[i].append(0)

    def add_twist(self, quaternions, twist):
        """Rotate each bead by pi*twist/atom_no, about the axis connecting it to the previous bead"""
        half_theta = math.pi * float(twist) / float(len(self.atoms))
        for index in range(1, len(self.atoms)):
            zaxis = map(lambda v: v[0] - v[1], zip(self.atoms[index][3:6], self.atoms[index - 1][3:6]))
            q0 = math.cos(index * half_theta)
            q1 = zaxis[0] * math.sin(index * half_theta)
            q2 = zaxis[1] * math.sin(index * half_theta)
            q3 = zaxis[2] * math.sin(index * half_theta)
            twist_quaternion = quaternion.quaternion(q0, q1, q2, q3)
            quaternions[index] = quaternion.quaternion.multiplycls(twist_quaternion, quaternions[index])
        return quaternions

    def more_angles(self,supercoil_boundary,connect):
        """Add the extra angles corresponding to the polytorsion lammps potential"""
        angle_no = len(self.angles[:])
        for i,angle in enumerate(self.angles[:]):
            if i == 0 and supercoil_boundary[0] == 1:
                self.angles.append([angle[0] + self.completed_angle_no, 3, angle[2], angle[3], angle[3]])
            elif i == angle_no - 1 and supercoil_boundary[1] == 1 and connect == True:
                self.angles.append([angle[0] + self.completed_angle_no, 3, angle[2], angle[3], angle[3]])
            else:
                self.angles.append([angle[0] + self.completed_angle_no, 2, angle[2], angle[3], angle[3]])
            # There is no angle where atom 1 - 2nd to last bead, atom 2 - last bead; create a supercoil angle between these.
            if connect == False and i == angle_no - 1:
                if supercoil_boundary[1] == 0:
                    self.angles.append([angle[0] + 1 + self.completed_angle_no, 2, angle[3], angle[4], angle[4]])
                elif supercoil_boundary[1] == 1:
                    self.angles.append([angle[0] + 1 + self.completed_angle_no, 3, angle[3], angle[4], angle[4]])


    def new_quaternions(self):
        """Find the quaternion associated to the first bead of a section. As the DNA object is
        currently untwisted, this quaternion is used for the other beads in the section as well"""
        quaternions = []
        z_axis = []
        x_axis = []
        y_axis = []
        self.dotproducts = [1.0]
        for index in range(0, len(self.atoms), self.section_length):
            veclist1, veclist2 = self.new_veclist(index, x_axis, y_axis, z_axis)
            single_quaternion = quaternion.quaternion.axes_transform_quaternion_rotation(veclist1, veclist2)
            quaternions.append(single_quaternion)
            for i in range(self.section_length - 1):
                quaternions.append(copy.deepcopy(single_quaternion))
                x_axis.append(x_axis[index])
                y_axis.append(y_axis[index])
                z_axis.append(z_axis[index])
        return quaternions

    def new_veclist(self, index, x_axis, y_axis, z_axis):
        """Return an orthonormal set of vectors where e3 connects the previous bead to this one. The new e1 is kept as close as possible to the
        previous e1"""
        z_axis.append(vector.vector.from_list(
            map(lambda v: v[0] - v[1], zip(self.atoms[index][3:6], self.atoms[index - 1][3:6]))))
        z_axis[index].rescale(1.0)
        if (index == 0):
            x_axis.append(vector.vector.cross(z_axis[0], vector.vector(0, 1, 0)))
        else:
            #x_axisvector, dotprod = (vector.vector.maxdot(z_axis[index], y_axis[index - 1], x_axis[index - 1], 10000))
            x_axisvector, dotprod = (vector.vector.getx(z_axis[index], z_axis[index - 1], x_axis[index - 1]))
            x_axis.append(x_axisvector)
            self.dotproducts.append(dotprod)
        x_axis[index].rescale(1.0)
        y_axis.append(vector.vector.cross(z_axis[index], x_axis[index]))
        y_axis[index].rescale(1.0)
        veclist1 = [vector.vector(1, 0, 0), vector.vector(0, 1, 0), vector.vector(0, 0, 1)]
        veclist2 = [x_axis[index], y_axis[index], z_axis[index]]
        return veclist1, veclist2

    def quat_to_ellipse(self, quaternions):
        """Write ellipse format for lammps input file. Format is id shape (ratio of x,y,z lengths) orientation (angle,rotation axis)"""
        for i, q in enumerate(quaternions):
            self.ellipsoids.append([i + 1, 1, 1, 1, q.q0, q.q1, q.q2, q.q3])

    def reformat_atoms(self):
        """For lammps style 'hybrid angle ellipse' we have id type x y z mol ellipsoidflag density.
        Ellipsoid flag is 1 for ellipsoidal particles, 0 for point particles"""
        for i in range(len(self.atoms)):
            col2 = self.atoms[i].pop(2)
            self.atoms[i].append(col2)
            self.atoms[i].append(1)
            self.atoms[i].append(1)
