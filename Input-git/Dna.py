import math
import vector
import random
import warnings
import RewindableIterator
try:
    from mpi4py import MPI
    import sys
    import numpy
except ImportError:
    pass


class Dna(object):
    def __init__(self,atom_no,boundaries,boundary_style=None,other_atoms=[],atom_type=1,switch_data=[],
                   start_point=None,start_point_minus_one=None,atom_diameter=1,section_length=1,connect=False,
                   in_bounds=True,overlap=False,style='',name='dna',extra_bonds=None,bond_type=1,angle_type=1):
        #Data Storage for any Dna object
        self.angles=[]
        self.angle_type = angle_type
        self.atoms=[]
        self.bonds=[]
        self.bond_type = bond_type
        self.velocities=[]
        self.vectors=[]
        #Options/Parameters which can apply to any Dna object
        self.atom_diameter=atom_diameter  
        self.atom_type=atom_type
        self.boundaries=boundaries
        self.boundary_style=boundary_style
        self.connect=connect
        self.completed_atom_no=atom_no
        self.exit_code=True
        self.extra_bonds=extra_bonds
        self.in_bounds=in_bounds
        self.name=name
        self.overlap_allowed=overlap
        self.restart_count=0
        self.restart_limit=10
        self.section_length=section_length
        self.start_point=start_point
        self.start_point_minus_one=start_point_minus_one
        self.style=style
        self.switch_data=switch_data
        self.angledict={'id':0,'type':1,'atom1':2,'atom2':3,'atom3':4}
        self.bonddict={'id':0,'type':1,'atom1':2,'atom2':3}
        self.veldict={'id':0,'xvel':1,'yvel':2,'zvel':3}
        self.completed_section_no=self.completed_atom_no/self.section_length
        
        #Method calls common to all Dna objects:
        self.boundaries,self.boundary_style,self.start_point=self.set_boundaries_and_start_point(self.boundaries,self.boundary_style,self.start_point)     
    
    def first_section(self,other_atoms):
        """Create first section or couple of points in DNA object"""
        self.restart_count+=1
        
        def trial_first_section():
            first_section=[]
            if(self.start_point_minus_one==None):
                """Don't worry about direction of the chain if there aren't any atoms in it yet!"""
                current_position=vector.vector(self.start_point[0],self.start_point[1],self.start_point[2])
                first_vector=vector.vector.random_vector(self.atom_diameter)
            
            else:
                current_position=vector.vector(self.start_point[0],self.start_point[1],self.start_point[2])
                previous_position=vector.vector(self.start_point_minus_one[0],self.start_point_minus_one[1],self.start_point_minus_one[2])
                first_vector=vector.vector.random_vector_selfavd(current_position,previous_position)
    
            first_position=vector.vector.add(current_position,first_vector)
            
            for i in range(self.section_length):
                first_section.append([i+1,1,self.atom_type,first_position.x,first_position.y,first_position.z])
                first_position=vector.vector.add(first_position,first_vector)
            
            return first_section,first_vector
        
        first_section,first_vector=trial_first_section()
        self.add_section(first_section,first_vector)
        
        if(self.overlap_allowed==False):
            """If atoms are not allowed to overlap, check their positions"""
            while(self.overlap(1,len(self.atoms),other_atoms)==True):
                """If any of the new atoms overlap existing ones, try again"""
                self.restart_count+=1
                self.remove_section(first_section,first_vector)
                first_section,first_vector=trial_first_section()
                self.add_section(first_section,first_vector)
                if(self.restart_count>self.restart_limit):
                    raise CreationError("Couldn't create first section")
   
    def in_boundary(self,atom_positions):
        """Return true if the particle is in bounds"""
        for atom in atom_positions:
            if (self.boundary_style=='rectangular' or self.boundary_style=='rectangular-start'):
                """Rectangular box (xmin,xmax,ymin,ymax,zmin,zmax)"""
                for i in range(0,3,1):
                    bdry_lo_index=2*i
                    bdry_hi_index=2*i+1
                    if(atom[i+3]<self.boundaries[bdry_lo_index]+self.atom_diameter/2.0 
                       or atom[i+3]>self.boundaries[bdry_hi_index]-self.atom_diameter/2.0):
                        return False           
            if (self.boundary_style=='cylinder'):
                """Cylinder (r,zmin,zmax)"""
                if(atom[5]<self.boundaries[1]+self.atom_diameter/2.0 
                   or atom[2]>self.boundaries[2]-self.atom_diameter/2.0):
                    return False
                radial_dist=math.sqrt(atom[3]**2+
                                      atom[4]**2)
                if (radial_dist>self.boundaries[0]-self.atom_diameter/2.0):
                    return False
        return True
    
    def overlap(self,start_atom,end_atom,other_atoms,max_processors=4):
        """Return true if atom positions overlap. Tests atoms in given range against all others in system.
        Args = atom id's rather than index of list."""
        if(max_processors>4):
            result=self.overlap_mpi(start_atom,end_atom,other_atoms,max_processors)
            print result
            return result
        else:
            if (len(self.atoms)==1 and other_atoms==[]):
                return False
            else:
                for i in range(start_atom,end_atom+1):
                    atom_pos=[self.atoms[i-1][3],self.atoms[i-1][4],
                                    self.atoms[i-1][5]]
                    atom_pos=self.atom_position_in_box(self.boundaries,atom_pos)
                    compare_with=range(1,end_atom+1) #Atom numbers to compare against (end_atom changed to start_atom as sections shouldn't overlap anyway)
                    compare_with.remove(i)
                    compare_with_iter=RewindableIterator.RewindableIterator(compare_with)
                    for k in compare_with_iter:
                        atom_pos_compare=[self.atoms[k-1][3],self.atoms[k-1][4],
                                    self.atoms[k-1][5]]
                        atom_pos_compare=self.atom_position_in_box(self.boundaries,atom_pos_compare)
                        dist=self.distance(atom_pos,atom_pos_compare)
                        if (dist<=self.atom_diameter*0.9):
                            return True
                        elif (dist>=2*self.atom_diameter): #If we are 2 atom lengths or more away from our target, skip a number of atoms as we are too far for the next one to ever overlap
                            compare_with_iter.seek(int((dist+0.0001)/self.atom_diameter)-1)
                    if(other_atoms!= []):
                        for DNAchain in other_atoms:
                            atom_iter=RewindableIterator.RewindableIterator(DNAchain.atoms)
                            for atom in atom_iter:
                                other_atom_pos=[atom[3],atom[4],atom[5]]
                                other_atom_pos=self.atom_position_in_box(self.boundaries,other_atom_pos)
                                dist=self.distance(atom_pos,other_atom_pos)
                                if (dist<=self.atom_diameter*0.9):
                                    return True
                                elif (int(dist)>=2):
                                    atom_iter.seek(int((dist+0.0001)/self.atom_diameter)-1)
                return False
    
    def overlap_mpi(self,start_atom,end_atom,other_atoms,max_processors):
        comm=MPI.COMM_SELF.Spawn(sys.executable,args=['overlapmpi.py'],maxprocs=max_processors)
        test_atoms=[]
        placed_atoms=[]
        for i in range(start_atom,end_atom+1):
            test_atom_pos=[self.atoms[i-1][3],self.atoms[i-1][4],
                    self.atoms[i-1][5]]
            test_atom_pos=self.atom_position_in_box(self.boundaries,test_atom_pos)
            test_atoms.append(test_atom_pos)
        #Atoms in a section shouldn't overlap so only test against others in system
        for i in range(1,start_atom+1):
            test_atom_pos=[self.atoms[i-1][3],self.atoms[i-1][4],
                    self.atoms[i-1][5]]
            test_atom_pos=self.atom_position_in_box(self.boundaries,test_atom_pos)
            placed_atoms.append(test_atom_pos)
        if(other_atoms!= []):
            for DNAchain in other_atoms:
                for atom in DNAchain.atoms:
                    other_atom_pos=[atom[3],atom[4],
                                    atom[5]]
                    other_atom_pos=self.atom_position_in_box(self.boundaries,other_atom_pos)
                    placed_atoms.append(other_atom_pos)
        test_atom_array=numpy.array(test_atoms,dtype='d')
        placed_atom_array=numpy.array(placed_atoms,dtype='d')
        test_array_size=numpy.array([len(test_atom_array)],dtype='i')
        comm.Bcast([test_array_size,MPI.INT],root=MPI.ROOT)
        comm.Bcast([test_atom_array,MPI.DOUBLE],root=MPI.ROOT)
        return False

    def add_section(self,new_section,vct_temp):
        self.vectors.append(vct_temp)
        for new_atom in new_section:
            self.atoms.append(new_atom)
            
    def assign_all(self,atom_no,connect=False):
        """Set up the correct angle, bond and velocity initial conditions, and
        whether there should be a bond connecting the chain end to the next chain"""
        self.assign_velocities(atom_no)
        self.assign_angles(atom_no,connect)
        self.assign_bonds(atom_no,connect)
        
    def assign_angles(self,atom_no,connect):
        """Set up angles between all points on the chain, as well as to the first part of the next chain,
        if they are supposed to be connected"""
        for i in range(1,atom_no-1):
            self.angles.append([i,self.angle_type,i,i+1,i+2])
        """1 gets auto-converted to boolean for the comparison ,so have to check type as well"""
        if (connect==2 or connect==True and type(connect)==bool): 
            if (atom_no>1):
                self.angles.append([atom_no-1,self.angle_type,atom_no-1,atom_no,atom_no+1])
            self.angles.append([atom_no,self.angle_type,atom_no,atom_no+1,atom_no+2])
        elif (connect==1):
            self.angles.append([atom_no-1,self.angle_type,atom_no-1,atom_no,atom_no+1])
    
    def assign_bonds(self,atom_no,connect):
        """Set up bonds between all points on the chain, as well as to the first part of the next chain,
        if they are supposed to be connected"""
        for i in range(1,atom_no):
            self.bonds.append([i,1,i,i+1])
        if (connect in [1,2,True]):
            self.bonds.append([atom_no,1,atom_no,atom_no+1])
        if self.extra_bonds is not None:
            for bond in self.extra_bonds:
                self.bonds.append(bond)

    def assign_velocities(self,atom_no):
        for i in range(atom_no):
            self.velocities.append([i+1,0,0,0])
            
    def atom_position_in_box(self,boundaries,atom_position):
        """Return the atom's position within the simulation boundary"""
        in_box_position=[]
        maxbdrys=[boundaries[i] for i in range(1,7,2)]
        minbdrys=[boundaries[i] for i in range(0,6,2)]
        for minbdry,maxbdry,pos in zip(minbdrys,maxbdrys,atom_position):
            bdry_length=maxbdry-minbdry
            box_id=0
            if(pos>maxbdry):
                box_id=1*int((abs(pos)+bdry_length/2.0)/(bdry_length))
            elif(pos<minbdry):
                box_id=-1*int((abs(pos)+bdry_length/2.0)/(bdry_length))
            in_box_position.append(pos - box_id*bdry_length)
        return in_box_position

    def distance(self,list1,list2):
        """Distance between two points with form (x1,y1,z1),(x2,y2,z2)"""
        dist=math.sqrt((list1[0]-list2[0])**2+(list1[1]-list2[1])**2+
                       (list1[2]-list2[2])**2)
        return dist

    def extra_section_test(self):
        """Loops are made with an even number of sections. Loops with an odd
        number of sections must split one vector into two parts"""
        self.extra_section=False
        if(self.completed_atom_no/self.section_length%2!=0): 
            self.extra_section=True
    
    def find_start_point(self,boundaries,boundary_style):
        """If start point has not been given, call method to place start
        in centre of simulation region"""
        #Cylinder style boundaries have 3 args (r,zmax,zmin)
        if(boundary_style=='cylinder'):
            zmax=boundaries[2]
            zmin=boundaries[1]
            z_point=(zmax+zmin)/2.0
            start_point=[0,0,z_point]
            return start_point

        #Rectangular style has 6 args (xmin,xmax,ymin,ymax,zmin,zmax)
        elif(boundary_style=='rectangular'):
            start_point=[]
            for i in range(0,6,2):
                mid_point=(boundaries[i+1]+boundaries[i])/2.0
                start_point.append(mid_point)
            return start_point
        else: 
            raise CreationError("Boundary style not specified")
        
    def mainloop(self):
        """Create the atoms of the DNA object"""
        raise NotImplementedError
            
    def modulo1(self,number,mod):
        if mod==0:
            return 1
        if(number%mod==0):
            return mod
        else: return number%mod
        
    def next(self):
        """Create next point/section of DNA object"""
        raise NotImplementedError

    def section_length_check(self):
        """Check that the length specified allows for complete sections"""
        if(self.completed_atom_no%self.section_length!=0 and self.style != 'linear'):
            warnings.warn('Total size of dna is not a multiple of section_length.')
        elif (self.completed_atom_no%self.section_length!=0 and self.style == 'linear'):
            self.completed_section_no += 1

    def set_boundaries_and_start_point(self,boundaries,boundary_style,start_point):
        """Set up boundary style and start point if not already specified"""
        if(len(boundaries)==3 and boundary_style=='rectangular-start'):
            new_boundaries=[0 for i in range(6)]
            for i in range(0,6,2):
                new_boundaries[i]=start_point[i/2]-boundaries[i/2]
                new_boundaries[i+1]=start_point[i/2]+boundaries[i/2]
            boundaries=new_boundaries
        elif(len(boundaries)==3):
            boundary_style='cylinder'
        elif(len(boundaries)==6):
            boundary_style='rectangular'
            
        if(start_point==None):
            start_point=self.find_start_point(boundaries,boundary_style)
        if(self.in_boundary([[0,0,0,start_point[0],start_point[1],start_point[2]]])==False):
            raise CreationError("Start Point is out of bounds")
        return boundaries,boundary_style,start_point
    
    def single_overlap(self,atom,other_atoms,atom_diameter):
        """Return true if atom positions overlap"""
        atom_pos=[atom[3],atom[4],atom[5]]
        for k in range(len(self.atoms)):
            atom_pos_compare=[self.atoms[k][3],self.atoms[k][4],
                                self.atoms[k][5]]
            if (self.distance(atom_pos,atom_pos_compare)<=atom_diameter):
                return True
        if(other_atoms!= []):
            for DNAchain in other_atoms:
                for atom in DNAchain.atoms:
                    other_atom_pos=[atom[3],atom[4],
                                            atom[5]]
                    if (self.distance(atom_pos,other_atom_pos)<=atom_diameter):
                        return True
        return False

    def split_vector(self):
        """Return two vectors which could replace a randomly chosen vector"""
        difference=self.vectors[random.randrange(0,len(self.vectors))]
        halfway=vector.vector.multiply(difference,0.5)
        bisector=vector.vector.normal_vector(halfway,difference.length()*math.sqrt(3)/2.0) #l**2/4+d**2=l**2
        st_to_mid=vector.vector.add(halfway,bisector)
        bisector=vector.vector.multiply(bisector,-1)
        mid_to_end=vector.vector.add(halfway,bisector)
        return difference,st_to_mid,mid_to_end
 
    def switch_atom_types(self,switch_data):
        """Switch data specifies individual atoms in a section to have their type changed."""
        if(type(switch_data)==str):
            self.switch_atom_types_from_file(switch_data)
        else:
            for i in range(0,len(switch_data),2):
                switch_id=switch_data[i]
                new_type=switch_data[i+1]
                self.atoms[switch_id-1][2]=new_type
                
    def switch_atom_types_from_file(self,switch_data):
        with open(switch_data,'r') as r:
            for line in r:
                tokens=line.split()
                for i in range(len(tokens)):
                    tokens[i]=int(tokens[i])
                for atom in self.atoms:
                    if(atom[self.atomdict['id']]==tokens[0]):
                        atom[self.atomdict['type']]=tokens[1]

    def to_supercoil_format(self):
        for i in range(len(self.atoms)):
            col2=self.atoms[i].pop(1)
            self.atoms[i].append(col2)
            self.atoms[i].append(0)
            self.atoms[i].append(1)
        for i in range(len(self.velocities)):
            self.velocities[i].extend([0,0,0])
    
    def remove_section(self,remove_section,vct_temp):
        self.vectors.remove(vct_temp)
        for rem_atom in remove_section:
            self.atoms.remove(rem_atom) 
   
    def restart_check(self,try_no,start_point,atom_diameter,section_length,atom_no,other_atoms,atom_type,extra_section=False):
        """Check if program can continue adding points, restart if it cannot"""
        raise NotImplementedError
          
class CreationError(Exception):
    def __init__(self,val,fatal=False):
        self.val=val
        print (val)
        if fatal is True:
            quit()
    def __str__(self):
        return repr(self.val)

