import vector
import numpy
import math
import Dna
import random
import RewindableIterator
class Dnaend(Dna.Dna): 
    
    def __init__(self,atom_no,boundaries,boundary_style=None,other_atoms=[],atom_type=1,switch_data=[],
                 start_point=None,start_point_minus_one=None,theta=0,phi=0,sections=10,atom_diameter=1,
                 section_length=1,connect=False,in_bounds=True,overlap=False,style='linear-endpt',name='dnaend',
                 extra_bonds=None):
        super(Dnaend,self).__init__(atom_no,boundaries,boundary_style,other_atoms,atom_type,switch_data,
                                    start_point,start_point_minus_one,atom_diameter,section_length,connect,
                                    in_bounds,overlap,style,name,extra_bonds)

        self.atomdict={'id':0,'mol':1,'type':2,'x':3,'y':4,'z':5}
        self.phi=phi
        self.sections=sections
        self.theta=theta
        if connect == False:
            self.completed_angle_no = atom_no - 2
        else:
            self.completed_angle_no = atom_no - 1

        self.section_length_check()
        self.end_point=self.endpoint()
        print 'start: ',self.start_point,' end: ',self.end_point
        self.vectorlist2(self.completed_atom_no,self.section_length,self.sections)

        # Currently has first vector assigned as [1,0,0]
        self.assigned_first_vector = numpy.random.choice(self.vectors)
        print self.assigned_first_vector
        self.first_section(other_atoms,self.assigned_first_vector)

        self.mainloop(other_atoms)
        
        self.assign_all(atom_no,connect)
        self.switch_atom_types(switch_data)
        
    
        
    def mainloop(self,other_atoms):
        try_no=1
        def next(current_atom_no,other_atoms,antiparallel_ends=False):
            """Add the next vector to the chain. If antiparallel_ends is True, this forces the first and last vectors
            to point in opposite directions"""
            new_section=[]
            if antiparallel_ends == False:
                vct=self.vectors[random.randrange(0,len(self.vectors))]
            else:
                if current_atom_no == atom_start_iter.getindex(-1):
                    vct=self.vectors[0]
                else:
                    vct=self.vectors[random.randrange(1,len(self.vectors))]

            current_position=vector.vector(self.atoms[current_atom_no-1][3],self.atoms[current_atom_no-1][4],self.atoms[current_atom_no-1][5])
            for i in range(self.section_length):
                current_position=vector.vector.add(current_position,vct)
                new_section.append([current_atom_no+i+1,1,self.atom_type,current_position.x,current_position.y,current_position.z])
            self.add_section(new_section,vct)
            return new_section,vct
        
        start_atom_id_list=[self.section_length*i for i in range(1,self.completed_section_no)]    
        atom_start_iter=RewindableIterator.RewindableIterator(start_atom_id_list)
        for start_id in atom_start_iter:
            # Add next set of atoms
            new_section,vct_temp=next(start_id,other_atoms,True)

            end_id=start_id+self.section_length

            if(self.overlap_allowed==True and int(self.in_boundary(new_section))*int(self.in_bounds)==int(self.in_bounds)):
                """Success, but only because there's nothing really to go wrong (apart from being out of boundaries)"""
                try_no=1
            elif(int(self.in_boundary(new_section))*int(self.in_bounds)==int(self.in_bounds) and
               self.overlap(start_id,end_id,other_atoms)==False):
                """Successfully placed atoms within overlap/boundary restrictions"""
                try_no=1
            elif(int(self.in_boundary(new_section))*int(self.in_bounds)!=int(self.in_bounds) or
                 self.overlap(start_id,end_id,other_atoms)==True):
                """Failed to place atoms for a particular section. Remove section and vector from list."""
                self.remove_section(new_section,vct_temp)
                try_no=try_no+1
                if (try_no>4*len(self.vectors)):
                    """Unsucessful after several tries, restart whole DNA object"""
                    print 'Failed at {0:d} atoms'.format(len(self.atoms))
                    self.atoms=[]
                    self.vectors=[]
                    self.end_point = self.endpoint()
                    self.vectorlist2(self.completed_atom_no,self.section_length,self.sections)
                    self.extra_section_test()
                    self.first_section(other_atoms,self.assigned_first_vector)
                    atom_start_iter.seek(0,True)
                else:
                    """Unsuccessful, try again with alternative vector (if available)"""
                    atom_start_iter.repeat()
                
    def first_section(self,other_atoms,assigned_vector=None):
        """Make the first section of atoms"""
        self.restart_count+=1
        def trial_first_section():
            first_section=[]
            if(self.start_point_minus_one==None):
                """Don't worry about direction of the chain if there aren't any atoms in it yet!"""
                current_position=vector.vector(self.start_point[0],self.start_point[1],self.start_point[2])
                first_vector=vector.vector.random_vector(self.atom_diameter)
            
            else:
                current_position=vector.vector(self.start_point[0],self.start_point[1],self.start_point[2])
                previous_position=vector.vector(self.start_point_minus_one[0],self.start_point_minus_one[1],self.start_point_minus_one[2])
                first_vector=vector.vector.random_vector_selfavd(current_position,previous_position,self.atom_diameter)
            if assigned_vector is not None:
                if type(first_vector) is list:
                    first_vector = vector.vector(assigned_vector[0],assigned_vector[1],assigned_vector[2])
                else:
                    first_vector = self.assigned_first_vector
            self.vectors.append(first_vector)
            first_position=vector.vector.add(current_position,first_vector)
            
            for i in range(self.section_length):
                first_section.append([i+1,1,self.atom_type,first_position.x,first_position.y,first_position.z])
                first_position=vector.vector.add(first_position,first_vector)
            
            return first_section,first_vector
        
        first_section,first_vector=trial_first_section()
        self.add_section(first_section,first_vector)
        
        if(self.overlap_allowed==False):
            """If atoms are not allowed to overlap, check their positions"""
            while(self.overlap(1,len(self.atoms),other_atoms)==True):
                """If any of the new atoms overlap existing ones, try again"""
                self.restart_count+=1
                self.remove_section(first_section,first_vector)
                first_section,first_vector=trial_first_section()
                self.add_section(first_section,first_vector)
                if(self.restart_count>self.restart_limit):
                    raise CreationError("Couldn't create first section")
        first_vector.negative()
        # Put this vector in position 0 so the antiparallel option in next() works. Otherwise vectors which fail
        # could get added to the end of the self.vectors list
        self.vectors.insert(0,first_vector)
        print first_vector,self.vectors[-1]
 
    def add_section(self,new_section,vct_temp):
        self.vectors.remove(vct_temp)
        for new_atom in new_section:
            self.atoms.append(new_atom)
            print new_atom
            
    def end_end_test(self):
        """Print the distance from start to end and component vectors""" 
        vsum=vector.vector(0,0,0)
        for i in range(len(self.vectors)):
            print self.vectors[i]
            vsum=vector.vector.add(vsum,self.vectors[i])
        print vsum
   
    def endpoint(self):
        """Adds vectors from start to end and returns the end_point"""
        start_vec=vector.vector(self.start_point[0],self.start_point[1],self.start_point[2])
        length=self.sections*self.section_length
        to_end=vector.vector.angle_vector(self.theta,self.phi,length)
        end_vec=vector.vector.add(start_vec,to_end)
        append_vec=vector.vector.multiply(to_end,1.0/float(self.sections*self.section_length))
        for i in range(self.sections):
            self.vectors.append(append_vec)
        return end_vec
           
    def remove_section(self,remove_section,vct_temp):
        self.vectors.append(vct_temp)
        for rem_atom in remove_section:
            self.atoms.remove(rem_atom) 
        
    def vectorlist(self,atom_no,section_length,sections):
        """Create list of vectors by continually splitting the initial start->end vectors"""
        vector_no=atom_no/section_length - sections
        for i in range(vector_no):
            vector_remove,vector_one,vector_two=self.split_vector()
            self.vectors.append(vector_one)
            self.vectors.append(vector_two)
            self.vectors.remove(vector_remove)
            
    def vectorlist2(self,atom_no,section_length,sections):
        """Create list of vectors by adding a loop vectorlist to initial start->end vectors"""
        vector_no=atom_no/section_length-sections

        for i in range(vector_no/2-1):
            new_vector=vector.vector.random_vector()
            self.vectors.append(new_vector)
            new_vector_rev=new_vector.negative_return()
            self.vectors.append(new_vector_rev)
        if(vector_no%2==1):
            vector_remove,vector_one,vector_two=self.split_vector()
            self.vectors.append(vector_one)
            self.vectors.append(vector_two)
            self.vectors.remove(vector_remove)
            


            