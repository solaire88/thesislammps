#!/bin/bash
# Move to lammps src directory and compile with packages required by simulations
rootdir=${PWD}
lammpssrc='lammps/src/'
lammpspy='lammps/python/'
echo "Checking for required libraries.."
pythonfound=0
mpifound=0
pyqtfound=0
condafound=0
mpi4pyfound=0
matplotlibfound=0
PATH=${HOME}/miniconda2/bin:$PATH
PATH=${rootdir}/miniconda2/bin:$PATH
declare -i programfound 
programfind() {
    if hash $1 2>/dev/null; then
        programfound=1
    else
        programfound=0
    fi
}

programfind python
if [[ "${programfound}" -eq 1 ]]; then echo "Python Distribution Found!"; pythonfound=1;
fi
programfind mpirun
if [[ "${programfound}" -eq 1 ]]; then echo "MPI Library Found!"; mpifound=1;
fi
programfind conda
if [[ "${programfound}" -eq 1 ]]; then echo "Conda Found!"; condafound=1;
fi
declare -i pyqtout
pyqtout=0
if [[ "${pythonfound}" -eq 1 ]] ; then pyqtout=`python -c 'import PyQt4; print "1"'`;
fi
declare -i mpi4pyout
mpi4pyout=0

if [[ "${pythonfound}" -eq 1 && "${mpifound}" -eq 1 ]] ; then mpi4pyout=`python -c 'import mpi4py; print "1"'`;
fi
declare -i mpi4pyout
matplotlibout=0

if [[ "${pythonfound}" -eq 1 ]] ; then matplotlibout=`python -c 'import matplotlib; print "1"'`;
fi

if [[ "${mpi4pyout}" -eq 1 ]] ; then echo "Mpi4Py Package Found!"; mpi4pyfound=1;
fi

if [[ "${matplotlibout}" -eq 1 ]] ; then echo "Matplotlib Package Found!"; matplotlibfound=1;
fi

if [[ "${pyqtout}" -eq 1 ]] ; then echo "PyQt Package Found!"; pyqtfound=1;
fi

if [[ ("${pyqtfound}" -eq 0 && "${condafound}" -eq 0)  || ("${pythonfound}" -eq 0 && "${condafound}" -eq 0) ]] ; then echo ""; echo "Python, Matplotlib or the PyQt package is missing. Download (mini)conda package manager and add required packages?" ; echo ""; echo "To make a selection type the associated number and hit return. e.g. To select 'Yes' type 1, to select 'No' type 2 etc.";
select opt in "Yes (Recommended)" "No"; do
    case $opt in 
	"Yes (Recommended)" ) bit=`uname -p`; name=`uname -s`; if [[ ${bit}=='x86_64' && ${name}=='Linux' ]]; then wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh; chmod 744 Miniconda2-latest-Linux-x86_64.sh; bash Miniconda2-latest-Linux-x86_64.sh; elif [[ ${bit}=='x86' && ${name}=='Linux' ]]; then wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86.sh; chmod 744 Miniconda2-latest-Linux-x86_64.sh; bash Miniconda2-latest-Linux-x86.sh; elif [[ ${name}=='Darwin' ]]; then wget https://repo.continuum.io/miniconda/Miniconda2-latest-MacOSX-x86_64.sh; chmod 744 Miniconda2-latest-MacOSX-x86_64.sh; bash Miniconda2-latest-MacOSX-x86_64.sh; 
fi; echo 'Close and re-open your terminal to complete conda install'; exit;;
	"No" ) echo 'Exiting install script'; exit;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done
fi
if [[ "${pyqtfound}" -eq 0 && "${condafound}" -eq 1  ]] ; then echo""; echo "The PyQt package is missing. Use conda to upgrade packages?" ; echo "To make a selection type the associated number and hit return. e.g. To select 'Yes' type 1, to select 'No' type 2 etc"
select opt in "Yes" "No"; do
    case $opt in
	"Yes" ) conda install pyqt=4; break ;;
	"No" ) echo "Exiting install script"; exit;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done
fi

if [[ "${matplotlibfound}" -eq 0 && "${condafound}" -eq 1  ]] ; then echo ""; echo "The Matplotlib package is missing. Use conda to upgrade packages?" ; echo "To make a selection type the associated number and hit return. e.g. To select 'Yes' type 1, to select 'No' type 2 etc"
select opt in "Yes" "No"; do
    case $opt in
	"Yes" ) conda install --no-update-deps matplotlib ; break ;;
	"No" ) echo "Exiting install script"; exit;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done
fi

if [[ "${mpi4pyfound}" -eq 0 && "${mpifound}" -eq 1 ]]; then
echo " "
echo "Add mpi4py (Python MPI library) to your python distribution? This may require root or admin access to do for all users. This is required if you want to run the python simulation scripts in parallel."
if [[ "${condafound}" -eq 0 ]]; then
select opt in "Yes, add for all users" "Yes, only add for this user" "No"; do
    case $opt in
	"Yes, add for all users" ) cd ${rootdir}'/mpi4py'; python setup.py install; break;;
	"Yes, only add for this user" ) cd ${rootdir}'/mpi4py'; python setup.py install --user; break;;
	"No" ) break;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done
else
select opt in "Yes, add for all users" "Yes, only add for this user" "Yes, add using conda" "No"; do
    case $opt in
	"Yes, add for all users" ) cd ${rootdir}'/mpi4py'; python setup.py install; break;;
	"Yes, only add for this user" ) cd ${rootdir}'/mpi4py'; python setup.py install --user; break;;
	"Yes, add using conda" ) conda install mpi4py=2; break;;
	"No" ) break;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done
fi
fi

echo ""
echo "Moving to ${rootdir}/${lammpssrc}"

cd ${rootdir}/${lammpssrc}
echo " "

echo "To make a selection type the associated number and hit return. e.g. To select 'Essential' type 1, to select 'Standard' type 2 etc."

echo " "

echo 'Install LAMMPS with standard packages or just essential ones?'
skiplammps=0
essential=0
select opt in "Essential (Recommended)" "Standard" "Skip LAMMPS Install"; do
    case $opt in
	"Standard" ) make no-all; make yes-asphere; make yes-body; make yes-class2; make yes-colloid; make yes-coreshell; make yes-dipole; make yes-granular; make yes-kspace; make yes-manybody; make yes-mc; make yes-misc; make yes-molecule; make yes-mpiio; make yes-opt; make yes-peri; make yes-qeq; make yes-replica; make yes-rigid; make yes-shock; make yes-snap; make yes-srd; break;;
	"Essential (Recommended)" ) make no-all; make yes-manybody; make yes-molecule; make yes-rigid; essential=1; break;;
	"Skip LAMMPS Install" ) skiplammps=1; break;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done

if [[ "${skiplammps}" -eq 0 ]]; then
echo " "
echo "Removing old object files before compiling."
make clean-all

echo " "
echo "Compile LAMMPS for single processor, multiple processors (OpenMPI or MPICH) or all versions?
"

programfind mpicxx
mpicompiler=0
if [[ "${programfound}" -eq 1 ]]; then mpicompiler=1
fi
mpichrec="MPICH"
programfind mpichversion
if [[ "${programfound}" -eq 1 && "${mpicompiler}" -eq 1 ]]; then echo ""; echo "It looks like you have an MPICH compiler ready!"; mpichrec="MPICH(Recommended)"
fi
ompirec="OpenMPI"
programfind mpirun.openmpi
if [[ "${programfound}" -eq 1 && "${mpicompiler}" -eq 1 ]]; then echo ""; echo "It looks like you have an OpenMPI compiler ready!"; ompirec="OpenMPI(Recommended)";
fi
all_done=0
while (( !all_done)); do
select opt in "Single" ${ompirec} ${mpichrec} "OpenMPI-Intel" "MPICH-Intel" "Skip"; do
    case $opt in
	"Single" ) make no-mpiio; make mode=shlib serial; echo ""; echo "Compile another library?"; echo "" ; 
	    select opt2 in "Yes" "No"; do 
		case $opt2 in 
		    "Yes" ) if [[ "${essential}" -eq 0 ]]; then make yes-mpiio; fi;  break 2;; 
		    "No" ) break 3 ;;
		esac
	    done ;;
	${ompirec} ) make mode=shlib g++_openmpi; echo ""; echo "Compile another library?"; echo "" ; 
	    select opt2 in "Yes" "No"; do 
		case $opt2 in 
		    "Yes" ) break 2;; 
		    "No" ) break 3 ;;
		esac
	    done ;;
	"MPICH" ) make mode=shlib g++_mpich; echo ""; echo "Compile another library?"; echo "" ;
	    select opt2 in "Yes" "No"; do 
		case $opt2 in 
		    "Yes" ) break 2;; 
		    "No" ) break 3 ;;
		esac
	    done ;;
	"OpenMPI-Intel" ) make mode=shlib icc_openmpi; echo ""; echo "Compile another library?"; echo "" ; 
	    select opt2 in "Yes" "No"; do 
		case $opt2 in 
		    "Yes" ) break 2;; 
		    "No" ) break 3 ;;
		esac
	    done ;;
	"MPICH-Intel" ) make mode=shlib icc_mpich; echo ""; echo "Compile another library?"; echo "" ; 
	    select opt2 in "Yes" "No"; do 
		case $opt2 in 
		    "Yes" ) break 2;; 
		    "No" ) break 3 ;;
		esac
	    done ;;
	# "All" )  make mode=shlib g++_openmpi; make mode=shlib g++_mpich; make mode=shlib icc_openmpi; make mode=shlib icc_mpich; make no-mpiio; make no-python; make mode=shlib g++_serial; break;;
	"Skip") break 2;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done
done
fi

cp liblammps* ../python/

	
echo " "
echo "Install LAMMPS python files and libraries to system python distribution? This will only be possible if you have root access to the current machine, but if not can still be done via the next prompt in this script, or manually."
select yn in "Yes" "No"
do
    case $yn in
	"Yes" ) echo " "; echo "This will attempt to install LAMMPS with root/admin priveleges! You need the root password in order to do this. If you don't have it you may see a scary sounding warning message (but you can ignore it). Do you still want to continue?"; 
	    select yn in "Yes" "No"; do
		case $yn in
		    "Yes" ) echo Moving to ${rootdir}'/'${lammpspy}; cd ${rootdir}'/'${lammpspy}; sudo python installthesisver.py; exit;;
		    "No" ) echo " "; echo "Continuing with install script"; break 2;;
		    *) echo "Invalid option, remember to type the appropriate number to make your selection"
		esac
	    done
	    ;;
	"No" ) echo ""; echo "Continuing with install script"; break;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done

# Add lammps library and python link to appropriate paths

#if [[ $PYTHONPATH != *"${rootdir}/${lammpspy}" && $LD_LIBRARY_PATH != *"${rootdir}/${lammpssrc}" ]]; then
#export PYTHONPATH="$PYTHONPATH:${rootdir}/${lammpspy}"
#export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${rootdir}/${lammpssrc}"
#echo " "
#echo "Do you want to add lammps to your python and library path in ${HOME}/.bashrc? This will mean not having to manually add lammps and the associated libraries to the path every time you want to run it."
#select yn in "Yes" "No"; do
#    case $yn in
#        "Yes" )  echo 'export PYTHONPATH="$PYTHONPATH:'"${rootdir}"'/'"${lammpspy}"'"'>>${HOME}/.bashrc ; echo 'export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:'"${rootdir}"'/'"${lammpssrc}"'"'>>${HOME}/.bashrc ; break;;
#        "No" ) break ;;
#	*) echo "Invalid option, remember to type the appropriate number to make your selection"
#    esac
#done
#fi

cd ${rootdir}

echo ""

echo "Test install? This will try to start up the new libraries and run a few simple commands."

echo ""
select yn in "Yes" "No"; do
    case $yn in
	"Yes" ) if [ ${pythonfound}=true ]; then 
python installtest.py ${mpifound} 
fi
break;;
	"No" ) break;;
	*) echo "Invalid option, remember to type the appropriate number to make your selection"
    esac
done
