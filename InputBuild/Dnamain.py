#!/usr/local/bin/python
import Dnalinear
import Dnaloop
import Dnaend
import Dnalooptwist
import Dnalineartwist
import Dna
import protein
import copy
import sys
import RewindableIterator
import time
import argparse
import Tkinter

class Dnamain(object):
    
    def __init__(self,global_conditions,dna_conditions):
        self.angles=[]
        self.atoms=[]
        self.velocities=[]
        self.bonds=[]
        self.ellipsoids=[]
        self.dna_sections=[]
        self.proton_count=0
        self.dna_count=0
        dict_add={'other_atoms':self.dna_sections}
        dna_conditions_iter=RewindableIterator.RewindableIterator(dna_conditions)
        for dictionary in dna_conditions_iter:
            exit_code=True
            print 'Section {0:d} of {1:d}'.format(dna_conditions_iter.index,len(dna_conditions))
            dictionary,dict_add=self.dict_update_pre(dictionary,dict_add)
            dna_conditions_iter.update(dictionary)
            obj_type=dictionary['style']
            st_time=time.time()
            try:
                if(obj_type=='linear'):
                    temp_section=Dnalinear.Dnalinear(**dictionary)
                    self.dna_sections.append(temp_section)
                    self.dna_count=self.dna_count+dictionary.get('atom_no')
                elif(obj_type=='loop'):
                    temp_section=Dnaloop.Dnaloop(**dictionary)
                    self.dna_sections.append(temp_section)
                    self.dna_count=self.dna_count+dictionary.get('atom_no')
                elif(obj_type=='linear-endpt'):
                    temp_section=Dnaend.Dnaend(**dictionary)
                    self.dna_sections.append(temp_section)
                    self.dna_count=self.dna_count+dictionary.get('atom_no')
                elif(obj_type=='twistedloop'):
                    temp_section=Dnalooptwist.Dnalooptwist(**dictionary)
                    self.dna_sections.append(temp_section)
                    self.dna_count=self.dna_count+dictionary.get('atom_no')
                elif (obj_type == 'twistedlinear'):
                    temp_section = Dnalineartwist.Dnalineartwist(**dictionary)
                    self.dna_sections.append(temp_section)
                    self.dna_count = self.dna_count + dictionary.get('atom_no')
                elif(obj_type=='prot'):
                    self.dna_sections.append(protein.protein(**dictionary))
                    self.proton_count=self.proton_count+dictionary.get('atom_no')
                elif(obj_type=='bdry'):
                    """Create new simulation boundaries based on currently existing atoms"""
                    global_conditions['boundaries']=self.rewrite_boundaries(global_conditions.get('boundaries'),dictionary.get('dynamic_boundaries'),obj_type)
                    #Update dictionaries for still to be created dna sections with new boundaries
                    for d in dna_conditions[dna_conditions_iter.index:]:
                        if d.has_key('boundaries')==True:
                            d['boundaries']=global_conditions['boundaries']
                elif(obj_type=='bdry-centre'):
                    """Create new simulation boundaries based on currently existing atoms, with simulation box roughly centered on the atoms in the simulation"""
                    global_conditions['boundaries'],success=self.rewrite_boundaries(global_conditions.get('boundaries'),dictionary.get('dynamic_boundaries'),obj_type)
                    if success == False:
                        dna_conditions_iter.prev()
                    #Update dictionaries for still to be created dna sections with new boundaries
                    else:
                        for d in dna_conditions[dna_conditions_iter.index:]:
                            if d.has_key('boundaries')==True:
                                d['boundaries']=global_conditions['boundaries']
                elif(obj_type=='bdry-shift'):
                    """Translate the dna objects so they fit in the specified boundaries. If they do not fit,
                    try creating all the dna again"""
                    global_conditions['boundaries'],success=self.rewrite_boundaries(global_conditions.get('boundaries'),dictionary.get('dynamic_boundaries'),obj_type)
                    if success == False:
                        print 'Couldn\'t fit dna object within simulation boundary. Retrying...'
                        dna_conditions_iter.seek(0,absolute=True)
                        self.dna_sections = []
                        self.dna_count = 0
                        # Remove other_atoms values from dictionaries as they aren't relevant anymore
                        for dict in dna_conditions_iter:
                            if dict.has_key('other_atoms'):
                                dict.pop('other_atoms')
                            dna_conditions_iter.update(dict)
                        dict_add = {'other_atoms': self.dna_sections}
                        dna_conditions_iter.seek(0, absolute=True)
                elif(obj_type=='copy'):
                    self.shift_object(**dictionary)
                else:
                    print 'Type of dna not given (linear,loop,end,twistedloop)'    
            except Dna.CreationError:
                    exit_code=False
                    dna_conditions_iter.prev()
                    try:
                        self.dna_sections.remove(self.dna_sections[-1])
                    except IndexError:
                        pass
                    
            if exit_code==True:
                #Add new start point for next dictionary if the section was created successfully
                dict_add=self.dict_update_post(dictionary,dict_add,-1)
            end_time=time.time()-st_time
            print 'Took '+str(end_time)+' s'

        self.correct_all_ids()
        
        if(global_conditions.has_key('switch_data')):
            self.switch_atom_types_from_file(global_conditions['switch_data'])
        
        self.copy_to_fields()    
        self.to_file(args.outfile,global_conditions)
        print 'Finished!'
        
    def atom_id_check(self):
        """If a bond or angle refers to an atom 1 or 2 greater than the highest dna id, change the id's in bonds/angles to link
        to the first dna section"""
        for i,bond in enumerate(self.bonds):
            for j in range(2,4):
                if(bond[j]>self.dna_count):
                    self.bonds[i][j]=bond[j]-self.dna_count
        for i,angle in enumerate(self.angles):
            for j in range(2,5):
                if(angle[j]>self.dna_count):
                    self.angles[i][j]=angle[j]-self.dna_count
                
    def copy_to_fields(self):
        """Put attributes from dna sections in correct fields and set atom ids to correct values"""
        for section in self.dna_sections:
            try:
                self.update_fields(section.bonds,self.bonds)
                self.update_fields(section.angles,self.angles)
            except AttributeError:
                pass
            try:
                self.update_fields(section.ellipsoids,self.ellipsoids)
            except AttributeError:
                pass
            self.update_fields(section.atoms,self.atoms)
            self.update_fields(section.velocities,self.velocities)
        
    def correct_all_ids(self):
        largest_angle_id=0
        largest_atom_id=0
        largest_bond_id=0
        largest_ellipse_id=0
        largest_velocity_id=0
        for section in self.dna_sections:
            section.atoms=self.correct_ids(section.atoms,(section.atomdict['id'],largest_atom_id))
            section.velocities=self.correct_ids(section.velocities,(section.veldict['id'],largest_velocity_id))
            try:
                section.bonds=self.correct_ids(section.bonds,(section.bonddict['id'],largest_bond_id),(section.bonddict['atom1'],largest_atom_id),
                                               (section.bonddict['atom2'],largest_atom_id))
                section.angles=self.correct_ids(section.angles,(section.angledict['id'],largest_angle_id),(section.angledict['atom1'],largest_atom_id),
                                                (section.angledict['atom2'],largest_atom_id),(section.angledict['atom3'],largest_atom_id))
            except AttributeError:
                pass
            try:
                section.ellipsoids=self.correct_ids(section.ellipsoids,(section.elldict['id'],largest_ellipse_id))
            except AttributeError:
                pass
            try:
                largest_atom_id+=len(section.atoms)
                largest_velocity_id+=len(section.velocities)
                largest_bond_id+=len(section.bonds)
                largest_angle_id+=len(section.angles)
                largest_ellipse_id+=len(section.ellipsoids)
            except AttributeError:
                pass

        self.atom_id_check()
        
    def correct_ids(self,section_field,*args):
        """In section_field, shift data in column arg[0] by arg[1]"""
        for line in section_field:
            for arg in args:
                line[arg[0]]=line[arg[0]]+arg[1]
        return section_field
    
    def dict_update_post(self,dictionary,dict_add,link_to):
        """If the current dna link connects to next, add start point to another dictionary"""
        if(dictionary.get('connect') in [1,2,True]):
            dict_add.update(start_point=[self.dna_sections[link_to].atoms[-1][self.dna_sections[link_to].atomdict['x']],self.dna_sections[link_to].atoms[-1][self.dna_sections[link_to].atomdict['y']],self.dna_sections[link_to].atoms[-1][self.dna_sections[link_to].atomdict['z']]])
            if len(self.dna_sections)==1 and len(self.dna_sections[link_to].atoms)==1:
                pass
            elif(len(self.dna_sections[link_to].atoms)==1):
                dict_add.update(start_point_minus_one=[self.dna_sections[link_to-1].atoms[-1][self.dna_sections[link_to-1].atomdict['x']],self.dna_sections[link_to-1].atoms[-1][self.dna_sections[link_to-1].atomdict['y']],self.dna_sections[link_to-1].atoms[-1][self.dna_sections[link_to-1].atomdict['z']]])
            else:
                dict_add.update(start_point_minus_one=[self.dna_sections[link_to].atoms[-2][self.dna_sections[link_to].atomdict['x']],self.dna_sections[link_to].atoms[-2][self.dna_sections[link_to].atomdict['y']],self.dna_sections[link_to].atoms[-2][self.dna_sections[link_to].atomdict['z']]])
        return dict_add
    
    def dict_update_pre(self,dictionary,dict_add):
        
        dictionary.update(dict_add)
        if(dict_add.has_key('start_point')):
            dict_add.pop('start_point')
        if(dict_add.has_key('start_point_minus_one')):
            dict_add.pop('start_point_minus_one')
        return dictionary,dict_add
    
    def rewrite_boundaries(self,box_full,dynamic_boundaries,boundary_type):
        """Get new simulation boundaries using the minimum and maximum x,y,z coordinate from dna in simulation so far, plus a fixed amount. Uses
        dictionary associated to each dna section to find x y or z column"""
        if type(dynamic_boundaries)==int:
            dynamic_boundaries=[dynamic_boundaries for i in range(3)]        
        if dynamic_boundaries==None or dynamic_boundaries==False:
            return box_full,True
        elif boundary_type == 'bdry' or boundary_type == 'bdry-centre' or boundary_type == 'bdry-shift':
            min_max=[]
            for i,dna in enumerate(self.dna_sections):
                atcol=zip(*dna.atoms)
                if(i==0):
                    min_max=[min(atcol[dna.atomdict.get('x')]),max(atcol[dna.atomdict.get('x')]),
                             min(atcol[dna.atomdict.get('y')]),max(atcol[dna.atomdict.get('y')]),
                             min(atcol[dna.atomdict.get('z')]),max(atcol[dna.atomdict.get('z')])]
                else:
                    min_max=[min([min_max[0],min(atcol[dna.atomdict.get('x')])]),max([min_max[1],max(atcol[dna.atomdict.get('x')])]),
                             min([min_max[2],min(atcol[dna.atomdict.get('y')])]),max([min_max[3],max(atcol[dna.atomdict.get('y')])]),
                             min([min_max[4],min(atcol[dna.atomdict.get('z')])]),max([min_max[5],max(atcol[dna.atomdict.get('z')])])]
            if boundary_type == 'bdry':
                for i in range(0,6,2):
                    min_max[i]=min_max[i]-float(dynamic_boundaries[i/2])
                    min_max[i+1]=min_max[i+1]+float(dynamic_boundaries[i/2])
                return min_max,True
            elif boundary_type == 'bdry-centre':
                # If centering the boundaries around (0,0,0) is okay, do that
                for i in range(0,6,2):
                    if(min_max[i+1] < float(dynamic_boundaries[i/2])/2.0 and min_max[i] > -float(dynamic_boundaries[i/2])/2.0):
                        min_max[i+1] = float(dynamic_boundaries[i/2])/2.0
                        min_max[i] = -float(dynamic_boundaries[i/2])/2.0
                    else:
                        length_add = (float(dynamic_boundaries[i/2]) - min_max[i+1] + min_max[i])/2.0
                        if (length_add < 0):
                            return min_max,False
                        min_max[i+1] = min_max[i+1] + length_add
                        min_max[i] = min_max[i] - length_add
                return min_max,True
            elif boundary_type == 'bdry-shift':
                # Try and shift the currently placed atoms into the initial simulation boundaries
                xyz_dict = {0:'x',2:'y',4:'z'}
                for i in range(0,6,2):
                    if(min_max[i+1] < float(dynamic_boundaries[i/2])/2.0 and min_max[i] > -float(dynamic_boundaries[i/2])/2.0):
                        pass
                    else:
                        # Distance from max atom to initial max boundary
                        to_max_bdry = float(dynamic_boundaries[i/2])/2.0 - min_max[i+1]
                        # Distance from min atom to initial min boundary
                        to_min_bdry = -float(dynamic_boundaries[i/2])/2.0 - min_max[i]
                        if to_max_bdry < 0.0 and to_min_bdry < 0.0 and to_max_bdry-to_min_bdry > 0.0:
                            for dna in self.dna_sections:
                                for atom in dna.atoms:
                                    atom[dna.atomdict.get(xyz_dict[i])] -= (to_max_bdry-to_min_bdry)/2.0
                        elif to_max_bdry > 0.0 and to_min_bdry > 0.0 and to_min_bdry-to_max_bdry > 0.0:
                            for dna in self.dna_sections:
                                for atom in dna.atoms:
                                    atom[dna.atomdict.get(xyz_dict[i])] += (to_min_bdry-to_max_bdry)/2.0
                        else:
                            # If the dna sections are outside the box at both sides, report a failure :<
                            return min_max,False
                    min_max[i+1] = float(dynamic_boundaries[i/2])/2.0
                    min_max[i] = -float(dynamic_boundaries[i/2])/2.0
                return min_max,True
    
    def shift_object(self,name,copy_name,shift_by,**args):
        """Copy dna object with name and translate x,y,z coordinates by shift_by[0],[1],[2]"""
        for dna_object in self.dna_sections[:]:
            if(dna_object.name==copy_name):
                dna_temp=copy.deepcopy(dna_object)
                dna_temp.name=name
                for i in range(len(dna_temp.atoms)):
                    dna_temp.atoms[i][dna_temp.atomdict['x']]=dna_temp.atoms[i][dna_temp.atomdict['x']]+shift_by[0]
                    dna_temp.atoms[i][dna_temp.atomdict['y']]=dna_temp.atoms[i][dna_temp.atomdict['y']]+shift_by[1]
                    dna_temp.atoms[i][dna_temp.atomdict['z']]=dna_temp.atoms[i][dna_temp.atomdict['z']]+shift_by[2]
                self.dna_sections.append(dna_temp)
                self.dna_count+=len(dna_object.atoms)
        
    def switch_atom_types_from_file(self,filename):
        with open(filename,'r') as r:
            for line in r:
                tokens=line.split()
                if('#' in tokens[0]):
                    pass
                else:
                    for i in range(len(tokens)):
                        tokens[i]=int(tokens[i])
                    for section in self.dna_sections:
                        for atom in section.atoms:
                            if(atom[section.atomdict['id']]==tokens[0]):
                                atom[section.atomdict['type']]=tokens[1]
                            
    def to_file(self,filename,global_conditions):
        with open(filename,'w') as self.o:
            self.write_input_header(global_conditions)
            for string in ['atoms','velocities','bonds','angles']:
                self.write_name(string)
            if(self.ellipsoids!=[]):
                self.write_name('ellipsoids')
                
    def types_list(self,obj,field_no):
        """Returns all (unique) values in field_no of obj"""
        zipped=zip(*obj)
        return list(set(zipped[field_no]))
    
    def types_no(self,obj,field_no):
        """Returns number of different values in field_no of obj."""
        zipped=zip(*obj)
        return len(set(zipped[field_no]))
    
    def update_fields(self,section_field,update_target):
        for line in section_field:
            update_target.append(line)
            
    def write_input_header(self,global_conditions):
        """This needs testing for compatibility with ellipsoid types. Not sure why non-empty
        self.ellipsoids get removed"""
        
        box_full = global_conditions['boundaries']
        if 'extra_bond_types' in global_conditions.keys():
            extra_bond_types = global_conditions['extra_bond_types']
        else:
            extra_bond_types = 0
            
        if 'extra_bonds_per_atom' in global_conditions.keys():
            extra_bonds_per_atom = global_conditions['extra_bonds_per_atom']
        else:
            extra_bonds_per_atom = 0

        if 'extra_angles_per_atom' in global_conditions.keys():
            extra_angle_per_atom = global_conditions['extra_angles_per_atom']
        else:
            extra_angle_per_atom = 0

        if 'extra_specials_per_atom' in global_conditions.keys():
            extra_special_per_atom = global_conditions['extra_specials_per_atom']
        else:
            extra_special_per_atom = 0
        
        obj_list=[self.atoms,self.bonds,self.angles,self.ellipsoids]
        str_list=['atom','bond','angle','ellipsoid']
        #Col list- which column are the atom/bond/angle types in, in self.atoms/bonds/angles
        col_list=[1,1,1] 
        if(self.ellipsoids==[]):
            obj_list.remove(self.ellipsoids)
            str_list.remove('ellipsoid')
            col_list=[2,1,1]
        self.o.write('#Inputs generated from file: '+args.infile+'\n')
        map(lambda w:self.o.write('{0:d} {1}s\n'.format(len(w[0]),w[1])),zip(obj_list,str_list))
        if(self.ellipsoids!=[]):
            obj_list.remove(self.ellipsoids)
            str_list.remove('ellipsoid')
        self.o.write('\n')
        #map(lambda w:self.o.write('{0:d} {1} types\n'.format(max(self.types_list(w[0],w[2])),w[1])),
        #    zip(obj_list,str_list,col_list))
        for obj,string,col in zip(obj_list,str_list,col_list):
            if string == 'bond':
                self.o.write('{0:d} {1} types\n'.format(max(self.types_list(obj,col))+extra_bond_types,
                                                        string))
            else:
                self.o.write('{0:d} {1} types\n'.format(max(self.types_list(obj,col)),string))
        if(extra_bonds_per_atom != 0):
            self.o.write('{0:d} extra bond per atom\n'.format(extra_bonds_per_atom))
        if (extra_angle_per_atom != 0):
            self.o.write('{0:d} extra angle per atom\n'.format(extra_angle_per_atom))
        if (extra_special_per_atom != 0):
            self.o.write('{0:d} extra special per atom\n'.format(extra_special_per_atom))
        self.o.write('\n')
        self.o.write('{0:.5f} {1:.5f} xlo xhi\n'.format(box_full[0],box_full[1]))
        self.o.write('{0:.5f} {1:.5f} ylo yhi\n'.format(box_full[2],box_full[3]))
        self.o.write('{0:.5f} {1:.5f} zlo zhi\n'.format(box_full[4],box_full[5]))
        self.o.write('\n')
        self.o.write('Masses\n')
        self.o.write('\n')
        #Loop over number of atom types. old_ver:range(1,self.types_no(self.atoms,col_list[0])+1)
        for i in range(1,max(self.types_list(self.atoms,col_list[0]))+1): 
            self.o.write('{0:d} {1:d} \n'.format(i,1))

    def write_name(self,str_name):
        """Write a heading 'Str_name' and the data from self.'str_name'"""
        self.o.write('\n')
        self.o.write(str.title(str_name)+'\n')
        self.o.write('\n') 
        all_attr=getattr(self,str_name)
        for line_attr in all_attr:
            attr_list=list(enumerate(line_attr,start=1))
            for single_attr in attr_list:
                self.o.write(str(single_attr[1]))
                if(single_attr[0]!=len(attr_list)):self.o.write(' ')        
            self.o.write('\n')

if __name__=="__main__":
    
    def commcheck(tokens):
        """Check if line is empty or a comment"""
        if(len(tokens)==0):
            return True
        elif(tokens[0].startswith('#')):
            return True
        return False
    
    def cond(tokens):
        """Read an expression from input file and return as list of string pairs [arg_name,arg_val]"""
        conditions_single=[]
        for token in tokens:
            if(token.count('=')==1):
                token_part=token.partition('=')
                conditions_single.append([token_part[0],token_part[2]])
            elif(token.count('=')>1):
                raise ValueError('Too many "=" in a single input')
        return conditions_single
    
    def input_help():
        """Print information about input file structure in a new window"""
        top = Tkinter.Tk()
        
        message= Tkinter.Message(text="""Writing an input file:
- Set global simulation parameters by starting a line with the word 'all'.

These include: 

- Simulation size (boundaries=xmin,xmax,ymin,ymax,zmin,zmax) or (boundaries=r,zmin,zmax)

- Order to write DNA sections (order=name1,name2,name3,etc.) If this is not given the DNA/proteins will be generated in whatever order they come up in the input file.

- Set individual DNA section parameters by starting a line with 'name = name1'. These are the names referenced in the writing order setting above.

Possible parameters for a section include:

- style=(linear,loop,twistedloop,linear-endpt) choosing linear-endpt gives a section beginning at a start point and ending at a point specified by a length and direction
- boundary_style='rectangular','cylinder','rectangular-start'
- boundaries specific to a DNA section. Same style as global sim. boundaries plus rectangular-start, which takes 3 arguments (xlength,ylength,zlength). Note: xlength = 10 and x start point = 5 will give boundaries of [-5,15]  
- atom_no, the number of atoms
- section_length, all chains are split into smaller sections and this is the length of a single section
- atom_type, an integer
- start_point, 3 coordinates
- connect, Whether the chain connects to the next one (2,1 or False). If there are 2 atoms or greater left in the full dna chain pick 2, while if there is only a single atom left pick 1. If the chain is not connected pick False.
- switch_data, Individual atoms to have their type changed e.g. switch_data=1,2,300,2 switches atoms 1 and 300 to type 2.
- atom_diameter
- twists, no. of twists in loop (twistedloop type only)
- theta/phi/sections, angle and length from start to endpoint. Phi is the angle between vector and z-axis, theta is the angle between the projection of the vector into the xy-plane and the +ve x-axis.
  Start to end length is sections x section_length. phi = 0 and theta = pi/2 points in y direction phi = -pi points in -z direction (linear-endpoint only)

-style can also be set with type=bdry, which will redraw simulation boundaries when called.
-dynamic boundaries (dynamic_boundaries= width of space beyond furthest atom in simulation) 5,10,5 gives buffers of 5 sim units in the x/z-directions and 10 in the y direction

-other style options: bdry-centre, bdry-shift. Centre will set up boundaries which try to fit dna objects. This may require bigger boundaries than initially requested. Shift translates all dna objects to be in bounds, if this is not possible new dna objects are created.

-extra_bond_types,extra_bonds_per_atom,extra_angles_per_atom,extra_specials_per_atom - adds to header of input file

All parameters for a DNA section should be on the same line, separated by spaces and have the form 'parametername=value'

Parameters for proteins in the simulation:

- must have 'style=prot' as first argument
- atom_no
- boundaries
- atom_type
- switch_data
- atom_diameter

If a dna object comes after a protein object, there may be errors when the program tries to check if dna sections connect to the right atoms 
""")
        message.pack()
        top.mainloop()
    
    def ordering(global_conditions,dna_conditions):
        """Places the dictionaries for each DNA section in the order specified by order=1,2,etc"""
        if(global_conditions.has_key('order')):
            temp_dict_list=[]
            for name in global_conditions.get('order'):
                for dicts in dna_conditions:
                    if(dicts.get('name')==name):
                        temp_dict_list.append(copy.deepcopy(dicts))
            #for dicts in temp_dict_list:
            #    dicts.pop('name')
            global_conditions.pop('order')
        else:
            #for dicts in dna_conditions:
            #    if dicts.has_key('name'):
            #        dicts.pop('name')
            return global_conditions,dna_conditions
        return global_conditions,temp_dict_list
    
    def preformat(conditions_list):
        """Convert from string to boolean,int,double or list"""
        for i in range(len(conditions_list)):
            if ',' in conditions_list[i][1]:
                conditions_list[i][1]=conditions_list[i][1].split(',')
                for j in range(len(conditions_list[i][1])):
                    try:
                        conditions_list[i][1][j]=int(conditions_list[i][1][j])
                    except(ValueError):
                        try:
                            conditions_list[i][1][j]=float(conditions_list[i][1][j])
                        except(ValueError):
                            pass
            else:
                try:
                    conditions_list[i][1]=int(conditions_list[i][1])
                except(ValueError):
                    try:
                        conditions_list[i][1]=float(conditions_list[i][1])
                    except(ValueError):
                        pass
            
            if conditions_list[i][1]=='True':conditions_list[i][1]=True
            elif conditions_list[i][1]=='False':conditions_list[i][1]=False

        return conditions_list
    
    parser = argparse.ArgumentParser('Create LAMMPS DNA input files')
    parser.add_argument('-in', dest='infile', type=str, help='File with input parameters for system',default=None)
    parser.add_argument('-out', dest='outfile', type=str, help='Location to write output file',default=None)
    parser.add_argument('-help', dest='writehelp', action='store_true', help='Print detailed help showing the possible options to be set in an input file')
    args = parser.parse_args()
 
    if args.writehelp is True:
        input_help()
        quit()
    elif args.infile is None or args.outfile is None:
        parser.print_usage()
        quit()
 
    global_conditions=[]
    dna_conditions=[]
    
    with open(args.infile,'r') as f_read:
        for line in f_read:
            tokens=line.split()
            if(commcheck(tokens)):
                pass
            elif(tokens[0]=='all' or tokens[0]=='global'):
                global_conditions_single=cond(tokens)
                global_conditions_single=preformat(global_conditions_single)
                global_conditions=dict(global_conditions_single)
            else:
                dna_conditions_single=cond(tokens)
                dna_conditions_single=preformat(dna_conditions_single)
                dna_conditions.append(dict(dna_conditions_single))
    global_conditions,dna_conditions=ordering(global_conditions,dna_conditions)
    for d in dna_conditions:
        if d.has_key('boundaries')==False:
            d['boundaries']=global_conditions['boundaries']
    if global_conditions.has_key('overlap'):
        for d in dna_conditions:
            d['overlap']=global_conditions['overlap']
    Dnamain(global_conditions,dna_conditions)
           
