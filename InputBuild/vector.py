import random
import math
import numpy
import copy

class vector:

    #Constructors
    def __init__(self,x=0,y=0,z=0):
        self.x=x
        self.y=y
        self.z=z
        
    @classmethod
    def angle_vector(cls,theta,phi,length=1.0):
        """Vector with angle theta in xy-plane (azimuth) and phi out of the plane (zenith)""" 
        x=length*math.cos(theta)*math.sin(phi)
        y=length*math.sin(theta)*math.sin(phi)
        z=length*math.cos(phi)
        return cls(x,y,z)
        
    @classmethod    
    def from_list(cls,position_list):
        """List of floats to vector"""
        return cls(position_list[0],position_list[1],position_list[2])
   
    @classmethod
    def random_vector(cls,length=1.0):
        """Random vector constrained to have specified length"""
        x=random.uniform(-length,length)
        y_limit=math.sqrt(length*length-x*x)
        y=random.uniform(-y_limit,y_limit)
        z_abs=math.sqrt(length*length-x*x-y*y)
        z=random.choice([-1,1])*z_abs
        return cls(x,y,z)

    @classmethod
    def random_vector_directed (cls,x_fact,y_fact,z_fact,length=1.0):
        """Rescaled random vector"""
        temp=vector.random_vector(length)
        temp.x=temp.x*x_fact
        temp.y=temp.y*y_fact
        temp.z=temp.z*z_fact
        norm_fact=math.sqrt(length*length/(temp.x*temp.x+temp.y*temp.y+
                                           temp.z*temp.z))
        temp.x=temp.x*norm_fact
        temp.y=temp.y*norm_fact
        temp.z=temp.z*norm_fact
        return cls(temp.x,temp.y,temp.z)
    
    @classmethod
    def random_vector_selfavd(cls,Vectorcurrent,Vectorprevious,length=1.0,distribution=True):
        """Return vector object avoiding previous bead."""
        prev_to_curr=vector.sub(Vectorcurrent,Vectorprevious)
        theta=math.atan2(prev_to_curr.y,prev_to_curr.x)
        phi=math.acos(prev_to_curr.z/length)
        rotate=vector.rot_mat(theta,phi)
        if distribution == False:
            r_vec = vector.rand_vec_sph()
        else:
            r_vec = vector.rand_vec_sph_distribution()
        correct_vector=rotate*r_vec
        return cls(float(correct_vector[0])*length,float(correct_vector[1])*length,float(correct_vector[2])*length)

    #Methods
    @staticmethod    
    def add(Vector1,Vector2):
        x=Vector1.x+Vector2.x
        y=Vector1.y+Vector2.y
        z=Vector1.z+Vector2.z
        return vector(x,y,z)

    @staticmethod
    def cross(Vector1,Vector2,length=1.0):
        try:    
            ratio=Vector1.x/Vector2.x
        except ZeroDivisionError:
            try:
                ratio=Vector1.y/Vector2.y
            except ZeroDivisionError:
                ratio=Vector1.z/Vector2.z
            
        if(Vector1.y==Vector2.y*ratio and Vector1.z==Vector2.z*ratio or Vector1.y==-Vector2.y*ratio and Vector1.z==-Vector2.z*ratio):
            #For parallel or anti-parallel vectors, return any vector normal to either of the inputs
            norm=vector.normal_vector(Vector1,length)
            return norm
        else:
            x=Vector1.y*Vector2.z-Vector1.z*Vector2.y
            y=Vector1.z*Vector2.x-Vector1.x*Vector2.z
            z=Vector1.x*Vector2.y-Vector1.y*Vector2.x
            crossprod=vector(x,y,z)
            crossprod.rescale(length)
            return crossprod

    @staticmethod
    def dot(Vector1,Vector2):
        dot_product=Vector1.x*Vector2.x+Vector1.y*Vector2.y+Vector1.z*Vector2.z
        return dot_product
    
    def length(self):
        mod=math.sqrt(self.x*self.x+self.y*self.y+self.z*self.z)
        return mod
        
    @staticmethod
    def maxdot(curr_z,old_y,old_x,precision):
        """Get a vector perpendicular to current z,while remaining as close to the
        previous x vector as possible"""
        trial_vec=vector.cross(old_y,curr_z)
        curr_dotprod=vector.dot(old_x,trial_vec)
        rot_angle=2.0*math.pi/precision
        #Increase angle 'forward' in steps of rot_angle,looking for the closest to previous x axis vector each time 
        for i in range(1,1+precision/2):
            new_vec=trial_vec.rotate_about_vector(curr_z,i*rot_angle)
            if(curr_dotprod>vector.dot(new_vec,old_x)):
                trial_vec1=trial_vec.rotate_about_vector(curr_z,(i-1)*rot_angle)
                break
            curr_dotprod=vector.dot(new_vec,old_x)
        #Increase angle 'backward' in steps of rot_angle,looking for the closest to previous x axis vector each time
        for i in range(-1,-1-precision/2,-1):
            new_vec=trial_vec.rotate_about_vector(curr_z,i*rot_angle)
            if(curr_dotprod>vector.dot(new_vec,old_x)):
                trial_vec2=trial_vec.rotate_about_vector(curr_z,(i+1)*rot_angle)
                break
            curr_dotprod=vector.dot(new_vec,old_x)
        if(vector.dot(trial_vec1,old_x)>vector.dot(trial_vec2,old_x)):
            return trial_vec1,vector.dot(trial_vec1,old_x)
        else: return trial_vec2,vector.dot(trial_vec2,old_x)        
        
    @staticmethod
    def multiply(Vector1,a):
        x=Vector1.x*a
        y=Vector1.y*a
        z=Vector1.z*a
        return vector(x,y,z)
    
    def negative(self):
        self.x=-self.x
        self.y=-self.y
        self.z=-self.z
        
    def negative_return(self):
        x=-self.x
        y=-self.y
        z=-self.z
        return vector(x,y,z)
    
        
    @staticmethod
    def normal_vector(Vector1,length=1.0):
        """Return a vector normal to the input."""
        x=0
        y=0
        z=0
        a=[Vector1.x,Vector1.y,Vector1.z]          
        if(a.count(0.0)==2):
            def result(length):
                zero=0.0
                first=random.uniform(-length,length)
                second_limit=math.sqrt(length*length-first*first)
                second=random.uniform(-second_limit,second_limit)
                return zero,first,second
            if(abs(a[0])!=0.0):
                x,y,z=result(length)
            elif(abs(a[1])!=0.0):
                y,z,x=result(length)
            elif(abs(a[2])!=0.0):
                z,x,y=result(length)
        elif(a.count(0.0)==1):
            def result(length,n):
                first=random.uniform(-length,length)
                second=-a[(n+1)%3]*first/a[(n+2)%3]
                zero=random.uniform(-length,length)
                return zero,first,second
            if(abs(a[0])==0.0):
                x,y,z=result(length,0)
            elif(abs(a[1])==0.0):
                y,z,x=result(length,1)
            elif(abs(a[2])==0.0):
                z,x,y=result(length,2)
        else:        
            x=random.uniform(-length,length)
            y_limit=math.sqrt(length*length-x*x)
            y=random.uniform(-y_limit,y_limit)
            z=(-Vector1.x*x-Vector1.y*y)/Vector1.z
        #Normalise
        currentlength=math.sqrt(x*x+y*y+z*z)
        x=x*length/currentlength
        y=y*length/currentlength
        z=z*length/currentlength
        return vector(x,y,z)
    
    @staticmethod
    def rand_vec_sph():
        """ Return vector with theta between -pi and pi and phi between 0 and 2*pi/3. i.e. a cone pointing along the z axis. Since this will just be used in a multiplication with a rotation matrix, return as numpy matrix.""" 
        th_rand=random.uniform(-math.pi,math.pi)
        phi_rand=random.uniform(0,2.0*math.pi/3.0)
        return numpy.matrix([[math.cos(th_rand)*math.sin(phi_rand)],
            [math.sin(th_rand)*math.sin(phi_rand)],[math.cos(phi_rand)]])
    
    @staticmethod
    def rand_vec_sph_distribution():
        """ Return vector with theta between -pi and pi and phi between 0 and 2*pi/3. i.e. a cone pointing along the z axis. Since this will just be used in a multiplication with a rotation matrix, return as numpy matrix.""" 
        th_rand=random.uniform(-math.pi,math.pi)
        while True:
            phi_rand=numpy.random.normal(0.0,1.0,1)
            if -2.0*math.pi/3.0 < phi_rand < 2.0*math.pi/3.0:
                break
            
        return numpy.matrix([[math.cos(th_rand)*math.sin(phi_rand)],
            [math.sin(th_rand)*math.sin(phi_rand)],[math.cos(phi_rand)]])
    
    #def replace_group(vector_list):
    #    """Take a list of vectors and return a list of the same length, where the sum of the two lists are equal"""
        
    def rescale(self,new_length):
        old_length=self.length()
        n=new_length/old_length
        self.x=self.x*n
        self.y=self.y*n
        self.z=self.z*n

    def rotate_about_vector(self,r,angle):
        c=math.cos(angle)
        s=math.sin(angle)
        x=r.x*(r.x*self.x+r.y*self.y+r.z*self.z)*(1.0-c)+self.x*c+(r.y*self.z-r.z*self.y)*s
        y=r.y*(r.x*self.x+r.y*self.y+r.z*self.z)*(1.0-c)+self.y*c+(r.z*self.x-r.x*self.z)*s
        z=r.z*(r.x*self.x+r.y*self.y+r.z*self.z)*(1.0-c)+self.z*c+(r.x*self.y-r.y*self.x)*s
        return vector(x,y,z)
     
    @staticmethod       
    def rot_mat(theta,phi):
        """Returns matrix transforming z-axis to direction from previous bead"""
        th_mat=vector.rot_mat_sgl('theta',theta)
        phi_mat=vector.rot_mat_sgl('phi',phi)
        rotation=th_mat*phi_mat
        return rotation
    
    @staticmethod        
    def rot_mat_sgl(ang_type,radians):
        c=math.cos(radians)
        s=math.sin(radians)
        if (ang_type=='theta'):
            return numpy.matrix([[c,-s,0],[s,c,0],[0,0,1]])
        elif (ang_type=='phi'):
            return numpy.matrix([[c,0,s],[0,1,0],[-s,0,c]])
        else:
            raise ValueError("Angle type not theta or phi")

    def __str__(self):
        return str(self.x)+','+str(self.y)+','+str(self.z)
        
    @staticmethod
    def sub(Vector1,Vector2):
        x=Vector1.x-Vector2.x
        y=Vector1.y-Vector2.y
        z=Vector1.z-Vector2.z
        return vector(x,y,z)
    
    @staticmethod
    def sum_list(input_list):
        current=vector()
        for v in input_list:
            current=vector.add(current,v)
        return current
    
    def to_list(self):
        return [self.x,self.y,self.z]
