import Dna
import random
import math
class protein(Dna.Dna):
    def __init__(self,atom_no,boundaries,boundary_style=None,other_atoms=[],
                 atom_type=1,switch_data=[],atom_diameter=1,style='prot',name='protein'):
        self.atoms=[]
        self.atomdict={'id':0,'type':1,'mol':2,'x':3,'y':4,'z':5}
        self.name=name
        self.veldict={'id':0,'xvel':1,'yvel':2,'zvel':3}
        self.velocities=[]
        current_atom_no=1
        boundary_style=self.set_boundary_style(boundaries)
        while(len(self.atoms)<atom_no):
            next_atom=self.next(boundaries,boundary_style,
                                    atom_diameter,current_atom_no,atom_type)
            if(self.single_overlap(next_atom,other_atoms,atom_diameter)==False):
                self.atoms.append(next_atom)
                self.velocities.append([current_atom_no,0,0,0])
                current_atom_no=current_atom_no+1
            else:
                pass
    
    def next(self,boundaries,boundary_style,atom_diameter,current_atom_no,
             atom_type):
        position=[0,0,0]
        if(boundary_style=='rectangular'):
            for i in range(0,3,1):
                position[i]=random.uniform(boundaries[2*i]+atom_diameter,boundaries[2*i+1]-atom_diameter)
        if(boundary_style=='cylinder'):
            position[0]=random.uniform(-boundaries[0]+atom_diameter,boundaries[0]-atom_diameter)
            max_position_1=math.sqrt(boundaries[0]*boundaries[0]-position[0]*position[0])
            position[1]=random.uniform(-max_position_1+atom_diameter,max_position_1-atom_diameter)
            position[2]=random.uniform(boundaries[1]+atom_diameter,boundaries[2]-atom_diameter)
        return [current_atom_no,1,atom_type,position[0],position[1],position[2]]
    
    def set_boundary_style(self,boundaries):
        if(len(boundaries)==3):
            boundary_style='cylinder'
        elif(len(boundaries)==6):
            boundary_style='rectangular'
        return boundary_style