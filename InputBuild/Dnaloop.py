import vector
import math
import Dna
import random
import RewindableIterator

class Dnaloop(Dna.Dna):
    
    def __init__(self,atom_no,boundaries,boundary_style=None,other_atoms=[],atom_type=1,switch_data=[],
                   start_point=None,start_point_minus_one=None,atom_diameter=1,section_length=1,connect=False,
                   in_bounds=True,overlap=False,style='loop',name='dnaloop',extra_bonds=None):
        super(Dnaloop,self).__init__(atom_no,boundaries,boundary_style,other_atoms,atom_type,switch_data,
                                        start_point,start_point_minus_one,atom_diameter,section_length,connect,
                                        in_bounds,overlap,style,name,extra_bonds)
        # Variables specific to Dnaloop
        self.atomdict={'id':0,'mol':1,'type':2,'x':3,'y':4,'z':5}
        self.extra_section=False
        self.loop_offset_vector=vector.vector()
        
        # Check DNA attributes aren't contradictory
        self.extra_section_test() 
        self.section_length_check()

        # Return error if there will not be enough vectors created
        if (atom_no/section_length < 6):
            raise Dna.CreationError('There are too few sections to create a loop, try reducing the'
                                    'value for section_length in the input file',fatal=True)
        
        # Initial sections and vectors
        
        self.first_section(other_atoms)   
    
        # Main Loop
        
        self.mainloop(other_atoms)
              
        # If all atoms are placed sucessfully, add bonds and change any atoms which are designated to be an alternative type
        self.assign_all(atom_no,connect)
        self.switch_atom_types(switch_data)
            
    def first_section(self,other_atoms):
        """Create two vectors and a vector representing the path from the start of the loop to the end."""
        self.restart_count+=1
        def trial_first_sections():
            first_section=[]
            last_section=[]
            if(self.start_point_minus_one==None):
                current_position=vector.vector(self.start_point[0],self.start_point[1],self.start_point[2])
                first_vector=vector.vector.random_vector(self.atom_diameter)
                last_vector=vector.vector.random_vector(self.atom_diameter)
                
            else:
                current_position=vector.vector(self.start_point[0],self.start_point[1],self.start_point[2])
                previous_position=vector.vector(self.start_point_minus_one[0],self.start_point_minus_one[1],self.start_point_minus_one[2])
                first_vector=vector.vector.random_vector_selfavd(current_position,previous_position,self.atom_diameter)
                last_vector=vector.vector.random_vector_selfavd(current_position,previous_position,self.atom_diameter)
            first_position=vector.vector.add(current_position,first_vector)
            last_position=vector.vector.add(current_position,last_vector)
            
            for i in range(self.section_length):
                first_section.append([i+1,1,self.atom_type,first_position.x,first_position.y,first_position.z])
                last_section.append([self.completed_atom_no-i,1,self.atom_type,last_position.x,last_position.y,last_position.z])
                first_position=vector.vector.add(first_position,first_vector)
                last_position=vector.vector.add(last_position,last_vector)
            end_target_vec=vector.vector.random_vector_selfavd(last_vector,vector.vector(),self.atom_diameter)
            end_target_pos=vector.vector.add(last_position,end_target_vec)
            end_target=[self.completed_atom_no-self.section_length-1,1,1,end_target_pos.x,end_target_pos.y,end_target_pos.z]
            
            #Divide by 1.0/self.section_length since other vectors are length=1, but actually represent a vector of length=self.section_length
            self.loop_offset_vector=vector.vector.multiply(end_target_vec,1.0/self.section_length)
                
            return first_section,first_vector,last_section,last_vector,end_target,end_target_vec
        
        first_section,first_vector,last_section,last_vector,end_target,end_target_vec=trial_first_sections()
        self.add_section(first_section,first_vector)
        #Since this vector is part of the other 'side' of the loop, add it's reverse to the vector list
        last_vector.negative()
        self.add_section(last_section,last_vector)
        
        if(self.overlap_allowed==False):
            while(self.overlap(1,len(self.atoms),other_atoms)==True or self.single_overlap(end_target,other_atoms,self.atom_diameter)==True):
                #If any of the new atoms overlap existing ones, try again
                self.restart_count+=1
                self.remove_section(first_section,first_vector)
                self.remove_section(last_section,last_vector)
                first_section,first_vector,last_section,last_vector,end_target,end_target_vec=trial_first_sections()
                self.add_section(first_section,first_vector)
                #Since this vector is part of the other 'side' of the loop, add it's reverse to the vector list
                last_vector.negative()
                self.add_section(last_section,last_vector)
                if(self.restart_count>self.restart_limit):
                    raise Dna.CreationError("Couldn't create first section")
        self.loop_offset_vector=vector.vector.multiply(end_target_vec,1.0/self.section_length)
                
    def mainloop(self,other_atoms):
        try_no=1
        vector_list_finished=False
        shifted=False
        def next(current_atom_no,other_atoms,vector_list_finished): 
            new_section=[]
            current_position=vector.vector(self.atoms[current_atom_no-1][3],self.atoms[current_atom_no-1][4],self.atoms[current_atom_no-1][5])
            if(current_atom_no!=1):
                previous_position=vector.vector(self.atoms[current_atom_no-2][3],self.atoms[current_atom_no-2][4],self.atoms[current_atom_no-2][5])
                vct=self.vector_select(current_position,previous_position,vector_list_finished)
            else:
                vct=vector.vector.random_vector()
            for i in range(self.section_length):
                current_position=self.updateposition(current_position,vct,vector_list_finished)
                new_section.append([current_atom_no+i+1,1,self.atom_type,current_position.x,current_position.y,current_position.z])
            self.add_section(new_section,vct,vector_list_finished)
            return new_section,vct
        #Since we have already placed 2 sections,list runs to self.completed_section_no-1
        start_atom_id_list=[self.section_length*i for i in range(1,self.completed_section_no-1)]    
        atom_start_iter=RewindableIterator.RewindableIterator(start_atom_id_list)
        for start_id in atom_start_iter:
            
            new_section,vct_temp=next(start_id,other_atoms,vector_list_finished)
            end_id=start_id+self.section_length
            
            if(self.overlap_allowed==True and int(self.in_boundary(new_section))*int(self.in_bounds)==int(self.in_bounds)):
                #Success, but only because there's nothing really to go wrong (apart from being out of boundaries)
                try_no=1
            elif(int(self.in_boundary(new_section))*int(self.in_bounds)==int(self.in_bounds) and
               self.overlap(start_id,end_id,other_atoms)==False):
                #Successfully placed atoms within overlap/boundary restrictions
                try_no=1
            elif(int(self.in_boundary(new_section))*int(self.in_bounds)!=int(self.in_bounds) or
               self.overlap(start_id,end_id,other_atoms)==True):
                #Failed to place atoms for a particular section. Remove section and vector from list.
                self.remove_section(new_section,vct_temp,vector_list_finished)
                try_no=try_no+1
                if (try_no>4*len(self.vectors)):
                    #Unsucessful after several tries, restart whole DNA object
                    print 'Failed at {0:d} atoms'.format(len(self.atoms))
                    self.atoms=[]
                    self.vectors=[]
                    vector_list_finished=False
                    shifted=False
                    self.atom_no=0
                    self.extra_section_test()
                    self.first_section(other_atoms)
                    atom_start_iter.seek(0,True)
                else:
                    #Unsuccessful, try again with alternative vector (if available)
                    atom_start_iter.repeat()
            vector_list_finished=self.is_list_finished(self.section_length)     
            shifted=self.split_vector_check(vector_list_finished,shifted)
                           
    def add_section(self,new_section,vct_temp,vector_list_finished=False):
        if (vector_list_finished==False):
            self.vectors.append(vct_temp)
        else:
            self.vectors.remove(vct_temp)
        for new_atom in new_section:
            self.atoms.append(new_atom)
        self.atoms.sort()
        
    def assign_angles(self,atom_no,connect):
        for i in range(1,atom_no+1):
            self.angles.append([i,1,self.modulo1(i,atom_no),self.modulo1(i+1,atom_no),
                                self.modulo1(i+2,atom_no)])
        if(connect==2 or connect==True and type(connect)==bool):
            self.angles.append([atom_no+1,1,atom_no-1,atom_no,atom_no+1])
            self.angles.append([atom_no+2,1,atom_no,atom_no+1,atom_no+2])
        elif (connect==1):
            self.angles.append([atom_no+1,1,atom_no-1,atom_no,atom_no+1])
            
    def assign_bonds(self,atom_no,connect):
        """Set up bonds between atoms in the loop. This sets the bond at the base of the loop to be type 2, where the others are type 1."""
        for i in range(1,atom_no+1):
            self.bonds.append([i,1,self.modulo1(i,atom_no),self.modulo1(i+1,atom_no)])
        if(connect in [1,2,True]):
            self.bonds[-1][1]=2
            self.bonds.append([atom_no+1,1,atom_no,atom_no+1])
        if self.extra_bonds is not None:
            for bond in self.extra_bonds:
                self.bonds.append(bond)

    def is_list_finished(self,section_length):
        sections=self.completed_atom_no/(2*section_length)
        return len(self.atoms)/section_length>=sections
    
    def remove_section(self,remove_section,vct_temp,vector_list_finished=False):
        if (vector_list_finished==False):
            self.vectors.remove(vct_temp)
        else:
            self.vectors.append(vct_temp)
        for rem_atom in remove_section:
            self.atoms.remove(rem_atom)
        self.atoms.sort()
        
    def shift_vectors(self):
        vec_no=len(self.vectors)
        start_end_vector=vector.vector.multiply(self.loop_offset_vector,1.0/vec_no)
        for i in range(len(self.vectors)):
            self.vectors[i]=vector.vector.sub(self.vectors[i],start_end_vector)
    
    def split_vector_check(self,vector_list_finished,shifted):
        if(self.extra_section==True and vector_list_finished==True):
            #If we have a flag for an extra vector and we've created our loop of vectors, split one of the
            #vectors we have into two.
            vector_remove,vector_one,vector_two=self.split_vector()
            self.vectors.append(vector_one)
            self.vectors.append(vector_two)
            self.vectors.remove(vector_remove) 
            self.extra_section=False
        if(vector_list_finished==True and shifted==False):
            self.shift_vectors()
            shifted=True
        return shifted

    def updateposition(self,current_position,vct,vector_list_finished):
        if(vector_list_finished):
            return vector.vector.sub(current_position,vct)
        else:
            return vector.vector.add(current_position,vct)
    
    def vector_select(self,current_position,previous_position,vector_list_finished):
        if(vector_list_finished):
            return self.vectors[random.randrange(0,len(self.vectors))]
            
        else:
            return vector.vector.random_vector_selfavd(current_position,previous_position,self.atom_diameter)
