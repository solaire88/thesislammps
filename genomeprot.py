#! /usr/bin/env python
from __future__ import print_function
import imp
from matplotlib import pyplot
import numpy as np
import os,sys
import argparse
import utility

sys.path.append(os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/')

# For parallel runs make use of python MPI capabilities. While the non-LAMMPS parts of the script are not parallelised
# any LAMMPS command must be executed simultaneously by all processes.

try:
    from mpi4py import MPI

    # An object which communicates between processes (comm), the total number of process and this process id

    comm = MPI.COMM_WORLD
    nprocs = comm.Get_size()
    proc_id = comm.Get_rank()

except ImportError:
    print ('Script running on 1 processor as mpi4py module could not be found/loaded')

    # If we don't have any MPI stuff going on, set our process id to 0

    MPI = None
    comm = None
    nprocs = 1
    proc_id = 0


# Read in things from the command line

if proc_id == 0:
    parser = argparse.ArgumentParser('Run a LAMMPS simulation with DNA and multiple protein types')
else:
    parser = argparse.ArgumentParser('Run a LAMMPS simulation with DNA and multiple protein types', add_help=False)
    parser.add_argument('-h', nargs=0, action=utility.QuitArgparse)
    parser.add_argument('--help', nargs=0, action=utility.QuitArgparse)

parser.add_argument('-rg', dest='rg', action=utility.GraphAction, help='Display a graph showing Radius of Gyration')
parser.add_argument('-p', dest='p', action=utility.GraphAction,
                    help='Display a graph showing proportion of bound proteins')
parser.add_argument('-nc',dest='nc', action=utility.GraphAction,
                    help='Display a graph showing number of protein clusters')
parser.add_argument('-ncmin', dest='ncminsize', type=float, default=5, help='Sets a minimum size for a group of '
                                                                            'proteins to be considered a cluster')
parser.add_argument('-in', dest='input', type=str, help='The name of the input atom datafile',
                    default=os.path.dirname(os.path.realpath(__file__)) + '/Proteins/100proteins.initial')
parser.add_argument('-d', dest='dumpfile', nargs=2, type=str, help='Filename of dumpfile for simulation and'
                    'output frequency. Use: -d dumpfile.location 10000', default=None)
parser.add_argument('-show', dest='show_dumpfile', action='store_true', help='Show the dumpfile trajectory in vmd')
parser.add_argument('-e', dest='energies', action=utility.GraphAction, help='Display a graph of average '
                    'pair energies')
parser.add_argument('-s', dest='steps', nargs=2, type=int, help='Run for arg1 * arg2 timesteps. Default setting is 100 '
                                                                'steps of size 10000', default=[100, 10000])
parser.add_argument('-a', dest='angle_coeff', type=float, help='Set the coefficient for the cosine angle potential.'
                    'The persistence length is angle_coeff * sigma.', default=20.0)
parser.add_argument('-ainit', dest='angle_init', type=float, help='Set the initial value coefficient for the cosine angle potential.'
                    'This will be gradually reduced to the value for -a during pre-equilibration', default=20.0)
parser.add_argument('-lib', dest='lammps_library', type=str, help='Loads library named liblammps_{lib}.so',
                    default=None)
parser.add_argument('-interact', dest='interaction_energy', nargs = "+", type=float, help='Epsilon values for the '
                    'interaction between proteins and dna. Form: DNA Type, Prot Type, Energy, Cutoff')
parser.add_argument('-eq', dest='equil_time', type=int, help='Runs equlibration (no interaction between DNA and '
                    'proteins) for {equil_time} timesteps', default=1000000)
parser.add_argument('-eqsoft', dest='pre_equil_time', type=int, help='Runs a pre equilibration with soft atomic potential and harmonic bonds',default=0)
parser.add_argument('-prot_types', dest='prot_types', type=int, help='Number of different protein types')
args = parser.parse_args()
# Send our initial conditions to any other processes

if MPI is not None:
    args = comm.bcast(args, 0)

# Create a pointer to our LAMMPS instance
try:
    lammps = imp.load_source('lammps',os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/lammps.py')
    if proc_id == 0:
        print ('Loading Downloaded LAMMPS Version')

# Or use an existing one
except:
    import lammps
    if proc_id == 0:
        print ('Loading Pre-Existing LAMMPS Version')

if args.lammps_library is None and nprocs == 0:
    lmp = lammps.lammps()
elif args.lammps_library is None and nprocs != 0:
    lmp = lammps.lammps('g++_openmpi')
else:
    lmp = lammps.lammps(args.lammps_library)

# Create a random seed number for use in proteins.lmp, and save it as the LAMMPS variable ${seed}

if proc_id == 0:
    seed_no = np.random.randint(0, 10000000)
else:
    seed_no = None

if MPI is not None:
    seed_no = comm.bcast(seed_no, 0)

lmp.command('variable seed equal {0:d}'.format(seed_no))

# Create a LAMMPS variable pointing to our input data file

if not os.path.isfile(args.input) and proc_id == 0:
    raise IOError('Input file not found at {0:s}'.format(args.input))
lmp.command('variable inputfile string {0:s}'.format(args.input))

# Create a lammps variable with the coefficient for the angle potential

lmp.command('variable anglecoeff equal {0:f}'.format(args.angle_coeff))
lmp.command('variable eqsoft equal {}'.format(args.pre_equil_time))

# Check if we have the correct format for protein-DNA interactions

interactions = []

if len(args.interaction_energy)%4 == 0:
    for i in range (0,len(args.interaction_energy),4):
        interactions.append([args.interaction_energy[i+j] for j in range(4)])
else:
    print ('The format for interaction styles is: DNA Type, Protein Type, Interaction Energy, Interaction Cutoff')
    quit()

# Run a script with basic settings for lammps

lmp.file(os.path.dirname(os.path.realpath(__file__)) + '/Genome/genome.lmp')

# Reduce the angle coeff in steps until the target value is reached
if abs(args.angle_init - args.angle_coeff) > 0.0001:

    astep = (- args.angle_init + args.angle_coeff)/20.0

    for angle in np.arange(args.angle_init,args.angle_coeff,astep):
        lmp.command('variable anglecoeff equal {0:f}'.format(angle))
        lmp.command('run ${eqsoft}')

# Equiibrate the simulation for the specified time

lmp.command('run {0:d}'.format(args.equil_time))

# Write to a output dumpfile every n timesteps

if args.dumpfile is not None:
    lmp.command('dump 1 all custom {0:d} {1:s} id type mol x y z'.format(int(args.dumpfile[1]), args.dumpfile[0]))

lmp.command('reset_timestep 0')
for interaction in interactions:
    print ('pair_coeff {0:d} {1:d} {2:f} 1.0 {3:f}'.format(int(interaction[0]),int(interaction[1]),
                                                                float(interaction[2]),float(interaction[3])))
    lmp.command('pair_coeff {0:d} {1:d} {2:f} 1.0 {3:f}'.format(int(interaction[1]),int(interaction[0]),
                                                                float(interaction[2]),float(interaction[3])))


# Run for args.steps[0] * args.steps[1] steps

for step in range(args.steps[0]):

    lmp.command('run {0:d}'.format(args.steps[1]))

if args.show_dumpfile:
    utility.launch_vmd(args.dumpfile[0], os.path.dirname(os.path.realpath(__file__)) + '/Genome/vmd.genomeprot.in')
