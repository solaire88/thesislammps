from __future__ import print_function
import os
import sys
import subprocess
import imp
print ('Looking for LAMMPS python files...')

# Search for lammps in the systems python path

lammpsthesis_found=False
otherlammps_found=False
mpifound=False
mpipy_found=False

# Add this folders python lammps stuff if not already in path

if len(sys.argv)>1:
    mpifound=bool(sys.argv[1])

localpath = os.path.dirname(os.path.realpath(__file__))+'/lammps/python'
if localpath not in sys.path:
    sys.path.append(localpath)

try:
    print (os.path.dirname(os.path.realpath(sys.argv[0])) + '/lammps/python/lammps.py')
    lammps = imp.load_source('lammps', os.path.dirname(os.path.realpath(sys.argv[0])) + '/lammps/python/lammps.py')
    lammpsthesis_found = True
    print ('Downloaded LAMMPS found!')
except:
    print('Downloaded LAMMPS not found!')
try:
    import lammps
    otherlammps_found = True
    print ('Other version of lammps found!')
except:
    pass

print ('Looking for LAMMPS shared library files...')

# Search for files with liblammps and .so in name, then try and load them

# First look for the included LAMMPS distribution

lammpslib = {}
if lammpsthesis_found is True:
    rootdir = os.path.dirname(os.path.realpath(__file__))
    try:
        locallammpscontents = os.listdir(rootdir+'/lammps/src')
        for i in range(len(locallammpscontents)):
            if locallammpscontents[i][:9] == 'liblammps' and locallammpscontents[i][-3:] == '.so':
                if locallammpscontents[i][10:-3] == '':
                    pass
                else:
                    lammpslib[locallammpscontents[i][10:-3]] = False
    except:
        pass
        # Test if the libraries load properly

    for library in lammpslib.keys():
        output = ''
        try:
            #print(library,os.path.dirname(os.path.realpath(__file__)))
            proc = subprocess.Popen(['python','loadlammps.py','thesis',library],stdout=subprocess.PIPE)
            #proc = subprocess.call(['python','loadlammps.py','thesis',library])
            output = proc.stdout.read()
            #print(output)
            proc.terminate()
            if output[-29] == ')' and output[-27] == '1' and output[-25] == 'T':
                lammpslib[library] = True
            #if output[-25:-9] == 'Total wall time:':
                #lammpslib[library] = True
        except:
            proc.terminate()
            pass
        if len(output) == 0 or output =='':
                print ('\nSomething went wrong with library {}, error message printed above\n'.format(library))
            
# If there is another LAMMPS python file, look in other areas on the library path

otherlib = {}

if otherlammps_found is True:
    libpath = os.environ['LD_LIBRARY_PATH']

    libpath = libpath.split(':')[1:]

    libpath = list(set(libpath))


    # Remove the path we just checked, if it exists
    try:
        libpath.remove('{}'.format(os.path.dirname(os.path.realpath(__file__))+'/lammps/src/'))
    except:
        pass

    for path in libpath:
        pathcontents = os.listdir(path)
        for i in range(len(pathcontents)):
            if pathcontents[i][:9] == 'liblammps' and pathcontents[i][-3:] == '.so':
                if pathcontents[i][10:-3] == '':
                    pass
                else:
                    otherlib[pathcontents[i][10:-3]] = False
    for library in otherlib.keys():
        output = ''
        try:
            proc = subprocess.Popen(['python','loadlammps.py','other',library],stdout=subprocess.PIPE)
            output = proc.stdout.read()
            proc.terminate()
            if output[-29] == ')' and output[-27] == '1' and output[-25] == 'T':
                otherlib[library] = True
            #if out[-25:-9] == 'Total wall time:':
                #otherlib[library] = True
        except:
            proc.terminate()
        if len(output) == 0 or output =='':
                print ('\nSomething went wrong with library {}, error message printed above\n'.format(library))

# Check if mpi4py is installed and working
mpi_thesis_dict = {lib:False for lib in lammpslib.keys()}
mpi_other_dict = {lib:False for lib in otherlib.keys()}
print(lammpslib.keys(),otherlib.keys())
if mpifound is True:
    print ('Looking for mpi4py...')

    try:
        from mpi4py import MPI
        mpipy_found = True
        comm = MPI.COMM_WORLD
        nprocs = comm.Get_size()
        proc_id = comm.Get_rank()
    except ImportError:
        'Could not find mpi4py'

    if mpipy_found is True:
        if lammpsthesis_found is True:
            for localkey in lammpslib.keys():
                if 'serial' in localkey:
                    mpi_thesis_dict[localkey] = 'N/A'
                    continue
                call_str = ['mpirun', '-n', '2', 'python', 'mpitest.py']
                call_str.append('-lammpsthesis')
                call_str.append(localkey)
                mpi_thesis_dict[localkey] = False
                output = None
                try:
                    proc = subprocess.Popen(call_str, stdout=subprocess.PIPE)
                    output = proc.stdout.read()
                    proc.terminate()
                    if len(output) == 48 and output[-29] == ')' and output[-27] == '1' and output[-25] == 'T':
                        mpi_thesis_dict[localkey] = True
                except:
                    proc.terminate()
                #print (localkey,output,len(output))
                if output is None:
                    print('\nSomething went wrong with library {}, error message printed above\n'.format(localkey))
                elif len(output) == 0:
                    print('\nSomething went wrong with library {}, error message printed above\n'.format(localkey))

        if otherlammps_found is True:
            for localkey in otherlib.keys():
                if 'serial' in localkey:
                    mpi_other_dict[localkey] = 'N/A'
                    continue
                call_str = ['mpirun', '-n', '2', 'python', 'mpitest.py']
                call_str.append('-lammps')
                call_str.append(localkey)
                mpi_other_dict[localkey] = False
                output = None
                try:
                    proc = subprocess.Popen(call_str, stdout=subprocess.PIPE)
                    output = proc.stdout.read()
                    proc.terminate()
                    if len(output) == 48 and output[-29] == ')' and output[-27] == '1'and output[-25] == 'T':
                        mpi_other_dict[localkey] = True
                except:
                    proc.terminate()
                #print (localkey,output,len(output))
                if output is None:
                    print('\nSomething went wrong with library {}, error message printed above\n'.format(localkey))
                elif len(output) == 0:
                    print('\nSomething went wrong with library {}, error message printed above\n'.format(localkey))
else:
    for key in mpi_thesis_dict.keys():
        mpi_thesis_dict[key] = 'N/A'
    for key in mpi_other_dict.keys():
        mpi_other_dict[key] = 'N/A'

matplotlib_found = False

try:
    from matplotlib import pyplot
    matplotlib_found = True
except:
    pass

try:
    # Make a nicer looking results window, and test PyQt a bit)
    from PyQt4 import QtGui
    use_pyqt = True
except:
    use_pyqt = False

if use_pyqt is True:
    app = QtGui.QApplication([''])
    main = QtGui.QMainWindow()
    main.setWindowTitle('Qt GUI Test')
    main.setGeometry(300,300,600,500)
    window  = QtGui.QPlainTextEdit()
    main.setCentralWidget(window)
    QtGui.QToolTip.setFont(QtGui.QFont('SansSerif', 10))
    #window.setFont(QtGui.QFont('SansSerif', 10))
    #window.setGeometry(300,300,600,500)
    window.setDocumentTitle('Qt GUI Test')
    if lammpsthesis_found and otherlammps_found:
        window.appendPlainText('Found the lammps version included with download as well as another lammps install. '
               'Simulation scripts will default to using the downloaded version.\n')
    elif lammpsthesis_found or otherlammps_found:
        window.appendPlainText('Found a version of lammps\n')

    if mpipy_found:
        window.appendPlainText('Found a version of mpi4py\n')
    else:
        window.appendPlainText('mpi4py not found or not working\n')
    if matplotlib_found:
        window.appendPlainText('Found a version of matplotlib\n')
    else:
        window.appendPlainText('matplotlib not found or not working\n')
    window.appendPlainText('---From included LAMMPS version---\n')
    window.appendPlainText('Library Name:\t\tSerial runs OK\tMPI runs OK\n')
    for key in lammpslib.keys():
        tabno =  3- (len(key)-1)/10
        tabstr = '\t'*tabno
        # print (tabno,len(key))
        window.appendPlainText('{}{}{}\t\t{}\n'.format(key,tabstr,lammpslib[key],mpi_thesis_dict[key]))
    if lammpslib =={}:
        window.appendPlainText('No libraries found!\n')
    window.appendPlainText('---From other LAMMPS version---\n')
    window.appendPlainText('Library Name:\t\tSerial runs OK\tMPI runs OK\n')
    for key in otherlib.keys():
        tabno = 3 - (len(key)-1)/10
        tabstr = '\t'*tabno
        window.appendPlainText('{}{}{}\t\t{}\n'.format(key,tabstr,otherlib[key],mpi_other_dict[key]))
    if otherlib =={}:
        window.appendPlainText('No libraries found!\n')
    values = mpi_thesis_dict.values()
    values.extend(mpi_other_dict.values())
    if set(values) == {False}:
        window.appendPlainText('\nAll MPI tests failed, MPI libraries may not be loaded/installed\n')
    window.setReadOnly(True)
    main.show()
    sys.exit(app.exec_())
else:
    print ('\n\n\n\n\n\n\n\n\n\n\n\n')
    if lammpsthesis_found and otherlammps_found:
        print ('Found the lammps version included with download as well as another lammps install. '
               'Simulation scripts will default to using the downloaded version.\n')
    elif lammpsthesis_found or otherlammps_found:
        print ('Found a version of lammps\n')

    if mpipy_found:
        print ('Found a version of mpi4py\n')
    else:
        print ('mpi4py not found or not working\n' )
    if matplotlib_found:
        print ('Found a version of matplotlib\n')
    else:
        print ('matplotlib not found or not working\n' )

    print ('---From included LAMMPS version---\n')
    print ('Library Name:\t\t\t\tSerial runs OK\tMPI runs OK\n')
    for key in lammpslib.keys():
        tabno = 5 - len(key)/8
        tabstr = '\t'*tabno
        print ('{}{}{}\t\t{}\n'.format(key,tabstr,lammpslib[key],mpi_thesis_dict[key]))
    if lammpslib =={}:
        print('No libraries found!\n')
    print ('---From other LAMMPS version---\n')
    print ('Library Name:\t\t\t\tSerial runs OK\tMPI runs OK\n')
    for key in otherlib.keys():
        tabno = 5 - len(key)/8
        tabstr = '\t'*tabno
        print ('{}{}{}\t\t{}\n'.format(key,tabstr,otherlib[key],mpi_other_dict[key]))
    if otherlib =={}:
        print('No libraries found!\n')
    values = mpi_thesis_dict.values()
    values.extend(mpi_other_dict.values())
    if set(values) == {False}:
        print ('\nAll MPI tests failed, MPI libraries may not be loaded/installed\n')
