import sys
import subprocess
import itertools
import os


class statedata_to_input_genomics(object):
    """Make an input file from hmm track and/or gc data and/or ctcf site data"""

    def __init__(self, inputfile, outputfile_beads, chromosome_name, htr_ids = ['13_Heterochrom/lo'], bead_info=['start', 'end', 3000, 90],
                 state_col_info=[0, 1, 2, 3], match_ids=[[1, 4, 5], [9, 10]],
                 match_names=['str', 'weak'], override=['weak', 'str'], gcfile=None, no_data_val=0,
                 htr_calc_info=None ):

        # A list of states from a hmm track input file
        self.statedata = []

        # A dict mapping hmm track numbers to their names
        self.statenames = dict(zip(match_names, match_ids))
        self.stateindex = dict(zip(match_names, range(len(match_names))))
        if self.depth(htr_ids) == False:
            self.htr_ids = [htr_ids]
        else:
            self.htr_ids = htr_ids

        self.chromosome_name = chromosome_name

        # If the file has been modified and columns are different to normal, set them here.
        self.chr_no_col = state_col_info[0]
        self.start_bp_col = state_col_info[1]
        self.end_bp_col = state_col_info[2]
        self.chr_state_col = state_col_info[3]

        # Give the variables in bead_info more descriptive names
        self.start_bp = bead_info[0]
        self.end_bp = bead_info[1]
        self.bin_size = bead_info[2]
        self.bin_thr = bead_info[3]
        self.htr_list = []

        # Write a file with info about the input conditions
        logfile = open('logfile.txt', 'w')

        chr_in = open(inputfile, 'r')

        # Use shell commands head or tail to read the start and end bp value, if not already specified
        if self.start_bp == 'start':
            for line in chr_in:
                tokens = line.split()
                if tokens[self.chr_no_col] == chromosome_name:
                    self.start_bp = int(tokens[self.start_bp_col])
                    break
        chr_in.seek(0,0)
        if self.end_bp == 'end':
            for line in chr_in:
                tokens = line.split()
                if tokens[self.chr_no_col] == chromosome_name:
                    self.end_bp = int(tokens[self.end_bp_col])
        logfile.write('Start Bp: ' + str(self.start_bp) + ' End Bp: ' + str(self.end_bp) + '\n')
        chr_in.seek(0, 0)
        # Find the number of bins in the input file
        self.bin_no = (self.end_bp - self.start_bp) / self.bin_size
        print self.start_bp,self.end_bp,chr_in

        for line in chr_in:
            tokens = line.split()
            # Ignore commented out lines
            if '#' in tokens[0]:
                pass
            # Read in data within specified range
            elif int(tokens[self.start_bp_col]) < self.end_bp and int(tokens[self.end_bp_col]) > self.start_bp and tokens[self.chr_no_col] == chromosome_name:
                self.statedata.append([int(tokens[self.start_bp_col]), int(tokens[self.end_bp_col]),
                                       str(tokens[self.chr_state_col])])
                #print self.statedata
        chr_in.close()
        # Variable for binned state data
        self.binned_statedata = [[] for i in range(self.bin_no)]
        self.state_bin_data(match_ids)

        htr_prop_from_state = self.htr_perc_from_state(inputfile, htr_calc_info)
        logfile.write('Heterochromatin proportion: ' + str(htr_prop_from_state) + ' Heterochromatin Beads: ' +
                      str(int(htr_prop_from_state * self.bin_no)) + '/' + str(self.bin_no) + '\n')

        print htr_prop_from_state
        # If you submit a gc file heterochromatin beads are assigned based on this
        if gcfile is not None:
            gcdata = []
            # If there is no gc data for sections of the genome (usually the centromere), set the gc-percentage to
            # no_data_val for those regions. Setting no_data_val = 0 will make those beads heterchromatic (low gc)
            for line in open(gcfile, 'r'):
                if '#' in line[0]:
                    pass
                else:
                    [bp, perc] = line.split()
                    if self.start_bp < int(bp) < self.end_bp:
                        gcdata.append([int(bp), float(perc)])
                    # Stop reading after end_bp
                    elif int(bp) > self.end_bp:
                        break

            # self.binned_gcdata = [start_bp,percentage of gc reads,flag if no data was available]
            self.binned_gcdata = [[0, 0, 0] for i in range(self.bin_no + 1)]
            self.gc_bin_data(gcdata, no_data_val)
            threshold = self.match_threshold(htr_prop_from_state)
            print threshold
            print self.binned_gcdata[-5:]

        logfile.write('{State Descriptions: Ids}\n' + str(self.statenames) + '\n')


        # self.write_globals(outputfile_beads, prot_info)
        dna_types, features, count_dict = self.write_bead_colour_file(outputfile_beads, override)
        self.dna_types = dna_types
        self.features = features
        self.count_dict = count_dict

        # Remove the 'overridden' features from the dict
        feature_reduce  = dict(features)

        override_index = []

        for pair in override:
            override_index.append([self.stateindex[pair[0]],self.stateindex[pair[1]]])

        keys = feature_reduce.keys()

        for key in keys:
            val = feature_reduce[key]
            for pair in override_index:
                if pair[0] in key and pair[1] in key:
                    feature_reduce.pop(key)
                    key_list = list(key)
                    key_list.remove(pair[0])
                    feature_reduce[tuple(key_list)] = val

        self.feature_reduce = feature_reduce

        print 'Types,Features,Count Dict.\n'
        print dna_types
        print features
        print '\n'
        print feature_reduce
        print count_dict
        print 'Statedata\n'
        print self.statedata[-5:]
        print 'Binned Statedata\n'
        print self.binned_statedata[-20:]
        # self.write_section(self.bin_no)
        # self.write_boundaries()

        logfile.write('There are ' + str(dna_types) + ' dna bead types\n')
        # print self.statenames,self.stateindex,features
        self.stateindex_rev = dict()
        for k in self.stateindex:
            self.stateindex_rev[self.stateindex[k]] = k
        stringlist = []
        for k in features:
            typestring = ''
            count = int(count_dict[k])
            for type in k:
                typestring += self.stateindex_rev[type] + ' '
            datastring = 'Bead type ' + str(features[k]) + ' is ' + typestring + ' (' + str(count) + ')'
            stringlist.append([features[k], datastring])
        stringlist.sort()
        for id, string in stringlist:
            logfile.write(string + '\n')


    def depth(self, list_input):
        if list_input == []:
            return 0
        function = lambda L: isinstance(L, list) and max(map(self.depth, L)) + 1
        return (function(list_input))

    def gc_bin_data(self, data, no_data_val):
        """The gc file format gives gc reads in (usually) 5 bp blocks"""
        for block_start, perc in data:
            bin_no = (block_start - self.start_bp) / self.bin_size
            self.binned_gcdata[bin_no][0] += perc
            self.binned_gcdata[bin_no][1] += 1
        for i in range(len(self.binned_gcdata)):
            if self.binned_gcdata[i][1] == 0:
                self.binned_gcdata[i][0] = no_data_val
                self.binned_gcdata[i][2] = 1
            else:
                self.binned_gcdata[i][0] = float(self.binned_gcdata[i][0]) / self.binned_gcdata[i][1]
        self.binned_gcdata = [[i, a[0], a[2]] for i, a in enumerate(self.binned_gcdata, 1)]

    def htr_perc_from_state(self, inputfile, htr_calc_info):
        """Find the percentage of heterochromatin from the hmm input file"""
        start_bp = self.start_bp
        end_bp = self.end_bp
        if htr_calc_info is not None:
            start_bp = htr_calc_info[0]
            end_bp = htr_calc_info[1]
        total_htr_bp = 0
        for line in open(inputfile, 'r'):
            tokens = line.split()
            # Ignore commented lines
            if '#' in tokens[0]:
                pass
            elif self.chromosome_name != tokens[self.chr_no_col]:
                pass
            elif (int(tokens[self.chr_state_col].split('_')[0]) in self.htr_ids and \
                            int(tokens[self.start_bp_col]) < end_bp and \
                            int(tokens[self.end_bp_col]) > start_bp) or (tokens[self.chr_state_col] in self.htr_ids and \
                            int(tokens[self.start_bp_col]) < end_bp and \
                            int(tokens[self.end_bp_col]) > start_bp):
                if int(tokens[self.end_bp_col]) > end_bp:
                    total_htr_bp += (self.end_bp - int(tokens[self.start_bp_col]))
                elif (int(tokens[self.start_bp_col]) < start_bp):
                    total_htr_bp += (int(tokens[self.end_bp_col]) - start_bp)
                else:
                    total_htr_bp += (int(tokens[self.end_bp_col]) - int(tokens[self.start_bp_col]))
                print total_htr_bp,start_bp,end_bp
        htr_prop = float(total_htr_bp) / float(end_bp - start_bp)
        return htr_prop

    def match_threshold(self, htr_perc_from_state):
        """Find gc threshold level where we have a number of heterochromatin
        beads equal to the bead no from state data only.
        Arguments:
        htr_perc_from_state -- The heterochromatin percentage calaculated from hmm state data.
        """
        curr_thr = 50.0
        thr = 50.0
        for i in range(15):
            print curr_thr
            htr_list = []
            # print thr,curr_thr
            for bin, perc, nodata_flag in self.binned_gcdata:
                if perc < curr_thr and nodata_flag == 0:
                    htr_list.append([bin, 1, 1])
                elif perc < curr_thr and nodata_flag == 1:
                    htr_list.append([bin, 1, 0])
                else:
                    htr_list.append([bin, 0, 1])
            # Calculate the percentage of heterochromatin beads for this threshold
            htr_list_finalcalc = [htr * nodata_flag for bin, htr, nodata_flag in htr_list]
            htr_list_prop = float(sum(htr_list_finalcalc)) / len(htr_list_finalcalc)
            print htr_list_prop,htr_perc_from_state,htr_list_prop > htr_perc_from_state
            # print htr_list_prop,htr_perc_from_state
            if (htr_list_prop > htr_perc_from_state):
                curr_thr = curr_thr - 25.0 / (2.0 ** i)

            else:
                curr_thr = curr_thr + 25.0 / (2.0 ** i)
        # print str(curr_thr)+'thr'
        self.htr_list = htr_list
        # Delete nodata_flag
        for value1, value2 in zip(self.binned_gcdata, htr_list):
            value1.pop(2)
            value2.pop(2)


        self.stateindex['htr'] = len(self.stateindex)
        for htr_value, data_value in zip(htr_list, self.binned_statedata):
            if htr_value[1] == 1:
                data_value[1].append(self.stateindex['htr'])
                data_value[1].sort()


    def state_bin_data(self, match_ids):
        """Map the information in the state data file to the beads in the simulation"""

        # Keep track of which identifiers go together. i.e [[1,2],3] -> {1:0,2:0,3:1}
        group_dict = dict()
        print match_ids
        for group_no, group_items in enumerate(match_ids):
            print group_no, group_items
            for item in group_items:
                group_dict[item] = group_no
        print 'Group Dict\n'
        print group_dict

        # Remove the groups in our match list, so we can iterate over it. i.e. [[1,2],3,[6,7]] -> [1,2,3,6,7]
        match_copy = match_ids[:]
        match_ids = []

        for x in match_copy:
            match_ids.extend(x)

        for sectionstart_bp, sectionend_bp, chrtype in self.statedata:
            if chrtype in match_ids or chrtype.split('_')[0] in match_ids:
                to_next_bin = self.bin_size - ((sectionstart_bp - self.start_bp) % self.bin_size)
                if (to_next_bin > (sectionend_bp - sectionstart_bp)):
                    bin_length = 1
                else:
                    bin_length = (sectionend_bp - sectionstart_bp - to_next_bin) / self.bin_size + 2
                start_bin = int((sectionstart_bp - self.start_bp) / self.bin_size)

                # Proportion of each section which lies in the range of a bin [amount in bp,bin id]
                prop_in_bin = []
                for j in range(bin_length):
                    # This is either from the start of the section to the start of the next bin, from the start of
                    # the bin to the end of the section, or just the entire bin
                    prop_in_bin.append([min(self.start_bp + (start_bin + 1 + j) * self.bin_size - sectionstart_bp,
                                            sectionend_bp - (start_bin + j) * self.bin_size - self.start_bp,
                                            self.bin_size),
                                        start_bin + j])

                for bp, bin_id in prop_in_bin:
                    # Sometimes the final section will extend beyond the end_bp value you set for the simulation. This
                    # prevents any attempts to write to those (non-existant!) bins
                    if bin_id > self.bin_no - 1:
                        break
                    # If enough of the section overlaps a bin, label it.
                    if (bp > self.bin_thr):
                        try:
                            self.binned_statedata[bin_id].append(group_dict[chrtype])
                        except:
                            self.binned_statedata[bin_id].append(group_dict[chrtype.split('_')[0]])
        # Label the binned state data from 1 -> no. beads rather than from 0 -> no. beads - 1 and remove duplicate
        # chrtypes.
        for i in range(len(self.binned_statedata)):
            self.binned_statedata[i] = [i + 1, list(set(self.binned_statedata[i]))]

    def write_bead_colour_file(self, filename, override):
        with open(filename, 'w') as w:
            features = {(): 1}
            count = 1
            for i in range(1, len(self.stateindex) + 1):
                for j in itertools.combinations([l for l in range(len(self.stateindex))], i):
                    count += 1
                    features[j] = count
            # print features, override, self.stateindex[override[0]], self.stateindex[override[1]]
            # Convert to nested list if there is only one type to override
            if self.depth(override) == 1:
                override = [override]

            """for orig, replace in override:
                print features
                for key in features:
                    if self.stateindex[orig] in key and self.stateindex[replace] in key:
                        keylist = list(key)
                        keylist.remove(self.stateindex[orig])
                        new_key = tuple(keylist)
                        features[key] = features[new_key]
            """

            equiv_dict = {key:list(key) for key in features}
            for key in equiv_dict:
                for orig, replace in override:
                    if self.stateindex[orig] in equiv_dict[key] and self.stateindex[replace] in equiv_dict[key]:
                        print self.stateindex[orig],equiv_dict[key]
                        equiv_dict[key].remove(self.stateindex[orig])

            for key in features:
                print equiv_dict[key],features[tuple(equiv_dict[key])]
                features[key]=features[tuple(equiv_dict[key])]
            # print features, override, self.stateindex[override[0][0]],self. stateindex[override[0][1]]
            # Delete unused feature combinations
            count_dict = dict()
            for k in features:
                count_dict[k] = 0
            for atom_data in self.binned_statedata:
                count_dict[tuple(atom_data[1])] += 1
            for k in count_dict:
                if (count_dict[k] == 0):
                    features.pop(k)

            # Since the combinations may not have consecutive atom numbers,rewrite them so they do
            valuelist = []
            for key in features:
                valuelist.append(features[key])
            unique_values = list(set(valuelist))
            for i in range(len(unique_values)):
                unique_values[i] = [unique_values[i], i + 1]
            for key in features:
                value = features[key]
                for pair in unique_values:
                    if value == pair[0]:
                        features[key] = pair[1]

            for atom_data in self.binned_statedata:
                if atom_data[1] is []:
                    w.write(str(atom_data[0]) + ' 1\n')
                else:
                    w.write(str(atom_data[0]) + ' ' + str(features[tuple(atom_data[1])]) + '\n')
        return max(zip(*unique_values)[1]), features, count_dict



