#! /usr/bin/env python
from __future__ import print_function
import imp
from matplotlib import pyplot
import numpy as np
import os,sys
import argparse
import utility

sys.path.append(os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/')

# For parallel runs make use of python MPI capabilities. While the non-LAMMPS parts of the script are not parallelised
# any LAMMPS command must be executed simultaneously by all processes.

try:
    from mpi4py import MPI

    # An object which communicates between processes (comm), the total number of process and this process id

    comm = MPI.COMM_WORLD
    nprocs = comm.Get_size()
    proc_id = comm.Get_rank()

except ImportError:
    print ('Script running on 1 processor as mpi4py module could not be found/loaded')

    # If we don't have any MPI stuff going on, set our process id to 0

    MPI = None
    comm = None
    nprocs = 1
    proc_id = 0


# Read in things from the command line

if proc_id == 0:
    parser = argparse.ArgumentParser('Run a LAMMPS simulation with DNA and proteins')
else:
    parser = argparse.ArgumentParser('Run a LAMMPS simulation with DNA and proteins', add_help=False)
    parser.add_argument('-h', nargs=0, action=utility.QuitArgparse)
    parser.add_argument('--help', nargs=0, action=utility.QuitArgparse)

parser.add_argument('-rg', dest='rg', action=utility.GraphAction, help='Display a graph showing Radius of Gyration')
parser.add_argument('-p', dest='p', action=utility.GraphAction,
                    help='Display a graph showing proportion of bound proteins')
parser.add_argument('-nc',dest='nc', action=utility.GraphAction,
                    help='Display a graph showing number of protein clusters')
parser.add_argument('-ncmin', dest='ncminsize', type=float, default=5, help='Sets a minimum size for a group of '
                                                                            'proteins to be considered a cluster')
parser.add_argument('-in', dest='input', type=str, help='The name of the input atom datafile',
                    default=os.path.dirname(os.path.realpath(__file__)) + '/Proteins/100proteins.initial')
parser.add_argument('-d', dest='dumpfile', nargs=2, type=str, help='Filename of dumpfile for simulation and'
                    'output frequency. Use: -d dumpfile.location 10000', default=None)
parser.add_argument('-show', dest='show_dumpfile', action='store_true', help='Show the dumpfile trajectory in vmd')
parser.add_argument('-e', dest='energies', action=utility.GraphAction, help='Display a graph of average '
                    'pair energies')
parser.add_argument('-s', dest='steps', nargs=2, type=int, help='Run for arg1 * arg2 timesteps. Default setting is 100 '
                                                                'steps of size 10000', default=[100, 10000])
parser.add_argument('-a', dest='angle_coeff', type=float, help='Set the coefficient for the cosine angle potential.'
                    'The persistence length is angle_coeff * sigma.', default=20.0)
parser.add_argument('-ainit', dest='angle_init', type=float, help='Set the initial value coefficient for the cosine angle potential.'
                    'This will be gradually reduced to the value for -a during pre-equilibration', default=20.0)
parser.add_argument('-lib', dest='lammps_library', type=str, help='Loads library named liblammps_{lib}.so',
                    default=None)
parser.add_argument('-intenergy', dest='interaction_energy', type=float, default=1.0, help='Epsilon value for the '
                    'interaction between proteins and dna')
parser.add_argument('-intcutoff', dest='interaction_cutoff', type=float, default=4.0, help='Distance cutoff value for '
                    'the interaction between proteins and dna')
parser.add_argument('-eq', dest='equil_time', type=int, help='Runs equlibration (no interaction between DNA and '
                    'proteins) for {equil_time} timesteps', default=1000000)
parser.add_argument('-eqsoft', dest='pre_equil_time', type=int, help='Runs a pre equilibration with soft atomic potential and harmonic bonds',default=0)
args = parser.parse_args()
# Send our initial conditions to any other processes

if MPI is not None:
    args = comm.bcast(args, 0)

# Create a pointer to our LAMMPS instance
try:
    lammps = imp.load_source('lammps',os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/lammps.py')
    if proc_id == 0:
        print ('Loading Downloaded LAMMPS Version')
# Or use an existing one
except:
    import lammps
    if proc_id == 0:
        print ('Loading Pre-Existing LAMMPS Version')

if args.lammps_library is None and nprocs == 0:
    lmp = lammps.lammps()
elif args.lammps_library is None and nprocs != 0:
    lmp = lammps.lammps('g++_openmpi')
else:
    lmp = lammps.lammps(args.lammps_library)

# Create a random seed number for use in proteins.lmp, and save it as the LAMMPS variable ${seed}

if proc_id == 0:
    seed_no = np.random.randint(0, 10000000)
else:
    seed_no = None

if MPI is not None:
    seed_no = comm.bcast(seed_no, 0)

lmp.command('variable seed equal {0:d}'.format(seed_no))

# Create a LAMMPS variable pointing to our input data file

if not os.path.isfile(args.input) and proc_id == 0:
    raise IOError('Input file not found at {0:s}'.format(args.input))
lmp.command('variable inputfile string {0:s}'.format(args.input))

# Create a lammps variable with the coefficient for the angle potential

lmp.command('variable anglecoeff equal {0:f}'.format(args.angle_coeff))
lmp.command('variable eqsoft equal {}'.format(args.pre_equil_time))

# Run a script with basic settings for lammps

lmp.file(os.path.dirname(os.path.realpath(__file__)) + '/Proteins/proteins.lmp')

astep = (- args.angle_init + args.angle_coeff)/20.0

if abs(args.angle_init - args.angle_coeff) > 0.0001:

    for angle in np.arange(args.angle_init,args.angle_coeff,astep):
        lmp.command('variable anglecoeff equal {0:f}'.format(angle))
        lmp.command('run ${eqsoft}')

# Set up computes if required

if args.rg is not None:
    lmp.command('compute rg DNA gyration')

if args.energies is not None:
    lmp.command('compute pair all pair lj/cut')

# Equiibrate the simulation for the specified time

lmp.command('run {0:d}'.format(args.equil_time))

# Write to a output dumpfile every n timesteps

if args.dumpfile is not None:
    lmp.command('dump 1 all custom {0:d} {1:s} id type mol x y z'.format(int(args.dumpfile[1]), args.dumpfile[0]))

# Create lists for data storage

cluster_size = []
bound_proteins = []
r_g = []
energy_data = []

nprot = len(utility.get_atom_data(lmp,match_types=2).keys())

def find_clusters(lmp_inst, protein_types=2, cluster_cut=args.interaction_cutoff, min_size=1):
    """Find protein clusters in the simulation. protein_type is the atom type for proteins in the simulation.
    cluster_cut is the maximum separation for 2 proteins to be considered in the same cluster. min_size is
    the minimum proportion of total proteins required before counting a group of proteins as a cluster. This
    can be as a proportion of the total (if min_size < 1.0) or as a minimum number of proteins (if min_size >=1.0) """
    proteins_dict = utility.get_atom_data(lmp_inst, match_types=protein_types)
    boundaries = [lmp_inst.extract_global('boxxlo', 1), lmp_inst.extract_global('boxxhi', 1),
                  lmp_inst.extract_global('boxylo', 1), lmp_inst.extract_global('boxyhi', 1),
                  lmp_inst.extract_global('boxzlo', 1), lmp_inst.extract_global('boxzhi', 1)]
    protein_keys = sorted(proteins_dict.keys())
    protein_no = len(proteins_dict)

    cluster_no = 1
    cluster_map = {}

    def grow_cluster(prot_id, cluster_id):
        """From the current protein, search for neighbours and remove the current protein from future searches"""

        # Create a new cluster if we were not a neighbour of an existing cluster
        if cluster_id not in cluster_map.keys():
            cluster_map[cluster_id] = [prot_id]

        # Take this protein out of list of potential neighbours. Otherwise, the code would jump back and forth between
        # two neighbouring proteins.

        protein_keys.remove(prot_id)
        rmv = [prot_id]

        # An iterable for the other protein ids which can be 'rewound'. Since this is not a generator it might have
        # high memory usage for (very) large, highly connected protein networks

        prot_iter = utility.RewindableIterator(protein_keys)
        for keyA in prot_iter:
            if utility.dist(proteins_dict[prot_id], proteins_dict[keyA], boundaries) < cluster_cut:
                cluster_map[cluster_id].append(keyA)
                curr_key = keyA
                rmv_part = grow_cluster(keyA, cluster_id)
                rmv.extend(rmv_part)

                # If proteins are removed from before an existing proteins place in the list take this into account
                # i.e if index = 2 [a,b,(c),d] -> [a,(c),d] index should = 1
                rewind_by = 1
                for protein in rmv:
                    if curr_key > protein:
                        rewind_by -= 1
                prot_iter.seek(rewind_by)

        return rmv

    # Keep looking for clusters until all proteins are assigned to one (even if the 'cluster' ends up being just the
    # protein itself

    while len(protein_keys) != 0:
        grow_cluster(protein_keys[0], cluster_no)
        cluster_no += 1

    # Delete from results cluster which don't meet a predefined condition

    for cluster in cluster_map.keys():
        if min_size >= 1.0:
            if len(cluster_map[cluster]) < min_size:
                cluster_map.pop(cluster)
        if min_size < 1.0:
            prop = float(len(cluster_map[cluster]))/float(protein_no)
            if prop < min_size:
                cluster_map.pop(cluster)
    return cluster_map

# Get the first cluster map, set the proteins to interact with DNA and reset our timestep count to 0
if args.nc.calculate is True or args.p.calculate is True:
    cluster_map_single = find_clusters(lmp, cluster_cut=args.interaction_cutoff, min_size=2)
    cluster_size.append(cluster_map_single)

if args.rg.calculate is True:
    rg_squared = lmp.extract_compute('rg', 0, 0)
    r_g.append(rg_squared ** 0.5)

if args.energies.calculate is True:
    e_pair_total = lmp.extract_compute('pair', 0, 0)
    energy_data.append(e_pair_total / nprot)


lmp.command('reset_timestep 0')
lmp.command('pair_coeff 1 2 {0:f} 1.0 {1:f}'.format(args.interaction_energy, args.interaction_cutoff))


# Run for args.steps[0] * args.steps[1] steps

for step in range(args.steps[0]):

    lmp.command('run {0:d}'.format(args.steps[1]))
    if args.nc.calculate is True or args.p.calculate is True:
        cluster_map_single = find_clusters(lmp, cluster_cut=args.interaction_cutoff, min_size=args.ncminsize)
        cluster_size.append(cluster_map_single)

    if args.rg.calculate is True:
        rg_squared = lmp.extract_compute('rg', 0, 0)
        r_g.append(rg_squared ** 0.5)

    if args.energies.calculate is True:
        e_pair = lmp.extract_compute('pair', 0, 0)
        energy_data.append(e_pair / nprot)

# Plot graphs on root process

proteins_info = utility.get_atom_data(lmp, match_types=[2])
nproteins = len(proteins_info)

if proc_id == 0:

    # Graph 1


    if args.nc.calculate is True or args.rg.calculate is True:
        fig1 = pyplot.figure(1)
        ax1 = pyplot.axes()
        ax1.set_xlabel('Timesteps')
        ax1.set_ylabel('Number of Clusters', color='b')

        ax2 = pyplot.axes()
        ax2.set_xlabel('Timesteps')

        if args.nc .calculate is True:
            ax1.plot([i*args.steps[1] for i in range(args.steps[0] + 1)], map(lambda cmap: len(cmap), cluster_size), 'b-',
                 label='Number of Clusters')
            fig1.add_axes(ax1)
            fig1.tight_layout()

    if args.rg.calculate is True and args.nc.calculate is True:
        ax2 = ax1.twinx()

    if args.rg.calculate is True:
        ax2.set_ylabel('Radius of Gyration', color='r')
        ax2.plot([i*args.steps[1] for i in range(args.steps[0] + 1)], r_g, 'r-', label='Radius of Gyration')
        fig1.add_axes(ax2)
        fig1.tight_layout()

    if args.rg.save is True:
        fig1.savefig(args.rg.saveAt)

    if args.nc.save is True:
        fig1.savefig(args.nc.saveAt)

    # Graph 2

    if args.nc.calculate is True or args.energies.calculate is True:
        fig2 = pyplot.figure(2)

    if args.nc.calculate is True:

        ax3 = pyplot.axes()
        ax3.set_xlabel('Timesteps')
        ax3.set_ylabel('Number of Clusters', color='b')
        ax3.plot([i * args.steps[1] for i in range(args.steps[0] + 1)], map(lambda cmap: len(cmap), cluster_size), 'b-',
                 label='Number of Clusters')
        fig2.add_axes(ax3)

    if args.energies.calculate is True:
        ax4 = pyplot.axes()
        ax4.set_xlabel('Timesteps')

    if args.nc.calculate is True and args.energies.calculate is True:
        ax4 = ax3.twinx()

    if args.energies.calculate is True:
        ax4.set_ylabel('Average Pair Energy', color='r')
        ax4.plot([i * args.steps[1] for i in range(args.steps[0] + 1)], energy_data, 'r-', label='Avg. Pair Energy')
        fig2.add_axes(ax4)
        fig2.tight_layout()

    if args.energies.save is True:
        fig2.savefig(args.energies.saveAt)

    # Graph 3
    if args.nc.calculate is True or args.p.calculate is True:
        fig3 = pyplot.figure(3)

    if args.nc.calculate is True:
        ax5 = pyplot.axes()
        ax5.set_xlabel('Timesteps')
        ax5.set_ylabel('Number of Clusters', color='b')
        ax5.plot([i * args.steps[1] for i in range(args.steps[0] + 1)], map(lambda cmap: len(cmap), cluster_size), 'b-',
                 label='Number of Clusters')
        fig3.add_axes(ax5)

    if args.p.calculate is True:
        ax6 = pyplot.axes()
        ax6.set_xlabel('Timesteps')

    if args.p.calculate is True and args.nc.calculate is True:
        ax6 = ax5.twinx()

    if args.p.calculate is True:
        ax6.set_ylabel('Proportion of Proteins in Cluster', color='r')
        print([i * args.steps[1] for i in range(args.steps[0] + 1)])
        print(map(lambda clusmap: float(utility.recursive_length(clusmap.values())) / nproteins, cluster_size))
        print (len([i * args.steps[1] for i in range(args.steps[0] + 1)]))
        print (len(map(lambda clusmap: float(utility.recursive_length(clusmap.values())) / nproteins, cluster_size)))
        ax6.plot([i * args.steps[1] for i in range(args.steps[0] + 1)],
                 map(lambda clusmap: float(utility.recursive_length(clusmap.values())) / nproteins, cluster_size), 'r-',
                 label='Prop. of Proteins in a Cluster')
        fig3.add_axes(ax6)
        fig3.tight_layout()

    if args.p.save is True:
        fig3.savefig(args.p.saveAt)

    if args.rg.show is True or args.energies.show is True or args.nc.show is True or args.p.show is True:
        print ('Close graph to continue..')
        pyplot.show()

    if args.show_dumpfile:
        utility.launch_vmd(args.dumpfile[0], os.path.dirname(os.path.realpath(__file__)) + '/Proteins/vmd.proteins.in')
