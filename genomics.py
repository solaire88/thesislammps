from PyQt4 import QtGui, QtCore, Qt
import subprocess
import sys
import os
import utility
import copy
#import itertools
import state_to_input_genomics

class genomics(QtGui.QMainWindow):
    
    def __init__(self,parent_widget=None,export_to=True):
        """Construct the Qwidget which covers all possible options for extracting data"""
        super(genomics,self).__init__(parent = parent_widget)
        self.setGeometry(100,100,800,200)
        self.setWindowModality(2)
        self.setWindowTitle("DNA From Genomic Data")

        self.mainwidget = QtGui.QWidget(self)
        self.mainwidget.layout = QtGui.QGridLayout(self.mainwidget)

        self.centrewidget = QtGui.QWidget(self)
        self.centrewidget.layout = QtGui.QGridLayout(self.centrewidget)

        self.interactionwidget = QtGui.QWidget(self)
        self.interactionwidget.layout = QtGui.QGridLayout(self.interactionwidget)

        self.protnoWidget = QtGui.QWidget(self)
        self.protnoWidget.layout = QtGui.QGridLayout(self.protnoWidget)

        self.htrwidget = QtGui.QWidget(self)
        self.htrwidget.layout = QtGui.QGridLayout(self.htrwidget)

        self.lowerwidget = QtGui.QWidget(self)
        self.lowerwidget.layout = QtGui.QHBoxLayout(self.lowerwidget)

        self.mainwidget.layout.addWidget(self.centrewidget,0,0)
        self.mainwidget.layout.addWidget(self.interactionwidget,1,0)
        self.mainwidget.layout.addWidget(self.protnoWidget,2,0)
        self.mainwidget.layout.addWidget(self.htrwidget,3,0)
        self.mainwidget.layout.addWidget(self.lowerwidget,4,0)

        self.mainwidget.setLayout(self.mainwidget.layout)

        # State file

        self.stateLabel = QtGui.QLabel("Input File For\nChromatin State Data")
        self.stateTextfield = QtGui.QLineEdit()
        self.stateTextfield.setText("Genome/wgEncodeBroadHmmGm12878HMM.bed")
        self.stateTextfield.setMinimumSize(self.stateTextfield.sizeHint())
        self.stateBtn = QtGui.QPushButton('Set State Inputfile Location')
        self.stateBtn.clicked.connect(lambda: self.filename_action(self.stateTextfield,True,"State Data"))
        self.centrewidget.layout.addWidget(self.stateLabel, 0, 0)
        self.centrewidget.layout.addWidget(self.stateTextfield, 0, 1)
        self.centrewidget.layout.addWidget(self.stateBtn, 0, 2)

        # GC file

        self.gclabel = QtGui.QLabel("Input File For GC\n Content Data (Optional)")
        self.gcTextfield = QtGui.QLineEdit()
        self.gcTextfield.setMinimumSize(self.gcTextfield.sizeHint())
        self.gcBtn = QtGui.QPushButton('Set GC Inputfile Location')
        self.gcBtn.clicked.connect(lambda: self.filename_action(self.gcTextfield, True, "GC Data"))
        self.centrewidget.layout.addWidget(self.gclabel, 0, 3)
        self.centrewidget.layout.addWidget(self.gcTextfield, 0, 4)
        self.centrewidget.layout.addWidget(self.gcBtn, 0, 5)

        # Protein Types

        self.protLabel = QtGui.QLabel("Number of Protein Types:")
        self.protSpinbox = QtGui.QSpinBox()
        self.protSpinbox.setMinimum(0)
        self.protSpinbox.setValue(0)
        self.protno = 0
        self.protSpinbox.valueChanged.connect(lambda: self.proteinchange(self.protSpinbox.value()))
        self.centrewidget.layout.addWidget(self.protLabel, 1, 0)
        self.centrewidget.layout.addWidget(self.protSpinbox, 1, 1)

        # Chromosome No.

        self.chromidLabel = QtGui.QLabel("Chomosome No.:")
        self.chromidTextfield = QtGui.QLineEdit()
        self.chromidTextfield.setText("1")
        self.chromidTextfield.setMinimumSize(self.chromidTextfield.sizeHint())
        self.centrewidget.layout.addWidget(self.chromidLabel, 2, 0)
        self.centrewidget.layout.addWidget(self.chromidTextfield, 2, 1)

        # Chromosome Start/End

        self.chromstartendLabel = QtGui.QLabel("Chomosome Start/End (bp):")
        self.chromstartTextfield = QtGui.QLineEdit()
        #self.chromstartTextfield.setValidator(QtGui.QIntValidator())
        self.chromstartTextfield.setText("start")
        self.chromstartTextfield.setMinimumSize(self.chromstartTextfield.sizeHint())
        self.chromendTextfield = QtGui.QLineEdit()
        #self.chromendTextfield.setValidator(QtGui.QIntValidator())
        self.chromendTextfield.setText("end")
        self.chromendTextfield.setMinimumSize(self.chromendTextfield.sizeHint())
        self.centrewidget.layout.addWidget(self.chromstartendLabel, 2, 2)
        self.centrewidget.layout.addWidget(self.chromstartTextfield, 2, 3)
        self.centrewidget.layout.addWidget(self.chromendTextfield, 2, 4)

        # Bp per simulation bead

        self.simbpLabel = QtGui.QLabel("Bead size in bp:")
        self.simbpTextfield = QtGui.QLineEdit()
        self.simbpTextfield.setValidator(QtGui.QIntValidator())
        self.simbpTextfield.setText("3000")
        self.simbpTextfield.setMinimumSize(self.simbpTextfield.sizeHint())
        self.centrewidget.layout.addWidget(self.simbpLabel,3,0)
        self.centrewidget.layout.addWidget(self.simbpTextfield, 3, 1)


        # Extra Protein Interactions

        self.protBtn = QtGui.QPushButton("Add Extra Protein Interaction")
        self.protBtn.clicked.connect(lambda: self.add_protein(self.protSpinbox.value(),True))
        self.protBtn.setMaximumSize(self.protBtn.sizeHint())

        self.inputBtn = QtGui.QPushButton("Import To Input File Creator")
        #self.inputBtn.clicked.connect(lambda: self.create_input_datafile())
        self.inputBtn.setMaximumSize(self.inputBtn.sizeHint())

        self.inputBtnDNA = QtGui.QPushButton("Import To Input File Creator (DNA only)")
        # self.inputBtn.clicked.connect(lambda: self.create_input_datafile())
        self.inputBtnDNA.setMaximumSize(self.inputBtnDNA.sizeHint())

        self.inputgenBtn = QtGui.QPushButton("Create Input File && Go To Simulation Set Up")
        #self.inputBtn.clicked.connect(lambda: self.create_input_datafile())
        self.inputgenBtn.setMaximumSize(self.inputgenBtn.sizeHint())


        self.HtrLabel = QtGui.QLabel("States marked as heterochromatin:")
        self.addstateBtn = QtGui.QPushButton('Add New State')
        self.addstateBtn.clicked.connect(lambda: self.add_state(self.statehbox2, empty_type='Label'))
        self.statehbox2 = QtGui.QHBoxLayout()
        self.statelabel2 = QtGui.QLabel("None")
        self.statehbox2.addWidget(self.statelabel2)
        self.htrwidget.layout.addWidget(self.HtrLabel, 0, 0)
        self.htrwidget.layout.addLayout(self.statehbox2, 0, 1, 1, 3)
        self.htrwidget.layout.addWidget(self.addstateBtn, 0, 4)
        self.lowerwidget.layout.addWidget(self.protBtn)
        self.lowerwidget.layout.addStretch(0)
        if export_to == True:
            self.lowerwidget.layout.addWidget(self.inputBtn)
            self.lowerwidget.layout.addWidget(self.inputBtnDNA)
        self.lowerwidget.layout.addWidget(self.inputgenBtn)

        self.centrewidget.setLayout(self.centrewidget.layout)
        self.lowerwidget.setLayout(self.lowerwidget.layout)

        self.setCentralWidget(self.mainwidget)

    def filename_action(self,store_to,open,filestr):
        """Open a file selection dialog and store the result in store_to, which should be of type QtGui.LineEdit"""
        if open == False:
            fname = QtGui.QFileDialog.getSaveFileName(self, 'Write {0} At..'.format(filestr),os.getcwd())
        elif open == True:
            fname = QtGui.QFileDialog.getOpenFileName(self, 'Use {0} At..'.format(filestr),os.getcwd())
        store_to.setText(fname)

    def proteinchange(self,spinbox_value):


        if spinbox_value > self.protno:
            self.add_protein(spinbox_value)
            for row in range(self.interactionwidget.layout.rowCount()):
                try:
                    prot_type = int(self.interactionwidget.layout.itemAtPosition(row,1).widget().value())
                except:
                    try:
                        prot_type = int(self.interactionwidget.layout.itemAtPosition(row,1).widget().text())
                    except:
                        prot_type = 0

                if(prot_type > spinbox_value):
                    self.remove_protein(row)
            label = QtGui.QLabel("Number of protein {} atoms:".format(spinbox_value))
            protnoField = QtGui.QLineEdit()
            protnoField.setMaximumSize(protnoField.sizeHint())
            protnoField.setText("1000")
            protnoField.setValidator(QtGui.QIntValidator())
            self.protnoWidget.layout.addWidget(label, spinbox_value - 1, 0)
            self.protnoWidget.layout.addWidget(protnoField, spinbox_value - 1, 4)

        if spinbox_value < self.protno:
            for row in range(self.interactionwidget.layout.rowCount()):
                try:
                    prot_type = int(self.interactionwidget.layout.itemAtPosition(row,1).widget().value())
                except:
                    try:
                        prot_type = int(self.interactionwidget.layout.itemAtPosition(row,1).widget().text())
                    except:
                        prot_type = 0

                if(prot_type > spinbox_value):
                    self.remove_protein(row)
            remove_label = self.protnoWidget.layout.itemAtPosition(spinbox_value, 0).widget()
            self.protnoWidget.layout.removeWidget(remove_label)
            remove_label.deleteLater()
            remove_field = self.protnoWidget.layout.itemAtPosition(spinbox_value, 4).widget()
            self.protnoWidget.layout.removeWidget(remove_field)
            remove_field.deleteLater()
        self.protno = spinbox_value

    def add_protein(self,new_prot_no,spinbox=False):

        statehbox = QtGui.QHBoxLayout()
        rows = self.interactionwidget.layout.rowCount()
        label = QtGui.QLabel("Protein No. ")
        remove_prot = QtGui.QPushButton("X")
        remove_prot.setMaximumWidth(25)
        remove_prot.clicked.connect(lambda: self.remove_protein(rows))
        if spinbox == True:
            protno = QtGui.QSpinBox()
            protno.setValue(new_prot_no)
            protno.setMinimum(1)
        else:
            protno = QtGui.QLabel("{}".format(new_prot_no))
            remove_prot.hide()
        label2 = QtGui.QLabel("binds to states: ")
        statedrop = QtGui.QComboBox()
        statedrop.addItems(["None", "Heterochromatin"])
        statehbox.addWidget(statedrop)
        label3 = QtGui.QLabel("with binding energy:")
        strengthTextfield = QtGui.QLineEdit()
        strengthTextfield.setValidator(QtGui.QDoubleValidator())
        strengthTextfield.setText("4.0")
        strengthTextfield.setMinimumSize(strengthTextfield.sizeHint())
        addstateBtn = QtGui.QPushButton('Add New State')
        addstateBtn.clicked.connect(lambda: self.add_state(statehbox))


        self.interactionwidget.layout.addWidget(label,rows,0)
        self.interactionwidget.layout.addWidget(protno,rows,1)
        self.interactionwidget.layout.addWidget(label2,rows,2)
        self.interactionwidget.layout.addLayout(statehbox,rows,3)
        self.interactionwidget.layout.addWidget(label3,rows,4)
        self.interactionwidget.layout.addWidget(strengthTextfield,rows,5)
        self.interactionwidget.layout.addWidget(addstateBtn,rows,6)
        self.interactionwidget.layout.addWidget(remove_prot, rows, 7)

    def remove_protein(self,row):
        for i in range(8):
            if i!=3:
                to_remove = self.interactionwidget.layout.itemAtPosition(row,i).widget()
                self.interactionwidget.layout.removeWidget(to_remove)
                to_remove.deleteLater()
            else:

                hbox_remove = self.interactionwidget.layout.itemAtPosition(row,i)
                for j in range(hbox_remove.count()):
                    to_remove = hbox_remove.itemAt(0).widget()
                    hbox_remove.removeWidget(to_remove)
                    to_remove.deleteLater()


    def add_state(self,hbox,empty_type='Drop'):

        print hbox.count()
        if hbox.count() == 1:
            to_remove = hbox.itemAt(0).widget()
            hbox.removeWidget(to_remove)
            to_remove.deleteLater()

        new_line = QtGui.QLineEdit()
        remove_line = QtGui.QPushButton("X")
        new_line.setMinimumSize(new_line.sizeHint())
        remove_line.setMaximumWidth(25)
        remove_line.clicked.connect(lambda: self.remove_state(hbox,new_line,remove_line,empty_type))
        hbox.addWidget(new_line)
        hbox.addWidget(remove_line)

    def remove_state(self,hbox,line,button,empty_type='Drop'):
        hbox.removeWidget(line)
        line.deleteLater()
        hbox.removeWidget(button)
        button.deleteLater()
        if hbox.isEmpty() == True and empty_type =='Drop':
            statedrop = QtGui.QComboBox()
            statedrop.addItems(["None", "Heterochromatin"])
            hbox.addWidget(statedrop)
        elif hbox.isEmpty() == True and empty_type =='Label':
            stateLabel = QtGui.QLabel("None")
            hbox.addWidget(stateLabel)


    def gather_args(self):
        args = {}
        print 'Hbox length:'+str(self.statehbox2.count())
        args['infile'] = str(self.stateTextfield.text())
        args['outfile'] = 'bead_colours.chr'+str(self.chromidTextfield.text())+'.txt'
        args['chromosome'] = 'chr'+str(self.chromidTextfield.text())
        if self.chromstartTextfield.text() != 'start':
            args['startbp'] = int(self.chromstartTextfield.text())
        else:
            args['startbp'] = 'start'
        if self.chromendTextfield.text() != 'end':
            args['endbp'] = int(self.chromendTextfield.text())
        else:
            args['endbp'] = 'end'
        args['beadsize'] = int(self.simbpTextfield.text())
        args['heterochromatin'] = [str(self.statehbox2.itemAt(i).widget().text()) for i in range(0,self.statehbox2.count(),2)]
        args['prot_types'] = self.protno
        protein_interactions = {}
        override = []

        for i in range(self.interactionwidget.layout.rowCount()):
            try:
                prot_type = int(self.interactionwidget.layout.itemAtPosition(i, 1).widget().text())
                dna_type_hbox = self.interactionwidget.layout.itemAtPosition(i, 3)
                prot_no  = int(self.protnoWidget.layout.itemAtPosition(prot_type-1,4).widget().text())
                dna_type_text = []
                print dna_type_hbox.count()
                if dna_type_hbox.count() != 1:
                    for j in range(0,dna_type_hbox.count(),2):
                        dna_type_text.append(str(dna_type_hbox.itemAt(j).widget().text()))
                else:
                    dna_type_text.append(str(dna_type_hbox.itemAt(0).widget().currentText()))
                print dna_type_text
                intstrength = float(self.interactionwidget.layout.itemAtPosition(i, 5).widget().text())
            except:
                print 'No Data for row {}'.format(i)
                prot_type = None
                intstrength = None
                dna_type_text = []
                prot_no = None
            if prot_type is not None:
                for key in protein_interactions.keys():
                    if protein_interactions[key][0] == prot_type:
                        # Replace a weaker interaction for the same protein with the stronger one
                        if protein_interactions[key][1] > intstrength:
                            override.append(['{}'.format(i),key])
                        elif intstrength > protein_interactions[key][1]:
                            override.append([key,'{}'.format(i)])
                protein_interactions['{}'.format(i)] = [prot_type,intstrength,dna_type_text,prot_no]
        args['ProtInteraction'] = protein_interactions
        args['override'] = override
        match_id_list = []
        match_name_list = []
        for key in args['ProtInteraction']:
            if str(args['ProtInteraction'][key][2][0]) == 'Heterochromatin':
                match_id_list.append(args['heterochromatin'])
            else:
                match_id_list.append(args['ProtInteraction'][key][2])
            match_name_list.append(key)
        args['match_id_list'] = match_id_list
        args['match_name_list'] = match_name_list
        state_obj = state_to_input_genomics.statedata_to_input_genomics(args['infile'], args['outfile'], args['chromosome'],
                                                                    args['heterochromatin'],
                                                                    [args['startbp'], args['endbp'],
                                                                     args['beadsize'], 90], [0, 1, 2, 3], match_id_list,
                                                                    match_name_list,
                                                                    override)
        #Make a note of how many beads in simulation
        args['dnano'] = state_obj.bin_no
        args['dnatypes'] = state_obj.dna_types
        args['features'] = state_obj.features
        args['count_dict'] = state_obj.count_dict
        args['feature_reduce'] = state_obj.feature_reduce
        args['feat_to_position'] = {i:int(j)-1 for i,j in enumerate(match_name_list)}
        return args
        #print args,match_id_list,match_name_list




    def send_result(self):
        """args = {}
        args['infile'] = self.stateTextfield.text()
        args['outfile'] = 'bead_colours.temp.txt'
        args['chromosome'] = self.chromidTextfield.text()

        state = state_to_input_genomics.statedata_to_input_genomics(args['infile'],args['outfile'],args['chromosome'],
                                                                    args['heterochromatin'],[args['startbp'],args['endbp'],
                                                                    args['beadsize'],90],[0,1,2,3],)"""
        # get args for proteins

