#!/usr/bin/env python
from PyQt4 import QtGui, QtCore, Qt
import subprocess
import sys
import os
import utility
import copy
import genomics

class loadingWindow(QtGui.QMainWindow):

    def __init__(self,parent_widget=None):
        super(loadingWindow,self).__init__(parent=parent_widget)
        self.setGeometry(100,100,300,100)
        self.setWindowModality(2)
        self.setWindowTitle("Creating Input File...")
        self.message = QtGui.QLabel("Creating Input File From Genomic Data..")
        print self.message
        self.setCentralWidget(self.message)

class Interface(QtGui.QMainWindow):
    """A graphical interface for running simulations. Allows a more user-friendly way to generate the long command line
    inputs required when a simulation has a lot of adjustable settings"""

    def __init__(self):
        """Construct a Qt Widget using the superclass('s?) constructor"""

        super(Interface, self).__init__()
        self.drawDefaultUI()


    def drawDefaultUI(self):
        """Fill the window with the components for an equilibration run, along with a component that allows the
        selection of other run types"""

        # Choose your font
        QtGui.QToolTip.setFont(QtGui.QFont('SansSerif', 10))

        # Set a mouseover message
        self.setToolTip('Choose the simulation type from the top menu and add details')

        self.equilAction = QtGui.QAction('&Eqilibration Simulation', self)
        self.equilAction.setShortcut('Ctrl+E')
        self.equilAction.setStatusTip('Equilibration Simulation')
        self.equilAction.triggered.connect(self.loadEquil)

        self.protAction = QtGui.QAction('&Proteins && DNA Simulation', self)
        self.protAction.setShortcut('Ctrl+P')
        self.protAction.setStatusTip('Proteins & DNA Simulation')
        self.protAction.triggered.connect(self.loadProt)

        self.genAction = QtGui.QAction('&Proteins && DNA Simulation (Genomic Data)', self)
        self.genAction.setShortcut('Ctrl+G')
        self.genAction.setStatusTip('Proteins & DNA Simulation (Genomic Data)')
        self.genAction.triggered.connect(self.loadGenome)

        self.cmdMenu = QtGui.QMenu('&Print command line input..')

        self.termAction = QtGui.QAction('&To Terminal',self)
        self.termAction.setShortcut('Ctrl+T')
        self.termAction.setStatusTip('Print to terminal')
        self.termAction.triggered.connect(self.writeTerm)
        self.fileAction = QtGui.QAction('&To File',self)
        self.fileAction.setShortcut('Ctrl+F')
        self.fileAction.setStatusTip('Print to file')
        self.fileAction.triggered.connect(self.writeFile)
        self.cmdMenu.addActions([self.termAction,self.fileAction])
        self.cmdMenu.setDisabled(True)

        self.inputAction = QtGui.QAction('&Create LAMMPS Input File',self)
        self.inputAction.triggered.connect(self.createInput)
        self.inputAction.setShortcut('Ctrl+I')

        self.copyAction = QtGui.QAction('&Copy Selected Items',self)
        self.copyAction.triggered.connect(self.copy)
        self.copyAction.setShortcut('Ctrl+Shift+C')
        self.copyAction.setDisabled(True)

        self.pasteMenu = QtGui.QMenu('&Paste..')
        self.pasteMenu.setDisabled(True)

        # lambda required here as the paste function can take arguments, but does not for the single paste case
        self.pasteAction = QtGui.QAction('&Paste', self)
        self.pasteAction.triggered.connect(lambda:self.paste())
        self.pasteAction.setShortcut('Ctrl+Shift+V')
        self.pasteAction.setDisabled(True)

        self.multipasteAction = QtGui.QAction('&Repeat Paste', self)
        self.multipasteAction.triggered.connect(lambda: self.pasteWindow())
        self.multipasteAction.setShortcut('Ctrl+Alt+V')
        self.multipasteAction.setDisabled(True)

        self.delAction = QtGui.QAction('&Delete Selected Items',self)
        self.delAction.triggered.connect(self.delete)
        self.delAction.setShortcut('Ctrl+Shift+D')
        self.delAction.setDisabled(True)

        self.addDNAAction = QtGui.QAction('&Add New DNA Section',self)
        self.addDNAAction.triggered.connect(self.addDNAwidget)
        self.addDNAAction.setShortcut('Alt+D')
        self.addDNAAction.setDisabled(True)

        self.addProtAction = QtGui.QAction('&Add New Protein Group',self)
        self.addProtAction.triggered.connect(self.addProtwidget)
        self.addProtAction.setShortcut('Alt+P')
        self.addProtAction.setDisabled(True)

        self.pasteMenu.addAction(self.pasteAction)
        self.pasteMenu.addAction(self.multipasteAction)


        # Set a menu with contents and actions to be performed on click

        self.menubar = self.menuBar()
        self.simMenu = self.menubar.addMenu('&Simulation')
        self.simMenu.addActions([self.equilAction,self.protAction,self.genAction])
        self.actMenu = self.menubar.addMenu('&Actions')
        self.actMenu.addMenu(self.cmdMenu)
        self.inputMenu = self.menubar.addMenu('&Inputs')
        self.inputMenu.addAction(self.inputAction)
        self.inputMenu.addAction(self.copyAction)
        self.inputMenu.addMenu(self.pasteMenu)
        self.inputMenu.addAction(self.addDNAAction)
        self.inputMenu.addAction(self.addProtAction)
        self.inputMenu.addAction(self.delAction)

        # Main Widget
        self.mainwidget = QtGui.QWidget(self)

        # Create buttons
        self.runbtn = QtGui.QPushButton('Run Sim.', self)
        self.runbtn.clicked.connect(self.runSimulation)
        self.runbtn.setToolTip('Run the Simulation!')
        self.runbtn.resize(self.runbtn.sizeHint())
        self.runbtn.setDisabled(True)

        self.runvmdbtn = QtGui.QPushButton('Run && Show in VMD')
        self.runvmdbtn.clicked.connect(lambda :self.runSimulation(vmd=True))
        self.runvmdbtn.setToolTip('Run the simulation and load the dumpfile in VMD')
        self.runvmdbtn.resize(self.runvmdbtn.sizeHint())
        self.runvmdbtn.setDisabled(True)


        self.cancelbtn = QtGui.QPushButton('Cancel', self)
        self.cancelbtn.clicked.connect(QtCore.QCoreApplication.instance().quit)
        self.cancelbtn.setToolTip('Close Window')
        self.cancelbtn.resize(self.cancelbtn.sizeHint())

        self.addDNAbtn = QtGui.QPushButton('Add DNA Section')
        self.addDNAbtn.clicked.connect(self.addDNAwidget)
        self.addDNAbtn.resize(self.addDNAbtn.sizeHint())
        self.addDNAbtn.hide()
        
        self.addDNAfromfilebtn = QtGui.QPushButton('Create Input DNA From File')
        self.addDNAfromfilebtn.clicked.connect(self.addDNAfromfile)
        self.addDNAfromfilebtn.resize(self.addDNAfromfilebtn.sizeHint())
        self.addDNAfromfilebtn.hide()

        self.addProtbtn = QtGui.QPushButton('Add Protein Group')
        self.addProtbtn.clicked.connect(self.addProtwidget)
        self.addProtbtn.resize(self.addProtbtn.sizeHint())
        self.addProtbtn.hide()

        self.createInputbtn = QtGui.QPushButton('Create Inputfile')
        self.createInputbtn.clicked.connect(self.make_inputfile)
        self.createInputbtn.resize((self.createInputbtn.sizeHint()))
        self.createInputbtn.hide()
        
        


        self.simMessage = QtGui.QLabel("Select a simulation type from the Simulation menu",self.mainwidget)

        self.mainwidget.layout = QtGui.QGridLayout(self.mainwidget)
        self.mainwidget.layout.addWidget(self.simMessage,0,0,1,5)
        self.mainwidget.layout.addWidget(self.cancelbtn, 2, 0)
        self.mainwidget.layout.addWidget(self.runvmdbtn, 2, 3)
        self.mainwidget.layout.addWidget(self.runbtn, 2, 4)

        self.mainwidget.setLayout(self.mainwidget.layout)


        self.setCentralWidget(self.mainwidget)

        self.setGeometry(100, 100, 700, 150)
        self.setWindowTitle('Enter Simulation Details')

        desk = QtGui.QDesktopWidget().availableGeometry()
        self.screenwidth = desk.width()
        self.screenheight = desk.height()


        #window = QtGui.QWidget(self)
        self.show()

    def pasteWindow(self):
        w = QtGui.QDialog()
        w.setGeometry(300,300,300,100)
        w.setWindowTitle("Paste Copied Objects N Times")
        hbox = QtGui.QHBoxLayout()
        pasteMulti = QtGui.QLineEdit()
        pasteMulti.setText('1')
        pasteMulti.setValidator(QtGui.QIntValidator())
        hbox.addWidget(pasteMulti)
        pasteBtn = QtGui.QPushButton('Paste!')
        pasteBtn.clicked.connect(lambda: self.paste_and_close(int(pasteMulti.text()),w))
        hbox.addWidget(pasteBtn)
        w.setLayout(hbox)
        w.exec_()

    def paste_and_close(self,repeat,window):
        self.paste(repeat)
        window.close()

    def createInput(self):
        self.selectedsim = None
        self.active = 'in'

        # Hide run simulation buttons

        self.runvmdbtn.hide()
        self.runbtn.hide()

        #Show Input file creator buttons
        self.mainwidget.layout.addWidget(self.addDNAbtn,2,3)
        self.addDNAbtn.show()
        self.addDNAAction.setEnabled(True)
        self.mainwidget.layout.addWidget(self.addProtbtn, 2, 2)
        self.addProtbtn.show()
        self.addProtAction.setEnabled(True)
        self.createInputbtn.show()
        self.mainwidget.layout.addWidget(self.createInputbtn,2,4)
        self.mainwidget.layout.addWidget(self.addDNAfromfilebtn,2,1)
        self.addDNAfromfilebtn.show()
        self.copyAction.setEnabled(True)
        self.delAction.setEnabled(True)


        # Show add DNA, add proteins and create inputfile buttons

        currentSimParamWidget = self.mainwidget.layout.itemAtPosition(0, 0).widget()
        self.mainwidget.layout.removeWidget(currentSimParamWidget)
        currentSimParamWidget.deleteLater()

        self.inputWidget = QtGui.QWidget(self.mainwidget)
        self.inputWidget.layout = QtGui.QVBoxLayout()

        # Global Simulation Parameters

        globalBounds = QtGui.QHBoxLayout()
        globalBounds.addWidget(QtGui.QLabel('Global Simulation Properties:'))
        globalBounds.addStretch(1)

        for label in ['X Min', 'X Max', 'Y Min', 'Y max', 'Z Min', 'Z max']:
            globalBounds.addWidget(QtGui.QLabel(label + ':'))
            lineEd = QtGui.QLineEdit()
            lineEd.setValidator(QtGui.QDoubleValidator())
            if 'Min' in label:
                lineEd.setText('-100.0')
            else:
                lineEd.setText('100.0')
            globalBounds.addWidget(lineEd)

        connectAll = QtGui.QCheckBox('Connect all DNA sections')
        connectAll.stateChanged.connect(lambda:self.connectAll(connectAll.isChecked()))

        globalBounds.addStretch(1)
        globalBounds.addWidget(connectAll)

        LmpFileLabel = QtGui.QLabel('LAMMPS Input File Name:')
        LmpFileField = QtGui.QLineEdit()
        LmpFileField.setText('LAMMPSInput.lmp')
        LmpFileField.setMinimumSize(LmpFileField.sizeHint())

        globalBounds.addWidget(LmpFileLabel)
        globalBounds.addWidget(LmpFileField)

        LmpFileCopiesLabel = QtGui.QLabel('Copies')
        LmpFileCopiesSpinbox = QtGui.QSpinBox()
        LmpFileCopiesSpinbox.setValue(1)
        LmpFileCopiesSpinbox.setMinimum(1)

        globalBounds.addWidget(LmpFileCopiesLabel)
        globalBounds.addWidget(LmpFileCopiesSpinbox)

        self.inputWidget.layout.addLayout(globalBounds)
        self.inputWidget.setLayout(self.inputWidget.layout)


        if self.screenwidth < 1400:
            self.setGeometry(100, 100, self.screenwidth, 200)
        else:
            self.setGeometry(100, 100, 1400, 200)

        inputheight = self.inputWidget.size().height()
        inputwidth = self.inputWidget.size().width()

        self.scrollArea = QtGui.QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setGeometry(100, 100, inputwidth, inputheight)
        self.scrollArea.setWidget(self.inputWidget)

        self.mainwidget.layout.addWidget(self.scrollArea, 0, 0, 1, 5)





    def copy(self):
        self.copiedArgs = []
        self.pasteMenu.setEnabled(True)
        self.pasteAction.setEnabled(True)
        self.multipasteAction.setEnabled(True)
        for vbox_index in range(1,self.inputWidget.layout.count()):
            selected = self.inputWidget.layout.itemAt(vbox_index).widget().layout.itemAt(0).itemAt(0).itemAt(1).widget().isChecked()
            if selected is True:
                style = self.inputWidget.layout.itemAt(vbox_index).widget().layout.itemAt(0).itemAt(0).itemAt(
                    0).widget().text()
                print style
                if style == 'DNA':
                    widgetArgs = self.getDNAWidgetargs(self.inputWidget.layout.itemAt(vbox_index).widget())
                    self.copiedArgs.append(widgetArgs)
                elif style == 'Proteins':
                    widgetArgs = self.getProtWidgetargs(self.inputWidget.layout.itemAt(vbox_index).widget())
                    self.copiedArgs.append(widgetArgs)
            print self.copiedArgs

    def paste(self,repeat=1):
        for i in range(repeat):
            #for widget in self.copiedItems:
            for widgetargs in self.copiedArgs:
                current_index = self.inputWidget.layout.count()
                if widgetargs['type'] == 'DNA':
                    widget = self.basicDNA(current_index,widgetargs)
                    self.inputWidget.layout.addWidget(widget)
                if widgetargs['type'] == 'Proteins':
                    widget = self.basicProt(current_index,widgetargs)
                    self.inputWidget.layout.addWidget(widget)
        self.inputWidget.setLayout(self.inputWidget.layout)

    def delete(self):
        reconstruct_list = []
        new_index = 1
        for vbox_index in range(1, self.inputWidget.layout.count()):
            selected = self.inputWidget.layout.itemAt(vbox_index).widget().layout.itemAt(0).itemAt(0).itemAt(
                1).widget().isChecked()
            if selected is False:
                widgetArgs = self.getDNAWidgetargs(self.inputWidget.layout.itemAt(vbox_index).widget())
                reconst_widget = self.basicDNA(new_index,widgetArgs)
                reconstruct_list.append(reconst_widget)
                new_index += 1
        for vbox_index in range(1, self.inputWidget.layout.count()):
            self.inputWidget.layout.itemAt(vbox_index).widget().deleteLater()
        for reconst in reconstruct_list:
            self.inputWidget.layout.addWidget(reconst)
            self.inputWidget.setLayout(self.inputWidget.layout)
            
    def addDNAfromfile(self):
        if self.active == 'in':
            self.genomics = genomics.genomics(export_to=True)
        elif self.active == 'genome':
            self.genomics = genomics.genomics(export_to=False)
        print self.genomics
        self.genomics.show()
        self.genomics.inputBtn.clicked.connect(lambda: self.import_to_file_creator())
        self.genomics.inputBtnDNA.clicked.connect(lambda: self.import_to_file_creator(False))
        self.genomics.inputgenBtn.clicked.connect(lambda: self.set_up_genomics_sim())
        #genomic_data = self.genomics.send_result()

    def set_up_genomics_sim(self):
        args = self.genomics.gather_args()
        print args
        self.genomics.close()
        self.createInput()
        DNAdict = self.genomicDNAdict(args)
        current_index = self.inputWidget.layout.count()
        DNAwidget = self.basicDNA(current_index, DNAdict)
        self.inputWidget.layout.addWidget(DNAwidget)
        self.inputWidget.setLayout(self.inputWidget.layout)

        type_to_protno = {}
        for interact in args['ProtInteraction']:
            interact_list = args['ProtInteraction'][interact]
            type_to_protno[interact_list[0]] = interact_list[3]

        for i in range(args['prot_types']):
            current_index = self.inputWidget.layout.count()
            print 'Import - current index'
            print current_index
            Protdict = self.genomicProtdict(args['dnatypes'] + i + 1, type_to_protno[i + 1])
            print Protdict
            ProtWidget = self.basicProt(current_index, Protdict)
            print ProtWidget
            self.inputWidget.layout.addWidget(ProtWidget)
            self.inputWidget.setLayout(self.inputWidget.layout)

        args['lammps-filename'] = os.getcwd()+'/LAMMPSInput.{}.lmp'.format(args['chromosome'])
        self.inputWidget.layout.itemAt(0).itemAt(17).widget().setText(args['lammps-filename'])

        # Set simulation box based on R_{g}^{2} /approx Na^{2}

        if DNAdict['atomno'] > 10000:
            half_box_length  = float(DNAdict['atomno']) ** (0.5)
            for itemLoc in [3,7,11]:
                self.inputWidget.layout.itemAt(0).itemAt(itemLoc).widget().setText(str(-half_box_length))
            for itemLoc in [5,9,13]:
                self.inputWidget.layout.itemAt(0).itemAt(itemLoc).widget().setText(str(half_box_length))

        self.make_inputfile()

        self.loadGenome()
        genomeMainWidget = self.mainwidget.layout.itemAtPosition(0, 0).widget()

        # Set input filenme
        genomeMainWidget.layout.itemAtPosition(1,1).widget().setText(args['lammps-filename'])

        # Set prot types
        for i in range(2,args['prot_types']+1):
            genomeMainWidget.layout.itemAtPosition(9, 1).widget().setValue(i)

        #Add extra interactions
        for extrainteract in range(args['prot_types'],len(args['ProtInteraction'])):
            genomeMainWidget.layout.itemAtPosition(9, 2).widget().click()

        assigned_proteins = []
        extras_assigned = 0
        for key in args['ProtInteraction']:
            interact = args['ProtInteraction'][key]
            # Fill up the sequential protein nos
            if interact[0] not in assigned_proteins:
                genomeMainWidget.layout.itemAtPosition(10,0).itemAtPosition(interact[0]-1,5).widget().setText(str(interact[1]))
                assigned_proteins.append(interact[0])
            # Use the next available 'extra' space
            else:
                genomeMainWidget.layout.itemAtPosition(10, 0).itemAtPosition(args['prot_types'] + extras_assigned,
                                                                             5).widget().setText(str(interact[1]))
                genomeMainWidget.layout.itemAtPosition(10, 0).itemAtPosition(args['prot_types'] + extras_assigned,
                                                                             1).widget().setValue(interact[0])
                position = args['prot_types'] + extras_assigned
                extras_assigned += 1

            """if interact[2] == ['Heterochromatin']:
                genomeMainWidget.layout.itemAtPosition(10, 0).itemAtPosition(position, 3).itemAt(
                    0).widget().setCurrentIndex(1)
            elif interact[2] == ['None']:
                genomeMainWidget.layout.itemAtPosition(10, 0).itemAtPosition(position,3).itemAt(
                    0).widget().setCurrentIndex(0)
            else:
                for i,match in enumerate(interact[2]):
                    genomeMainWidget.layout.itemAtPosition(10, 0).itemAtPosition(position, 6).widget().click()
                    genomeMainWidget.layout.itemAtPosition(10, 0).itemAtPosition(position, 3).itemAt(2*i).widget().setText(match)
            """
        types_per_line = {i:0 for i in range(args['prot_types'] + extras_assigned)}
        for featuregroup in args['feature_reduce']:
            for feature in featuregroup:
                print args['feat_to_position'],feature,args['feat_to_position'][feature]
                genomeMainWidget.layout.itemAtPosition(10, 0).itemAtPosition(args['feat_to_position'][feature], 6).widget().click()
                genomeMainWidget.layout.itemAtPosition(10, 0).itemAtPosition(args['feat_to_position'][feature], 3).itemAt(2 * types_per_line[args['feat_to_position'][feature]]).widget().setText(
                str(args['feature_reduce'][featuregroup]))
                types_per_line[args['feat_to_position'][feature]]+=1


        # Add Heterochromatin states
        for i,htrstate in enumerate(args['heterochromatin']):
            if htrstate != 'None':
                genomeMainWidget.layout.itemAtPosition(11,4).widget().click()
                genomeMainWidget.layout.itemAtPosition(11, 1).itemAt(2*i).widget().setText(htrstate)

    def import_to_file_creator(self,prot=True):
        args = self.genomics.gather_args()
        self.genomics.close()
        DNAdict = self.genomicDNAdict(args)
        current_index = self.inputWidget.layout.count()
        DNAwidget = self.basicDNA(current_index,DNAdict)
        self.inputWidget.layout.addWidget(DNAwidget)
        self.inputWidget.setLayout(self.inputWidget.layout)
        if prot:
            type_to_protno = {}
            for interact in args['ProtInteraction']:
                interact_list = args['ProtInteraction'][interact]
                type_to_protno[interact_list[0]] = interact_list[3]

            for i in range(args['prot_types']):
                current_index = self.inputWidget.layout.count()
                print 'Import - current index'
                print current_index
                Protdict = self.genomicProtdict(args['dnatypes']+i+1,type_to_protno[i+1])
                print Protdict
                ProtWidget = self.basicProt(current_index,Protdict)
                print ProtWidget
                self.inputWidget.layout.addWidget(ProtWidget)
                self.inputWidget.setLayout(self.inputWidget.layout)

    def genomicDNAdict(self,genome_args):
        DNAdict={}
        DNAdict['dnatypeIndex'] = 0
        DNAdict['atomno'] = str(genome_args['dnano'])
        DNAdict['typesfile'] = genome_args['outfile']
        DNAdict['actfield'] = ['Set Atom Types From File']
        return DNAdict

    def genomicProtdict(self,type,no):
        Protdict = {}
        Protdict['atomno'] = no
        Protdict['atomtype'] = type
        return Protdict

    def rebuildWidget(self,widget,hboxcopy,i):
        """since you can't copy directly, check what type a widget is and fill in parameters"""
        if 'QLabel' in str(type(widget)):
            widget_copy = QtGui.QLabel(widget.text())
            return widget_copy

        # Special case for add extra field combobox
        if 'QComboBox' in str(type(widget)):
            widget_copy = QtGui.QComboBox()
            for i in range(widget.count()):
                print widget.itemText(i)
                widget_copy.addItem(widget.itemText(i))
            widget_copy.setCurrentIndex(widget.currentIndex())

            return widget_copy

        if 'QLineEdit' in str(type(widget)):
            widget_copy = QtGui.QLineEdit()
            widget_copy.setText(widget.text())
            widget_copy.setValidator(widget.validator())
            return widget_copy

        if 'QCheckBox' in str(type(widget)):
            widget_copy = QtGui.QCheckBox()
            widget_copy.setChecked(widget.isChecked())
            return widget_copy

        if 'QSpinBox' in str(type(widget)):
            widget_copy = QtGui.QSpinBox()
            widget_copy.setMinimum(1)
            widget_copy.setValue(widget.value())
            return widget_copy

        # Special case for file select pushbutton
        if 'QPushButton' in str(type(widget)):
            widget_copy = QtGui.QPushButton(widget.text())
            widget_copy.clicked.connect(lambda: self.write_to(hboxcopy.itemAt(i-1).widget(), True, 'Atom Types File'))
            return widget_copy

        # Special case for add extra field combobox

    def connectAll(self,checked):
        for vbox_index in range(1,self.inputWidget.layout.count()):
            label =  self.inputWidget.layout.itemAt(vbox_index).widget().layout.itemAt(0).itemAt(0).itemAt(0).widget().text()
            if label == 'DNA':
                checkBox = self.inputWidget.layout.itemAt(vbox_index).widget().layout.itemAt(0).itemAt(0).itemAt(10).widget()
                if checked:
                    checkBox.setDisabled(True)
                    checkBox.setChecked(True)
                else:
                    checkBox.setEnabled(True)
                    checkBox.setChecked(False)

    def runSimulation(self,vmd=False):
        args = self.getArgs()
        call_string_list = self.getCallString(args)
        if vmd is True:
            call_string_list.append('-show')
        # Add python file and libraries to path in case this wasn't done by install
        """try:
            os.environ['LD_LIBRARY_PATH'] += (':' + os.path.dirname((os.path.realpath(sys.argv[0]))) + '/lammps/src/')
        except KeyError:
            os.environ['LD_LIBRARY_PATH'] = os.path.dirname((os.path.realpath(sys.argv[0]))) + '/lammps/src/'

        try:
            os.environ['PYTHONPATH'] += (':' + os.path.dirname(os.path.realpath(sys.argv[0])) + '/lammps/python/')
        except KeyError:
            os.environ['PYTHONPATH'] = os.path.dirname(os.path.realpath(sys.argv[0])) + '/lammps/python/'"""

        sys.path.append(os.path.dirname(os.path.realpath(sys.argv[0]))+'/lammps/python/')
        
        subprocess.check_call(call_string_list)
        QtCore.QCoreApplication.instance().quit()

    def writeTerm(self):
        """Write to terminal a command line input to run the simulation with current settings."""

        args = self.getArgs()
        call_string_list = self.getCallString(args)
        call_string = ''
        for item in call_string_list[:-1]:
            call_string += str(item) + ' '
        call_string += call_string_list[-1]
        print call_string

    def writeFile(self):
        """Write to file a command line input to run the simulation with current settings."""

        args = self.getArgs()
        call_string_list = self.getCallString(args)
        fname = QtGui.QFileDialog.getSaveFileName(self, 'Write At..', os.getcwd())
        cmdFile = open(str(fname),'w')
        for item in call_string_list[:-1]:
            cmdFile.write(str(item) + ' ')
        cmdFile.write(call_string_list[-1])
        cmdFile.close()

    def getCallString(self,args):
        call_string = ['python']

        if args['lib'][0] in ['g++_openmpi','g++_mpich','icc_openmpi','icc_mpich']:
            call_string = ['mpirun', '-np', str(args['nprocs'][0]), 'python']
        call_string.append(str(self.selectedsim))

        for argname in args:
            if argname != 'nprocs':
                arglist = args[argname]
                if arglist is True:
                    call_string.append('-' + str(argname))
                else:
                    call_string.append('-' + str(argname))
                    for argvalue in arglist:
                        call_string.append(str(argvalue))
        return call_string

    def getArgs(self):

        args = {}

        # Get args common to equil and proteins
        currentSimParamWidget = self.mainwidget.layout.itemAtPosition(0, 0).widget()

        if self.active in ['eq','prot','genome']:

            args['s'] = self.getValuesfromLine(currentSimParamWidget.layout,[[0,1],[0,4]])
            if self.getValuefromCombo(currentSimParamWidget.layout,2,6) == 'Yes':
                args['d'] = self.getValuesfromLine(currentSimParamWidget.layout,[[2,1],[2,4]])
            if self.getValuefromCombo(currentSimParamWidget.layout,1,4) =='Datafile':
                args['in'] = [self.getValuefromLine(currentSimParamWidget.layout,1,1)]
            elif self.getValuefromCombo(currentSimParamWidget.layout,1,4) =='Restartfile':
                args['rest'] = [self.getValuefromLine(currentSimParamWidget.layout,1,1)]
            args['lib'] = [self.getValuefromCombo(currentSimParamWidget.layout,3,1)]
            args['nprocs'] = [self.getValuefromLine(currentSimParamWidget.layout,3,4)]
            args['a'] = [self.getValuefromLine(currentSimParamWidget.layout,4,1)]
            if self.getCheckBoxState(currentSimParamWidget.layout,5,0) is True:
                args['eqsoft'] = [self.getValuefromLine(currentSimParamWidget.layout,5,3)]
                args['ainit'] = [self.getValuefromLine(currentSimParamWidget.layout,5,5)]
            else:
                args['eqsoft'] = [0]
                args['ainit'] = args['a']

        # Get graphs for all run types

        if self.active in ['eq','prot']:

            graphHbox = currentSimParamWidget.layout.itemAtPosition(6, 0)

            if self.active == 'eq':

                graphList = ['e']
                comboboxValue = self.getValuefromCombo(graphHbox, 4, 1)
                if comboboxValue == 'No Graph':
                    pass
                elif comboboxValue == 'Show (Rad. Gyration Only)':
                    args['rg'] = True
                elif comboboxValue == 'Show (Temp. Only)':
                    args['t'] = True
                elif comboboxValue == 'Show (Both)':
                    args['rg'] = True
                    args['t'] = True
                elif comboboxValue == 'Save (Rad. Gyration Only)':
                    args['rg'] = [self.getValuefromLine(graphHbox,4, 2)]
                elif comboboxValue == 'Save (Temp. Only)':
                    args['t'] = [self.getValuefromLine(graphHbox,4, 2)]
                elif comboboxValue == 'Save (Both)':
                    args['rg'] = [self.getValuefromLine(graphHbox,4, 2)]
                    args['t'] = [self.getValuefromLine(graphHbox,4, 2)]

            elif self.active == 'prot':
                graphList = ['e', 'rg']
                comboboxValue = self.getValuefromCombo(graphHbox, 6, 1)
                if comboboxValue == 'No Graph':
                    pass
                elif comboboxValue == 'Show (Clusters Only)':
                    args['nc'] = True
                elif comboboxValue == 'Show (Proteins Only)':
                    args['p'] = True
                elif comboboxValue == 'Show (Both)':
                    args['nc'] = True
                    args['p'] = True
                elif comboboxValue == 'Save (Clusters Only)':
                    args['nc'] = [self.getValuefromLine(graphHbox, 6, 2)]
                elif comboboxValue == 'Save (Proteins Only)':
                    args['p'] = [self.getValuefromLine(graphHbox, 6, 2)]
                elif comboboxValue == 'Save (Both)':
                    args['nc'] = [self.getValuefromLine(graphHbox, 6, 2)]
                    args['p'] = [self.getValuefromLine(graphHbox, 6, 2)]

            for boxIndex,argKey in enumerate(graphList):
                if self.getValuefromCombo(graphHbox, 2*(boxIndex+1), 1) == 'No Graph':
                    pass
                elif self.getValuefromCombo(graphHbox, 2*(boxIndex+1), 1) == 'Show':
                    args[argKey] = True
                elif self.getValuefromCombo(graphHbox, 2*(boxIndex+1), 1) == 'Save':
                    args[argKey] = [self.getValuefromLine(graphHbox, 2*(boxIndex+1), 2)]

        # Get extra info for protein runs

        if self.active == 'prot':
            args['eq'] = [self.getValuefromLine(currentSimParamWidget.layout, 7, 1)]
            args['intenergy'] = [self.getValuefromLine(currentSimParamWidget.layout, 8, 1)]
            args['intcutoff'] = [self.getValuefromLine(currentSimParamWidget.layout, 8, 4)]

        if self.active == 'genome':
            args['eq']  = [self.getValuefromLine(currentSimParamWidget.layout, 7, 1)]
            args['interact'] = []
            args['prot_types']  = [currentSimParamWidget.layout.itemAtPosition(9,1).widget().value()]
            interact_line_no = currentSimParamWidget.layout.itemAtPosition(10,0).rowCount()

            # Record the interactions between proteins and DNa from interface

            for i in range(interact_line_no):
                print currentSimParamWidget.layout.itemAtPosition(10,0).rowCount()
                print currentSimParamWidget.layout.itemAtPosition(10,0)
                try:
                    prot_no = currentSimParamWidget.layout.itemAtPosition(10,0).itemAtPosition(i,1).widget().text()
                except:
                    prot_no = currentSimParamWidget.layout.itemAtPosition(10, 0).itemAtPosition(i, 1).widget().value()
                print prot_no

                DNAhbox = currentSimParamWidget.layout.itemAtPosition(10,0).itemAtPosition(i,3)
                intstr = currentSimParamWidget.layout.itemAtPosition(10,0).itemAtPosition(i,5).widget().text()
                if DNAhbox.count() != 1:
                    for j in range(0,DNAhbox.count(),2):
                        dnaVal = DNAhbox.itemAt(j).widget().text()
                        args['interact'].append([int(prot_no),int(dnaVal),float(intstr),1.8])
                else:
                    pass

            # Change protein types to be += max dna type
            if args['interact']!=[]:
                max_dna_type = max(zip(*args['interact'])[1])

                for i in range(len(args['interact'])):
                    args['interact'][i][0] += max_dna_type


                args['interact'] = utility.flatten_list(args['interact'])

        print args

        return args

    def getValuesfromLine(self,layout,index_pair_list):
        for boxtype in ['QHBoxLayout','QVBoxLayout','QBoxLayout']:
            if boxtype in str(type(layout)):
                return [layout.itemAt(i).itemAt(j).widget().text() for i,j in index_pair_list]
        if 'QGridLayout' in str(type(layout)):
            return [layout.itemAtPosition(i,j).widget().text() for i,j in index_pair_list]

    def getValuefromLine(self,layout,i,j,k = None):
        for boxtype in ['QHBoxLayout','QVBoxLayout','QBoxLayout']:
            if boxtype in str(type(layout)):
                if k is None:
                    return layout.itemAt(i).itemAt(j).widget().text()
                else:
                    return layout.itemAt(i).itemAt(j).itemAt(k).widget().text()
        if 'QGridLayout' in str(type(layout)):
            return layout.itemAtPosition(i,j).widget().text()

    def getValuefromCombo(self,layout,i,j,k = None):
        for boxtype in ['QHBoxLayout','QVBoxLayout','QBoxLayout']:
            if boxtype in str(type(layout)):
                if k is None:
                    return layout.itemAt(i).itemAt(j).widget().currentText()
                else:
                    return layout.itemAt(i).itemAt(j).itemAt(k).widget().currentText()
        if 'QGridLayout' in str(type(layout)):
            if k is None:
                return layout.itemAtPosition(i,j).widget().currentText()
            else:
                return layout.itemAtPosition(i,j).itemAt(k).widget().currentText()
            
    def getCheckBoxState(self,layout,i,j, k = None):
        for boxtype in ['QHBoxLayout','QVBoxLayout','QBoxLayout']:
            if boxtype in str(type(layout)):
                if k is None:
                    return layout.itemAt(i).itemAt(j).widget().isChecked()
                else:
                    return layout.itemAt(i).itemAt(j).itemAt(k).widget().isChecked()
        if 'QGridLayout' in str(type(layout)):
            if k is None:
                return layout.itemAtPosition(i,j).widget().isChecked()
            else:
                return layout.itemAtPosition(i,j).itemAt(k).widget().isChecked()

    def addProtwidget(self):
        next_index = self.inputWidget.layout.count()
        prot = self.basicProt(next_index)
        self.inputWidget.layout.addWidget(prot)
        self.inputWidget.setLayout(self.inputWidget.layout)
        inputheight = self.inputWidget.size().height()
        inputwidth = self.inputWidget.size().width()
        if (inputheight < self.screenheight):
            self.setGeometry(100,100,1400,inputheight+100)
        #if (inputwidth < self.screenwidth and inputheight < self.screenheight):
        #    self.scrollArea.setGeometry(100, 100, inputwidth, inputheight)
        #elif (inputwidth > self.screenwidth and inputheight > self.screenheight):
        #    self.scrollArea.setGeometry(100, 100, self.screenwidth, self.screenheight)
        #elif (inputwidth > self.screenwidth):
        #    self.scrollArea.setGeometry(100, 100, self.screenwidth, inputheight)
        #elif (inputheight > self.screenheight):
        #    self.scrollArea.setGeometry(100, 100, inputwidth, self.screenheight)

    def addDNAwidget(self):
        """Add a HBox Layout which contains 1 HBox (default parameters), N Hbox(es?) (optional parameters)"""
        next_index = self.inputWidget.layout.count()
        dna = self.basicDNA(next_index)
        self.inputWidget.layout.addWidget(dna)
        self.inputWidget.setLayout(self.inputWidget.layout)
        inputheight = self.inputWidget.size().height()
        inputwidth = self.inputWidget.size().width()

        if (inputheight < self.screenheight):
            self.setGeometry(100,100,1400,inputheight+100)

        #if (inputwidth < self.screenwidth and inputheight < self.screenheight):
        #    self.scrollArea.setGeometry(100, 100, inputwidth, inputheight)
        #elif(inputwidth > self.screenwidth and inputheight > self.screenheight):
        #    self.scrollArea.setGeometry(100, 100, self.screenwidth, self.screenheight)
        #elif (inputwidth > self.screenwidth):
        #    self.scrollArea.setGeometry(100, 100, self.screenwidth, inputheight)
        #elif (inputheight > self.screenheight):
        #    self.scrollArea.setGeometry(100, 100, inputwidth, self.screenheight)


    def getProtWidgetargs(self,ProtWidget):
        protData = {}
        fixedHbox = ProtWidget.layout.itemAt(0).itemAt(0)
        protData['type'] = fixedHbox.itemAt(0).widget().text()
        protData['atomno'] = fixedHbox.itemAt(3).widget().text()
        protData['atomtype'] = fixedHbox.itemAt(5).widget().value()
        protData['atomsize'] = fixedHbox.itemAt(7).widget().text()

        """extraHbox = ProtWidget.layout.itemAt(0).itemAt(1)

        if extraHbox.count() == 2:
            extraCount = extraHbox.itemAt(1).widget().count()
            extraCombo = extraHbox.itemAt(1).widget()
            comboList = ['Set Atom Types From File','Set DNA Section Specific Boundary']
        else:
            extraCount = 0
            comboList = []

        for i in range(extraCount):
            text = extraCombo.itemText(i)
            comboList.remove(text)

        protData['actfield'] = comboList
        # Iterate over fixed, extra(sectionsize,atomtype,dnaboundary)

        protData['bstyle'] = None
        protData['typesfile'] = None

        for i in range(1,ProtWidget.layout.count()):
            FirstLabelText = ProtWidget.layout.itemAt(i).itemAt(0).widget().text()
            if FirstLabelText == 'Set Section Size:':
                protData['sectionsize'] = ProtWidget.layout.itemAt(i).itemAt(1).widget().text()
            if FirstLabelText == 'Boundary Type:':
                protData['bstyle'] = ProtWidget.layout.itemAt(i).itemAt(1).widget().currentText()
                if protData['bstyle'] == 'Rectangular':
                    protData['bdryrct'] = {}
                    protData['bdryrct']['X Min'] = ProtWidget.layout.itemAt(i).itemAt(3).widget().text()
                    protData['bdryrct']['X Max'] = ProtWidget.layout.itemAt(i).itemAt(5).widget().text()
                    protData['bdryrct']['Y Min'] = ProtWidget.layout.itemAt(i).itemAt(7).widget().text()
                    protData['bdryrct']['Y Max'] = ProtWidget.layout.itemAt(i).itemAt(9).widget().text()
                    protData['bdryrct']['Z Min'] = ProtWidget.layout.itemAt(i).itemAt(11).widget().text()
                    protData['bdryrct']['Z Max'] = ProtWidget.layout.itemAt(i).itemAt(13).widget().text()
                elif protData['bstyle'] == 'Cylindrical':
                    protData['bdrycyl'] = {}
                    protData['bdrycyl']['R'] = ProtWidget.layout.itemAt(i).itemAt(3).widget().text()
                    protData['bdrycyl']['Z Min'] = ProtWidget.layout.itemAt(i).itemAt(5).widget().text()
                    protData['bdrycyl']['Z Max'] = ProtWidget.layout.itemAt(i).itemAt(7).widget().text()
            if FirstLabelText == 'Read Atom Types From File:':
                protData['typesfile'] = ProtWidget.layout.itemAt(i).itemAt(1).widget().text()
        """
        return protData

    def getDNAWidgetargs(self,DNAWidget):
        dnaData = {}
        fixedHbox = DNAWidget.layout.itemAt(0).itemAt(0)
        dnaData['type'] = fixedHbox.itemAt(0).widget().text()
        dnaData['dnatypeIndex'] = fixedHbox.itemAt(3).widget().currentIndex()
        dnaData['atomno'] = fixedHbox.itemAt(5).widget().text()
        dnaData['atomtype'] = fixedHbox.itemAt(7).widget().value()
        dnaData['atomsize'] = fixedHbox.itemAt(9).widget().text()
        dnaData['connectcheck'] = fixedHbox.itemAt(10).widget().isChecked()
        dnaData['startX'] = fixedHbox.itemAt(12).widget().text()
        dnaData['startY'] = fixedHbox.itemAt(13).widget().text()
        dnaData['startZ'] = fixedHbox.itemAt(14).widget().text()
        if fixedHbox.itemAt(16).widget().isVisible():
            dnaData['twistno'] = fixedHbox.itemAt(16).widget().text()
        else:
            dnaData['twistno'] = None

        extraHbox = DNAWidget.layout.itemAt(0).itemAt(1)

        if extraHbox.count() == 2:
            extraCount = extraHbox.itemAt(1).widget().count()
            extraCombo = extraHbox.itemAt(1).widget()
            comboList = ['Set Section Size','Set Atom Types From File','Set DNA Section Specific Boundary']
        else:
            extraCount = 0
            comboList = []

        for i in range(extraCount):
            text = extraCombo.itemText(i)
            comboList.remove(text)

        dnaData['actfield'] = comboList
        # Iterate over fixed, extra(sectionsize,atomtype,dnaboundary)

        dnaData['sectionsize'] = None
        dnaData['bstyle'] = None
        dnaData['typesfile'] = None

        for i in range(1,DNAWidget.layout.count()):
            FirstLabelText = DNAWidget.layout.itemAt(i).itemAt(0).widget().text()
            if FirstLabelText == 'Set Section Size:':
                dnaData['sectionsize'] = DNAWidget.layout.itemAt(i).itemAt(1).widget().text()
            if FirstLabelText == 'Boundary Type:':
                dnaData['bstyle'] = DNAWidget.layout.itemAt(i).itemAt(1).widget().currentText()
                if dnaData['bstyle'] == 'Rectangular':
                    dnaData['bdryrct'] = {}
                    dnaData['bdryrct']['X Min'] = DNAWidget.layout.itemAt(i).itemAt(3).widget().text()
                    dnaData['bdryrct']['X Max'] = DNAWidget.layout.itemAt(i).itemAt(5).widget().text()
                    dnaData['bdryrct']['Y Min'] = DNAWidget.layout.itemAt(i).itemAt(7).widget().text()
                    dnaData['bdryrct']['Y Max'] = DNAWidget.layout.itemAt(i).itemAt(9).widget().text()
                    dnaData['bdryrct']['Z Min'] = DNAWidget.layout.itemAt(i).itemAt(11).widget().text()
                    dnaData['bdryrct']['Z Max'] = DNAWidget.layout.itemAt(i).itemAt(13).widget().text()
                elif dnaData['bstyle'] == 'Cylindrical':
                    dnaData['bdrycyl'] = {}
                    dnaData['bdrycyl']['R'] = DNAWidget.layout.itemAt(i).itemAt(3).widget().text()
                    dnaData['bdrycyl']['Z Min'] = DNAWidget.layout.itemAt(i).itemAt(5).widget().text()
                    dnaData['bdrycyl']['Z Max'] = DNAWidget.layout.itemAt(i).itemAt(7).widget().text()
            if FirstLabelText == 'Read Atom Types From File:':
                dnaData['typesfile'] = DNAWidget.layout.itemAt(i).itemAt(1).widget().text()

        return dnaData

    def getglobalsimArgs(self,InputHbox):
        globalData = {}
        globalData['Xmin'] = InputHbox.itemAt(3).widget().text()
        globalData['Xmax'] = InputHbox.itemAt(5).widget().text()
        globalData['Ymin'] = InputHbox.itemAt(7).widget().text()
        globalData['Ymax'] = InputHbox.itemAt(9).widget().text()
        globalData['Zmin'] = InputHbox.itemAt(11).widget().text()
        globalData['Zmax'] = InputHbox.itemAt(13).widget().text()

        return globalData

    def addTwistInput(self,ComboBoxText,twistItems):
        if 'Supercoil' in ComboBoxText:
            for item in twistItems:
                item.show()

        else:
            for item in twistItems:
                item.hide()

    def dual_checkbox(self,changed_box,other_box):
        other_box.setCheckState(changed_box.checkState())

    def basicDNA(self,currentIndex, args={}):
        """Make a set of input field for DNA. This can either be from defaults, or prefilled with existing parameters"""

        dna = QtGui.QWidget()
        # Layout for all DNA associated options
        dna.layout = QtGui.QVBoxLayout()

        qlinesize = Qt.QSize(QtGui.QLineEdit().sizeHint())
        qlinewidth = qlinesize.width()
        qlinesize.setWidth(qlinewidth/3)
        qlinesize_mid = Qt.QSize(QtGui.QLineEdit().sizeHint())
        qlinesize_mid.setWidth(qlinewidth/2)


        # Layout for just the basics
        dnaDefaultHBox = QtGui.QHBoxLayout()

        typeLabel = QtGui.QLabel('DNA')
        #dnaSelectedLabel = QtGui.QLabel('Selected:')
        dnaSelected = QtGui.QCheckBox('Selected')

        twistLabel = QtGui.QLabel('Linking No:')
        twistLabel.hide()
        twistnoField = QtGui.QLineEdit()
        twistnoField.setValidator(QtGui.QIntValidator())
        twistnoField.setText('20')
        twistnoField.setMinimumSize(qlinesize)
        twistnoField.hide()
        
        twistpassStart = QtGui.QCheckBox('Block Supercoiling\nAt First Atom:')
        twistpassStart.hide()
        if currentIndex != 1:
            twistpassStart.setChecked(False)
        twistpassEnd = QtGui.QCheckBox('Block Supercoiling\nAt Last Atom:')
        twistpassEnd.hide()
        twistpassStart.stateChanged.connect(lambda: self.dual_checkbox(twistpassStart,twistpassEnd))
        twistpassEnd.stateChanged.connect(lambda: self.dual_checkbox(twistpassEnd,twistpassStart))
        
        if args.has_key('twistno'):
            if args['twistno'] is not None:
                twistnoField.setText(args['twistno'])

        dnaTypeLabel = QtGui.QLabel('Dna Type:')
        dnaTypeCombo = QtGui.QComboBox()
        dnaTypeCombo.addItems(['Linear','Loop','Supercoil-Linear','Supercoil-Loop'])
        dnaTypeCombo.currentIndexChanged.connect(lambda: self.addTwistInput(dnaTypeCombo.currentText(),[twistLabel,twistnoField,twistpassStart,twistpassEnd]))

        if args.has_key('dnatypeIndex'):
            dnaTypeCombo.setCurrentIndex(args['dnatypeIndex'])

        atomnoLabel = QtGui.QLabel('Atom No.:')
        atomnoField = QtGui.QLineEdit()
        atomnoField.setValidator(QtGui.QIntValidator())
        atomnoField.setText('2000')
        #atomnoField.setMinimumSize(atomnoField.sizeHint())
        atomnoField.setMinimumSize(qlinesize_mid)
        if args.has_key('atomno'):
            atomnoField.setText(args['atomno'])

        atomtypeLabel = QtGui.QLabel('Atom Type:')
        atomtypeField = QtGui.QSpinBox()
        atomtypeField.setMinimum(1)
        atomtypeField.setValue(1)
        if args.has_key('atomtype'):
            atomtypeField.setValue(args['atomtype'])

        atomsizeLabel = QtGui.QLabel('Atom Size:')
        atomsizeField = QtGui.QLineEdit()
        atomsizeField.setValidator(QtGui.QDoubleValidator())
        atomsizeField.setText('1.0')
        #atomsizeField.setMinimumSize(atomsizeField.sizeHint())
        atomsizeField.setMinimumSize(qlinesize)
        if args.has_key('atomsize'):
            atomsizeField.setText(args['atomsize'])

        connectTo = QtGui.QCheckBox('Connect to next DNA section')
        if args.has_key('connectcheck'):
            connectTo.setChecked(args['connectcheck'])
        connectTo.stateChanged.connect(lambda: self.clickedConnector(connectTo.isChecked(),currentIndex))
        # Check if all dna types should be connected. Item At 15 due to widgets & spacing
        if self.inputWidget.layout.itemAt(0).itemAt(15).widget().isChecked():
            connectTo.setDisabled(True)
            connectTo.setChecked(True)

        startPointLabel = QtGui.QLabel('Start Point:')
        startPointXField = QtGui.QLineEdit()
        startPointXField.setValidator(QtGui.QDoubleValidator())
        startPointXField.setText('0.0')
        #startPointXField.setMinimumSize(startPointXField.sizeHint())
        startPointXField.setMinimumSize(qlinesize)
        if args.has_key('startX'):
            startPointXField.setText(args['startX'])
        startPointYField = QtGui.QLineEdit()
        startPointYField.setValidator(QtGui.QDoubleValidator())
        startPointYField.setText('0.0')
        #startPointYField.setMinimumSize(startPointYField.sizeHint())
        startPointYField.setMinimumSize(qlinesize)
        if args.has_key('startY'):
            startPointYField.setText(args['startY'])
        startPointZField = QtGui.QLineEdit()
        startPointZField.setValidator(QtGui.QDoubleValidator())
        startPointZField.setText('0.0')
        #startPointZField.setMinimumSize(startPointZField.sizeHint())
        startPointZField.setMinimumSize(qlinesize)

        if args.has_key('startZ'):
            startPointZField.setText(args['startZ'])

        # If the previous dna is connected to this one
        if currentIndex != 1:
            print self.inputWidget.layout.itemAt(currentIndex-1).widget().layout.itemAt(0).itemAt(0).itemAt(0).widget().text()
        if currentIndex != 1 and self.inputWidget.layout.itemAt(currentIndex-1).widget().layout.itemAt(0).itemAt(0).itemAt(0).widget().text() == 'DNA':
            checkBox = self.inputWidget.layout.itemAt(currentIndex-1).widget().layout.itemAt(0).itemAt(0).itemAt(10).widget()
            if checkBox.isChecked():
                startPointLabel.hide()
                startPointXField.hide()
                startPointYField.hide()
                startPointZField.hide()

        sizeHbox = QtGui.QHBoxLayout()
        sectionSizeLabel = QtGui.QLabel('Set Section Size:')
        sectionSizeLabel.hide()
        sectionSizeField = QtGui.QLineEdit()
        sectionSizeField.setValidator(QtGui.QIntValidator())
        sectionSizeField.setText('5')
        sectionSizeField.setMinimumSize(qlinesize)
        #sectionSizeField.sizehint = qlinesize
        sectionSizeField.hide()
        if args.has_key('sectionsize'):
            if args['sectionsize'] is not None:
                sectionSizeField.setText(args['sectionsize'])
        sizeHbox.addWidget(sectionSizeLabel)
        sizeHbox.addWidget(sectionSizeField)
        sizeHbox.addStretch(1)

        typesHbox = QtGui.QHBoxLayout()
        typesFromFileLabel = QtGui.QLabel('Read Atom Types From File:')
        typesFromFileLabel.hide()
        typesFromFileField = QtGui.QLineEdit()
        typesFromFileField.setText('Atom Types File')
        typesFromFileField.setMinimumSize(qlinesize)
        typesFromFileField.hide()
        typesFromFileBtn = QtGui.QPushButton('Set Atom Types File')
        typesFromFileBtn.clicked.connect(lambda: self.write_to(typesFromFileField, True, 'Atom Types File'))
        typesFromFileBtn.hide()
        if args.has_key('typesfile'):
            if args['typesfile'] is not None:
                typesFromFileField.setText(args['typesfile'])

        typesHbox.addWidget(typesFromFileLabel)
        typesHbox.addWidget(typesFromFileField)
        typesHbox.addWidget(typesFromFileBtn)
        typesHbox.addStretch(1)

        boundaryHbox = QtGui.QHBoxLayout()
        boundaryLabel = QtGui.QLabel('Boundary Type:')
        boundaryLabel.hide()
        boundaryCombo = QtGui.QComboBox()
        boundaryCombo.addItems(['Rectangular','Cylindrical'])
        boundaryCombo.currentIndexChanged.connect(lambda :self.swap(boundaryHbox))
        boundaryCombo.hide()
        boundaryHbox.addWidget(boundaryLabel)
        boundaryHbox.addWidget(boundaryCombo)
        if args.has_key('bstyle'):
            if args['bstyle'] == 'Rectangular':
                for label in ['X Min', 'X Max', 'Y Min', 'Y Max', 'Z Min', 'Z Max']:
                    linelabel = QtGui.QLabel(label + ':')
                    linelabel.hide()
                    boundaryHbox.addWidget(linelabel)
                    lineEd = QtGui.QLineEdit()
                    lineEd.setValidator(QtGui.QDoubleValidator())
                    lineEd.setText(args['bdryrct'][label])
                    lineEd.setMinimumSize(qlinesize)
                    lineEd.hide()
                    boundaryHbox.addWidget(lineEd)
            elif args['bstyle'] == 'Cylindrical':
                for label in ['R','Z Min', 'Z Max']:
                    linelabel = QtGui.QLabel(label + ':')
                    linelabel.hide()
                    boundaryHbox.addWidget(linelabel)
                    lineEd = QtGui.QLineEdit()
                    lineEd.setValidator(QtGui.QDoubleValidator())
                    lineEd.setText(args['bdrycyl'][label])
                    lineEd.setMinimumSize(qlinesize)
                    lineEd.hide()
                    boundaryHbox.addWidget(lineEd)
        else:
            for label in ['X Min', 'X Max', 'Y Min', 'Y Max', 'Z Min', 'Z Max']:
                linelabel = QtGui.QLabel(label + ':')
                linelabel.hide()
                boundaryHbox.addWidget(linelabel)
                lineEd = QtGui.QLineEdit()
                lineEd.setValidator(QtGui.QDoubleValidator())
                lineEd.setText('0.0')
                lineEd.setMinimumSize(qlinesize)
                lineEd.hide()
                boundaryHbox.addWidget(lineEd)

        #lst = [[sectionSizeLabel,sectionSizeField],[typesFromFileLabel,typesFromFileField]]

        hboxlst = {'Set Section Size': sizeHbox, 'Set Atom Types From File': typesHbox,
                   'Set DNA Section Specific Boundary': boundaryHbox}

        extraFieldsLabel = QtGui.QLabel('Set Extra Options:')
        extraFields = QtGui.QComboBox()
        extraFields.addItems(['Set Section Size', 'Set Atom Types From File', 'Set DNA Section Specific Boundary'])
        extraFields.setCurrentIndex(-1)
        # extraFields.currentIndexChanged.connect(lambda:self.addExtraField(dna.layout,self.hboxlst[str(extraFields.currentText())],extraFields,extraFieldsLabel))
        extraFields.currentIndexChanged.connect(
            lambda: self.showExtraField(hboxlst[str(extraFields.currentText())], extraFields, extraFieldsLabel))

        # Core Options
        dnaDefaultHBox.addWidget(typeLabel)
        #dnaDefaultHBox.addWidget(dnaSelectedLabel)
        dnaDefaultHBox.addWidget(dnaSelected)
        dnaDefaultHBox.addWidget(dnaTypeLabel)
        dnaDefaultHBox.addWidget(dnaTypeCombo)
        dnaDefaultHBox.addWidget(atomnoLabel)
        dnaDefaultHBox.addWidget(atomnoField)
        dnaDefaultHBox.addWidget(atomtypeLabel)
        dnaDefaultHBox.addWidget(atomtypeField)
        dnaDefaultHBox.addWidget(atomsizeLabel)
        dnaDefaultHBox.addWidget(atomsizeField)
        dnaDefaultHBox.addWidget(connectTo)
        dnaDefaultHBox.addWidget(startPointLabel)
        dnaDefaultHBox.addWidget(startPointXField)
        dnaDefaultHBox.addWidget(startPointYField)
        dnaDefaultHBox.addWidget(startPointZField)
        dnaDefaultHBox.addWidget(twistLabel)
        dnaDefaultHBox.addWidget(twistnoField)
        dnaDefaultHBox.addWidget(twistpassStart)
        dnaDefaultHBox.addWidget(twistpassEnd)
        dnaDefaultHBox.addSpacing(1)
        # Extra fields Combobox
        dnaExtraFieldHBox = QtGui.QHBoxLayout()
        dnaExtraFieldHBox.addWidget(extraFieldsLabel)
        dnaExtraFieldHBox.addWidget(extraFields)



        if args.has_key('actfield'):
            print args['actfield']
            for activatedField in args['actfield']:
                print activatedField
                self.showExtraField(hboxlst[activatedField],extraFields,extraFieldsLabel)

        dnaHbox1 = QtGui.QHBoxLayout()

        dnaHbox1.addLayout(dnaDefaultHBox)
        dnaHbox1.addLayout(dnaExtraFieldHBox)
        dna.layout.addLayout(dnaHbox1)
        dna.layout.addLayout(sizeHbox)
        dna.layout.addLayout(typesHbox)
        dna.layout.addLayout(boundaryHbox)

        dna.setLayout(dna.layout)

        return dna

    def basicProt(self,current_index,args={}):
        """Add protein input widget"""


        prot = QtGui.QWidget()
        prot.layout = QtGui.QVBoxLayout()

        protHbox = QtGui.QHBoxLayout()

        protLabel = QtGui.QLabel('Proteins')

        protSelected = QtGui.QCheckBox('Selected')

        atomnoLabel = QtGui.QLabel('Atom No.:')
        atomnoField = QtGui.QLineEdit()
        atomnoField.setValidator(QtGui.QIntValidator())
        atomnoField.setText('2000')
        if args.has_key('atomno'):
            atomnoField.setText(str(args['atomno']))

        atomtypeLabel = QtGui.QLabel('Atom Type:')
        atomtypeField = QtGui.QSpinBox()
        atomtypeField.setMinimum(1)
        atomtypeField.setValue(1)
        if args.has_key('atomtype'):
            atomtypeField.setValue(int(args['atomtype']))

        atomsizeLabel = QtGui.QLabel('Atom Size:')
        atomsizeField = QtGui.QLineEdit()
        atomsizeField.setValidator(QtGui.QDoubleValidator())
        atomsizeField.setText('1.0')
        if args.has_key('atomsize'):
            atomsizeField.setText(args['atomsize'])

        protHbox.addWidget(protLabel)
        protHbox.addWidget(protSelected)
        protHbox.addWidget(atomnoLabel)
        protHbox.addWidget(atomnoField)
        protHbox.addWidget(atomtypeLabel)
        protHbox.addWidget(atomtypeField)
        protHbox.addWidget(atomsizeLabel)
        protHbox.addWidget(atomsizeField)

        # Set atom types from file

        typesHbox = QtGui.QHBoxLayout()
        typesFromFileLabel = QtGui.QLabel('Read Atom Types From File:')
        typesFromFileLabel.hide()
        typesFromFileField = QtGui.QLineEdit()
        typesFromFileField.setText('Atom Types File')
        typesFromFileField.hide()
        if args.has_key('typesfile'):
            if args['typesfile'] is not None:
                typesFromFileField.setText(args['typesfile'])
        typesFromFileBtn = QtGui.QPushButton('Set Atom Types File')
        typesFromFileBtn.clicked.connect(lambda: self.write_to(typesFromFileField, True, 'Atom Types File'))
        typesFromFileBtn.hide()

        typesHbox.addWidget(typesFromFileLabel)
        typesHbox.addWidget(typesFromFileField)
        typesHbox.addWidget(typesFromFileBtn)

        # Place atoms within boundary

        boundaryHbox = QtGui.QHBoxLayout()
        boundaryLabel = QtGui.QLabel('Boundary Type:')
        boundaryLabel.hide()
        boundaryCombo = QtGui.QComboBox()
        boundaryCombo.addItems(['Rectangular', 'Cylindrical'])
        boundaryCombo.currentIndexChanged.connect(lambda: self.swap(boundaryHbox))
        boundaryCombo.hide()
        boundaryHbox.addWidget(boundaryLabel)
        boundaryHbox.addWidget(boundaryCombo)
        if args == {}:
            for label in ['X Min', 'X Max', 'Y Min', 'Y Max', 'Z Min', 'Z Max']:
                linelabel = QtGui.QLabel(label + ':')
                linelabel.hide()
                boundaryHbox.addWidget(linelabel)
                lineEd = QtGui.QLineEdit()
                lineEd.setValidator(QtGui.QDoubleValidator())
                lineEd.setText('0.0')
                lineEd.hide()
                boundaryHbox.addWidget(lineEd)
        elif args.has_key('bstyle'):
            if args['bstyle'] == 'Rectangular':
                for label in ['X Min', 'X Max', 'Y Min', 'Y Max', 'Z Min', 'Z Max']:
                    linelabel = QtGui.QLabel(label + ':')
                    linelabel.hide()
                    boundaryHbox.addWidget(linelabel)
                    lineEd = QtGui.QLineEdit()
                    lineEd.setValidator(QtGui.QDoubleValidator())
                    lineEd.setText(args['bdryrct'][label])
                    lineEd.hide()
                    boundaryHbox.addWidget(lineEd)
            elif args['bstyle'] == 'Cylindrical':
                for label in ['R', 'Z Min', 'Z Max']:
                    linelabel = QtGui.QLabel(label + ':')
                    linelabel.hide()
                    boundaryHbox.addWidget(linelabel)
                    lineEd = QtGui.QLineEdit()
                    lineEd.setValidator(QtGui.QDoubleValidator())
                    lineEd.setText(args['bdrycyl'][label])
                    lineEd.hide()
                    boundaryHbox.addWidget(lineEd)

        #protExtraHbox = QtGui.QHBoxLayout()

        #hboxlst = {'Set Atom Types From File': typesHbox,
        #           'Set DNA Section Specific Boundary': boundaryHbox}

        extraFieldsLabel = QtGui.QLabel('Set Extra Options:')
        extraFields = QtGui.QComboBox()
        extraFields.addItems(['Set Atom Types From File', 'Set DNA Section Specific Boundary'])
        extraFields.setCurrentIndex(-1)
        extraFields.currentIndexChanged.connect(
            lambda: self.showExtraField(hboxlst[str(extraFields.currentText())], extraFields, extraFieldsLabel))

        #protExtraHbox.addWidget(extraFieldsLabel)
        #protExtraHbox.addWidget(extraFields)

        protHboxFull = QtGui.QHBoxLayout()
        protHboxFull.addLayout(protHbox)
        #protHboxFull.addLayout(protExtraHbox)
        prot.layout.addLayout(protHboxFull)

        prot.layout.addLayout(boundaryHbox)

        prot.layout.addLayout(typesHbox)

        prot.setLayout(prot.layout)

        # Switch off the 'connect to next section item if the previous widget is DNA
        print 'Basic prot - current index'
        print current_index

        try:
            line_type = self.inputWidget.layout.itemAt(current_index - 1).widget().layout.itemAt(0).itemAt(
                0).itemAt(0).widget().text()
        except:
            line_type = None


        if current_index != 1 and line_type == 'DNA':
            self.inputWidget.layout.itemAt(current_index - 1).widget().layout.itemAt(0).itemAt(
                0).itemAt(10).widget().hide()
        return prot

    def clickedConnector(self,active,currentIndex):
        # Does a next object exist?
        if self.inputWidget.layout.count() > currentIndex + 1:
            nextHbox = self.inputWidget.layout.itemAt(currentIndex + 1).widget().layout.itemAt(0)
            # Is it DNA?
            #self.inputWidget.layout.itemAt(currentIndex - 1).widget().layout.itemAt(0).itemAt(11).widget()
            if nextHbox.itemAt(0).itemAt(0).widget().text() == 'DNA' and active:
                nextHbox.itemAt(0).itemAt(11).widget().hide()
                nextHbox.itemAt(0).itemAt(12).widget().hide()
                nextHbox.itemAt(0).itemAt(13).widget().hide()
                nextHbox.itemAt(0).itemAt(14).widget().hide()
            elif nextHbox.itemAt(0).itemAt(0).widget().text() == 'DNA' and not active:
                nextHbox.itemAt(0).itemAt(11).widget().show()
                nextHbox.itemAt(0).itemAt(12).widget().show()
                nextHbox.itemAt(0).itemAt(13).widget().show()
                nextHbox.itemAt(0).itemAt(14).widget().show()

    def swap(self,hbox):

        currIndex = hbox.itemAt(1).widget().currentIndex()
        if currIndex == 1:
            for index in range(2,14):
                remove_widget = hbox.itemAt(2).widget()
                hbox.removeWidget(remove_widget)
                print remove_widget
                remove_widget.deleteLater()
            for label in ['R','Z Min','Zmax']:
                hbox.addWidget(QtGui.QLabel(label+':'))
                lineEd = QtGui.QLineEdit()
                lineEd.setValidator(QtGui.QDoubleValidator())
                lineEd.setText('0.0')
                hbox.addWidget(lineEd)
        elif currIndex == 0:
            for index in range(2,8):
                remove_widget = hbox.itemAt(2).widget()
                hbox.removeWidget(remove_widget)
                print remove_widget
                remove_widget.deleteLater()
            for label in ['X Min','X Max','Y Min','Y max','Z Min','Z max']:
                hbox.addWidget(QtGui.QLabel(label+':'))
                lineEd = QtGui.QLineEdit()
                lineEd.setValidator(QtGui.QDoubleValidator())
                lineEd.setText('0.0')
                hbox.addWidget(lineEd)

    def showExtraField(self,extraLayout,combo,label):

        for counter in range(extraLayout.count()):
            print type(extraLayout.itemAt(counter)),extraLayout.itemAt(counter).widget()
            if extraLayout.itemAt(counter).widget() is not None:
                extraLayout.itemAt(counter).widget().show()

        combo.blockSignals(True)
        combo.removeItem(combo.currentIndex())
        combo.setCurrentIndex(-1)
        combo.blockSignals(False)
        if combo.count() == 0:
            combo.hide()
            label.hide()

    def loadEquil(self):

        self.selectedsim = 'equil.py'
        self.active = 'eq'
        equilWidget = self.sharedParams('equil')


        show = [self.runbtn,self.runvmdbtn]
        hide = [self.createInputbtn,self.addDNAbtn,self.addProtbtn,self.addDNAfromfilebtn]
        map(lambda widget: widget.show(),show)
        map(lambda widget: widget.hide(), hide)
        
        if utility.find_command('vmd') is False:
            self.runvmdbtn.setDisabled(True)
        else:
            self.runvmdbtn.setEnabled(True)
        self.runbtn.setEnabled(True)

        # Add options for graphs specific to equil

        GraphsHbox = QtGui.QHBoxLayout()

        LabelHbox = QtGui.QHBoxLayout()
        graphLabel = QtGui.QLabel('Graphs:')

        LabelHbox.addWidget(graphLabel)

        EnergyHbox = QtGui.QHBoxLayout()
        energyLabel = QtGui.QLabel('Energy')
        energyCombo = QtGui.QComboBox()
        energyCombo.addItems(['No Graph','Show','Save'])
        energyCombo.currentIndexChanged.connect(lambda:self.addGraphName(EnergyHbox,energyCombo.currentIndex(),[2]))

        EnergyHbox.addWidget(energyLabel)
        EnergyHbox.addWidget(energyCombo)

        RgHbox = QtGui.QHBoxLayout()
        rgLabel = QtGui.QLabel('Rad. of Gyration & Temp.')
        rgCombo = QtGui.QComboBox()
        rgCombo.addItems(['No Graph', 'Show (Rad. Gyration Only)', 'Show (Temp. Only)', 'Show (Both)',
                          'Save (Rad. Gyration Only)', 'Save (Temp. Only)', 'Save (Both)'])
        rgCombo.currentIndexChanged.connect(lambda: self.addGraphName(RgHbox, rgCombo.currentIndex(), [4,5,6]))

        RgHbox.addWidget(rgLabel)
        RgHbox.addWidget(rgCombo)

        GraphsHbox.addLayout(LabelHbox)
        GraphsHbox.addStretch(1)
        GraphsHbox.addLayout(EnergyHbox)
        GraphsHbox.addStretch(1)
        GraphsHbox.addLayout(RgHbox)


        # Update our layout with new widgets

        equilWidget.layout.addLayout(GraphsHbox,6,0,1,6)

        # Remove whatever was there before and replace

        currentSimParamWidget = self.mainwidget.layout.itemAtPosition(0,0).widget()
        self.mainwidget.layout.removeWidget(currentSimParamWidget)
        currentSimParamWidget.deleteLater()
        self.mainwidget.layout.addWidget(equilWidget,0,0,1,5)

    def loadProt(self):

        # Create a object with all assignable parameters for the simulation

        self.selectedsim = 'protein.py'
        self.active = 'prot'

        protWidget = self.sharedParams('protein')

        

        show = [self.runbtn, self.runvmdbtn]
        hide = [self.createInputbtn, self.addDNAbtn, self.addProtbtn,self.addDNAfromfilebtn]
        map(lambda widget: widget.show(), show)
        map(lambda widget: widget.hide(), hide)
        
        if utility.find_command('vmd') is False:
            self.runvmdbtn.setDisabled(True)
        else:
            self.runvmdbtn.setEnabled(True)
        self.runbtn.setEnabled(True)

        GraphsHbox = QtGui.QHBoxLayout()

        LabelHbox = QtGui.QHBoxLayout()
        graphLabel = QtGui.QLabel('Graphs:')

        LabelHbox.addWidget(graphLabel)

        EnergyHbox = QtGui.QHBoxLayout()
        energyLabel = QtGui.QLabel('Energy')
        energyCombo = QtGui.QComboBox()
        energyCombo.addItems(['No Graph', 'Show', 'Save'])
        energyCombo.currentIndexChanged.connect(lambda: self.addGraphName(EnergyHbox, energyCombo.currentIndex(),[2]))

        EnergyHbox.addWidget(energyLabel)
        EnergyHbox.addWidget(energyCombo)

        RgHbox = QtGui.QHBoxLayout()
        rgLabel = QtGui.QLabel('Radius of\n Gyration')
        rgCombo = QtGui.QComboBox()
        rgCombo.addItems(['No Graph', 'Show', 'Save'])
        rgCombo.currentIndexChanged.connect(lambda: self.addGraphName(RgHbox, rgCombo.currentIndex(),[2]))

        RgHbox.addWidget(rgLabel)
        RgHbox.addWidget(rgCombo)

        ClusHbox = QtGui.QHBoxLayout()
        clusLabel = QtGui.QLabel('Number of Clusters\n& Prop. Proteins in a Cluster')
        clusCombo = QtGui.QComboBox()
        clusCombo.addItems(['No Graph', 'Show (Clusters Only)', 'Show (Proteins Only)', 'Show (Both)',
                          'Save (Clusters Only)', 'Save (Proteins Only)', 'Save (Both)'])
        clusCombo.currentIndexChanged.connect(lambda: self.addGraphName(ClusHbox, clusCombo.currentIndex(),[4,5,6]))

        ClusHbox.addWidget(clusLabel)
        ClusHbox.addWidget(clusCombo)

        GraphsHbox.addLayout(LabelHbox)
        GraphsHbox.addStretch(1)
        GraphsHbox.addLayout(EnergyHbox)
        GraphsHbox.addStretch(1)
        GraphsHbox.addLayout(RgHbox)
        GraphsHbox.addStretch(1)
        GraphsHbox.addLayout(ClusHbox)

        protWidget.layout.addLayout(GraphsHbox,6,0,1,6)

        # Add input for equilibration time

        #eqHbox = QtGui.QHBoxLayout()
        eqLabel  = QtGui.QLabel('Equilibration Time:')
        eqField = QtGui.QLineEdit()
        eqField.setValidator(QtGui.QIntValidator())
        eqField.setAlignment(QtCore.Qt.AlignLeft)
        eqField.setText('1000000')
        eqField.setMaximumSize(eqField.sizeHint())
        #eqHbox.addWidget(eqLabel)
        #eqHbox.addWidget(eqField)
        #eqHbox.addStretch(1)


        protWidget.layout.addWidget(eqLabel,7,0)
        protWidget.layout.addWidget(eqField, 7, 1)

        # Add inputs for protein energies & cutoff

        #intHbox = QtGui.QHBoxLayout()

        intenLabel = QtGui.QLabel('Protein-DNA\nInteraction Energy')
        intenField = QtGui.QLineEdit()
        intenField.setValidator(QtGui.QDoubleValidator())
        intenField.setAlignment(QtCore.Qt.AlignLeft)
        intenField.setText('3.0')
        intenField.setMaximumSize(intenField.sizeHint())
        #intHbox.addWidget(intenLabel)
        #intHbox.addWidget(intenField)

        intcutLabel = QtGui.QLabel('Protein-DNA\nCutoff Range')
        intcutField = QtGui.QLineEdit()
        intcutField.setValidator(QtGui.QDoubleValidator())
        intcutField.setAlignment(QtCore.Qt.AlignLeft)
        intcutField.setText('2.2')
        intcutField.setMaximumSize(intcutField.sizeHint())

        #intHbox.addWidget(intcutLabel)
        #intHbox.addWidget(intcutField)
        #intHbox.addStretch(1)

        protWidget.layout.addWidget(intenLabel, 8, 0)
        protWidget.layout.addWidget(intenField, 8, 1)
        protWidget.layout.addWidget(intcutLabel, 8, 3)
        protWidget.layout.addWidget(intcutField, 8, 4)

        # Get the object with at position 0,0 in our grid layout and delete it

        currentSimParamWidget = self.mainwidget.layout.itemAtPosition(0,0).widget()
        self.mainwidget.layout.removeWidget(currentSimParamWidget)
        currentSimParamWidget.deleteLater()

        # Replace it with a new object at 0,0 which has the correct assignable parameters

        self.mainwidget.layout.addWidget(protWidget,0,0,1,5)

    def loadGenome(self,args = {}):

        # Create a object with all assignable parameters for the simulation

        self.selectedsim = 'genomeprot.py'
        self.active = 'genome'

        protWidget = self.sharedParams('genomics')
        self.mainwidget.layout.addWidget(self.addDNAfromfilebtn, 2, 1)

        show = [self.runbtn, self.runvmdbtn,self.addDNAfromfilebtn]
        hide = [self.createInputbtn, self.addDNAbtn, self.addProtbtn]
        map(lambda widget: widget.show(), show)
        map(lambda widget: widget.hide(), hide)

        if utility.find_command('vmd') is False:
            self.runvmdbtn.setDisabled(True)
        else:
            self.runvmdbtn.setEnabled(True)
        self.runbtn.setEnabled(True)

        GraphsHbox = QtGui.QHBoxLayout()

        LabelHbox = QtGui.QHBoxLayout()
        graphLabel = QtGui.QLabel('Graphs:')

        LabelHbox.addWidget(graphLabel)

        EnergyHbox = QtGui.QHBoxLayout()
        energyLabel = QtGui.QLabel('Energy')
        energyCombo = QtGui.QComboBox()
        energyCombo.addItems(['No Graph', 'Show', 'Save'])
        energyCombo.currentIndexChanged.connect(lambda: self.addGraphName(EnergyHbox, energyCombo.currentIndex(), [2]))

        EnergyHbox.addWidget(energyLabel)
        EnergyHbox.addWidget(energyCombo)

        RgHbox = QtGui.QHBoxLayout()
        rgLabel = QtGui.QLabel('Radius of\n Gyration')
        rgCombo = QtGui.QComboBox()
        rgCombo.addItems(['No Graph', 'Show', 'Save'])
        rgCombo.currentIndexChanged.connect(lambda: self.addGraphName(RgHbox, rgCombo.currentIndex(), [2]))

        RgHbox.addWidget(rgLabel)
        RgHbox.addWidget(rgCombo)

        ClusHbox = QtGui.QHBoxLayout()
        clusLabel = QtGui.QLabel('Number of Clusters\n& Prop. Proteins in a Cluster')
        clusCombo = QtGui.QComboBox()
        clusCombo.addItems(['No Graph', 'Show (Clusters Only)', 'Show (Proteins Only)', 'Show (Both)',
                            'Save (Clusters Only)', 'Save (Proteins Only)', 'Save (Both)'])
        clusCombo.currentIndexChanged.connect(lambda: self.addGraphName(ClusHbox, clusCombo.currentIndex(), [4, 5, 6]))

        ClusHbox.addWidget(clusLabel)
        ClusHbox.addWidget(clusCombo)

        GraphsHbox.addLayout(LabelHbox)
        GraphsHbox.addStretch(1)
        GraphsHbox.addLayout(EnergyHbox)
        GraphsHbox.addStretch(1)
        GraphsHbox.addLayout(RgHbox)
        GraphsHbox.addStretch(1)
        GraphsHbox.addLayout(ClusHbox)

        protWidget.layout.addLayout(GraphsHbox, 6, 0, 1, 6)

        # Set angle coeff default to 4 instead of 20

        protWidget.layout.itemAtPosition(4,1).widget().setText('3.0')

        # Add input for equilibration time

        # eqHbox = QtGui.QHBoxLayout()
        eqLabel = QtGui.QLabel('Equilibration Time:')
        eqField = QtGui.QLineEdit()
        eqField.setValidator(QtGui.QIntValidator())
        eqField.setAlignment(QtCore.Qt.AlignLeft)
        eqField.setText('1000000')
        eqField.setMaximumSize(eqField.sizeHint())
        # eqHbox.addWidget(eqLabel)
        # eqHbox.addWidget(eqField)
        # eqHbox.addStretch(1)


        protWidget.layout.addWidget(eqLabel, 7, 0)
        protWidget.layout.addWidget(eqField, 7, 1)

        # Number of protein types

        protLabel = QtGui.QLabel("Number of Protein Types:")
        protSpinbox = QtGui.QSpinBox()
        protSpinbox.setMinimum(1)
        try:
            if args.has_key('prot_types'):
                protSpinbox.setValue(args['prot_types'])
                protno = args['prot_types']
            else:
                protSpinbox.setValue(1)
                self.prottypes = 1
        except:
            protSpinbox.setValue(1)
            self.prottypes = 1
        protSpinbox.valueChanged.connect(lambda: self.proteinchange(protWidget,protSpinbox.value(),self.prottypes))

        protInteractionBtn = QtGui.QPushButton("Add Extra Protein Interaction")
        protInteractionBtn.clicked.connect(lambda: self.add_protein(protWidget,protSpinbox.value(),True))

        protWidget.layout.addWidget(QtGui.QLabel("--- Protein Interactions ---"),8,0,1,2)

        protWidget.layout.addWidget(protLabel, 9, 0)
        protWidget.layout.addWidget(protSpinbox, 9, 1)
        protWidget.layout.addWidget(protInteractionBtn,9,2)

        # Add inputs for protein energies & cutoff
        
        energyGrid = QtGui.QGridLayout()

        # intHbox = QtGui.QHBoxLayout()
        for i in range(self.prottypes):
            statehbox = QtGui.QHBoxLayout()
            rows = protWidget.layout.rowCount()
            label = QtGui.QLabel("Protein Type ")

            """protnoSpinbox = QtGui.QSpinBox()
            protnoSpinbox.setValue(i+1)
            protnoSpinbox.setMinimum(1)"""
            protno = QtGui.QLabel("{}".format(i+1))
            label2 = QtGui.QLabel(" binds to DNA types: ")
            #statedrop = QtGui.QComboBox()
            #statedrop.addItems(["None","Heterochromatin"])
            statedrop= QtGui.QLabel("None")
            statehbox.addWidget(statedrop)
            label3 = QtGui.QLabel("with binding energy:")
            strengthTextfield = QtGui.QLineEdit()
            strengthTextfield.setValidator(QtGui.QDoubleValidator())
            strengthTextfield.setText("4.0")
            try:
                if args.has_key('prot_energy{}'.format(i+1)):
                    strengthTextfield.setText(args['prot_energy{}'.format(i+1)])
            except:
                strengthTextfield.setText("4.0")
            strengthTextfield.setMinimumSize(strengthTextfield.sizeHint())
            addstateBtn = QtGui.QPushButton('Add Custom Type')
            addstateBtn.clicked.connect(lambda: self.add_state(statehbox))
            
            numberLabel = QtGui.QLabel("No. of Proteins:")
            numberTextfield = QtGui.QLineEdit()
            numberTextfield.setValidator(QtGui.QDoubleValidator())
            numberTextfield.setText("1000")

            remove_prot = QtGui.QPushButton("X")
            remove_prot.setMaximumWidth(25)
            remove_prot.clicked.connect(lambda: self.remove_protein(protWidget, rows))
            remove_prot.hide()
            
            energyGrid.addWidget(label,i,0)
            energyGrid.addWidget(protno,i,1)
            energyGrid.addWidget(label2,i,2)
            energyGrid.addLayout(statehbox,i,3)
            energyGrid.addWidget(label3,i,4)
            energyGrid.addWidget(strengthTextfield,i,5)
            energyGrid.addWidget(addstateBtn,i,6)
            energyGrid.addWidget(remove_prot, i, 7)

        # Get the object with at position 0,0 in our grid layout and delete it
        
        prot_no_layout = QtGui.QGridLayout()
        
        for i in range(self.prottypes):
            label = QtGui.QLabel("Number of protein {} atoms:".format(i+1))
            protnoField = QtGui.QLineEdit()
            protnoField.setMaximumSize(protnoField.sizeHint())
            protnoField.setText("1000")
            protnoField.setValidator(QtGui.QIntValidator())
            
            prot_no_layout.addWidget(label,i,0)
            prot_no_layout.addWidget(protnoField,i,4)
            
        protWidget.layout.addLayout(energyGrid,protWidget.layout.rowCount(),0,1,7)
        #protWidget.layout.addLayout(prot_no_layout,protWidget.layout.rowCount(),0,1,7)

        currentRow = protWidget.layout.rowCount()
        HtrLabel = QtGui.QLabel("States marked as heterochromatin:")
        addstateBtn = QtGui.QPushButton('Add New State')
        addstateBtn.clicked.connect(lambda: self.add_state(statehbox2))
        statehbox2 = QtGui.QHBoxLayout()
        statelabel2 = QtGui.QLabel("None")
        statehbox2.addWidget(statelabel2)
        protWidget.layout.addWidget(HtrLabel,currentRow,0)
        protWidget.layout.addLayout(statehbox2, currentRow, 1,1,3)
        protWidget.layout.addWidget(addstateBtn, currentRow, 4)

        currentSimParamWidget = self.mainwidget.layout.itemAtPosition(0, 0).widget()
        self.mainwidget.layout.removeWidget(currentSimParamWidget)
        currentSimParamWidget.deleteLater()

        # Replace it with a new object at 0,0 which has the correct assignable parameters

        self.mainwidget.layout.addWidget(protWidget, 0, 0, 1, 5)

    def proteinchange(self,protWidget,spinbox_value,protno):
        energyGrid = protWidget.layout.itemAtPosition(10,0)
        #numberGrid = protWidget.layout.itemAtPosition(11,0)
        if spinbox_value > protno:
            for row in range(0,energyGrid.rowCount()):
                try:
                    prot_type = int(energyGrid.itemAtPosition(row,1).widget().value())
                except:
                    try:
                        prot_type = int(energyGrid.itemAtPosition(row,1).widget().text())
                    except:
                        prot_type = 0

                if(prot_type > spinbox_value):
                    self.remove_protein(protWidget,row)
            self.add_protein(protWidget,spinbox_value)
            label = QtGui.QLabel("Number of protein {} atoms:".format(spinbox_value))
            protnoField = QtGui.QLineEdit()
            protnoField.setMaximumSize(protnoField.sizeHint())
            protnoField.setText("1000")
            protnoField.setValidator(QtGui.QIntValidator())
            #numberGrid.addWidget(label,spinbox_value - 1,0)
            #numberGrid.addWidget(protnoField,spinbox_value - 1,4)

        if spinbox_value < protno:
            for row in range(0,energyGrid.rowCount()):
                try:
                    prot_type = int(energyGrid.itemAtPosition(row,1).widget().value())
                except:
                    try:
                        prot_type = int(energyGrid.itemAtPosition(row,1).widget().text())
                    except:
                        prot_type = 0

                if(prot_type > spinbox_value):
                    self.remove_protein(protWidget,row)
            #remove_label = numberGrid.itemAtPosition(spinbox_value,0).widget()
            #numberGrid.removeWidget(remove_label)
            #remove_label.deleteLater()
            #remove_field = numberGrid.itemAtPosition(spinbox_value,4).widget()
            #numberGrid.removeWidget(remove_field)
            #remove_field.deleteLater()
        self.prottypes = spinbox_value
            
    def add_state(self,hbox):
        if hbox.count() == 1:
            to_remove = hbox.itemAt(0).widget()
            hbox.removeWidget(to_remove)
            to_remove.deleteLater()

        new_line = QtGui.QLineEdit()
        remove_line = QtGui.QPushButton("X")
        new_line.setMinimumSize(new_line.sizeHint())
        remove_line.setMaximumWidth(25)
        remove_line.clicked.connect(lambda: self.remove_state(hbox,new_line,remove_line))
        hbox.addWidget(new_line)
        hbox.addWidget(remove_line)

    def remove_state(self,hbox,line,button):
        hbox.removeWidget(line)
        line.deleteLater()
        hbox.removeWidget(button)
        button.deleteLater()
        if hbox.isEmpty() == True:
            statedrop = QtGui.QComboBox()
            statedrop.addItems(["None", "Heterochromatin"])
            hbox.addWidget(statedrop)

    def add_protein(self,protWidget,new_prot_no,spinbox = False):
        energyGrid = protWidget.layout.itemAtPosition(10,0)
        statehbox = QtGui.QHBoxLayout()
        rows = energyGrid.rowCount()
        label = QtGui.QLabel("Protein Type ")
        remove_prot = QtGui.QPushButton("X")
        remove_prot.setMaximumWidth(25)
        remove_prot.clicked.connect(lambda: self.remove_protein(protWidget, rows))
        if spinbox == True:
            protno = QtGui.QSpinBox()
            protno.setValue(new_prot_no)
            protno.setMinimum(1)
        else:
            protno = QtGui.QLabel("{}".format(new_prot_no))
            remove_prot.hide()
        label2 = QtGui.QLabel("binds to DNA types: ")
        #statedrop = QtGui.QComboBox()
        #statedrop.addItems(["None", "Heterochromatin"])
        statedrop = QtGui.QLabel("None")
        statehbox.addWidget(statedrop)
        label3 = QtGui.QLabel("with binding energy:")
        strengthTextfield = QtGui.QLineEdit()
        strengthTextfield.setValidator(QtGui.QDoubleValidator())
        strengthTextfield.setText("4.0")
        try:
            if args['prot_energy{}'.format(new_prot_no)] is not None:
                strengthTextfield.setText(args['prot_energy{}'.format(new_prot_no)])
        except:
            strengthTextfield.setText("4.0")
        strengthTextfield.setMinimumSize(strengthTextfield.sizeHint())
        addstateBtn = QtGui.QPushButton('Add Custom Type')
        addstateBtn.clicked.connect(lambda: self.add_state(statehbox))

        
        energyGrid.addWidget(label,rows,0)
        energyGrid.addWidget(protno,rows,1)
        energyGrid.addWidget(label2,rows,2)
        energyGrid.addLayout(statehbox,rows,3)
        energyGrid.addWidget(label3,rows,4)
        energyGrid.addWidget(strengthTextfield,rows,5)
        energyGrid.addWidget(addstateBtn,rows,6)
        energyGrid.addWidget(remove_prot, rows, 7)

    def remove_protein(self,protWidget,row):
        energyGrid = protWidget.layout.itemAtPosition(10,0)
        for i in range(8):
            if i!=3:
                to_remove = energyGrid.itemAtPosition(row,i).widget()
                energyGrid.removeWidget(to_remove)
                to_remove.deleteLater()
            else:
                hbox_remove = energyGrid.itemAtPosition(row,i)
                for j in range(hbox_remove.count()):
                    to_remove = hbox_remove.itemAt(0).widget()
                    hbox_remove.removeWidget(to_remove)
                    to_remove.deleteLater()
                energyGrid.removeItem(hbox_remove)

    def addGraphName(self,graphHbox,index,active):
        """If a graph is to be saved to a file, add in a dialog box for the filename"""

        if graphHbox.count() != 4 and index in active:
            graphFile = QtGui.QLineEdit()
            graphFile.setMinimumSize(graphFile.sizeHint())
            graphBtn = QtGui.QPushButton('Save Graph At..')
            graphBtn.clicked.connect(lambda:self.write_to(graphFile,False,'Graph'))
            graphHbox.file = graphFile
            graphHbox.btn = graphBtn
            graphHbox.addWidget(graphHbox.file)
            graphHbox.addWidget(graphHbox.btn)

        elif graphHbox.count() == 4  and index not in active:
            btnDel = graphHbox.itemAt(3).widget()
            lineDel = graphHbox.itemAt(2).widget()
            graphHbox.removeWidget(btnDel)
            btnDel.deleteLater()
            graphHbox.removeWidget(lineDel)
            lineDel.deleteLater()

    def sharedParams(self,current_sim):
        """ Add parameters which are used in all simulation types"""

        self.cmdMenu.setEnabled(True)
        paramsWidget = QtGui.QWidget()

        stepsLabel = QtGui.QLabel('Simulation Steps:', paramsWidget)
        steps = QtGui.QLineEdit()
        steps.setValidator(QtGui.QIntValidator())
        steps.setAlignment(QtCore.Qt.AlignLeft)
        steps.setText('100')
        steps.setMaximumSize(steps.sizeHint())

        stepsizeLabel = QtGui.QLabel('Step Size:', paramsWidget)
        stepsize = QtGui.QLineEdit()
        stepsize.setValidator(QtGui.QIntValidator())
        stepsize.setAlignment(QtCore.Qt.AlignLeft)
        stepsize.setText('10000')
        stepsize.setMaximumSize(stepsize.sizeHint())

        dfileLabel = QtGui.QLabel('Dumpfile Name:')

        dfile = QtGui.QLineEdit()
        dfile.setAlignment(QtCore.Qt.AlignLeft)
        if current_sim == 'equil':
            dfile.setText(os.getcwd() + '/Equil/equil_dumpfile.lammpstrj')
        elif current_sim == 'protein':
            dfile.setText(os.getcwd() + '/Proteins/prot_dumpfile.lammpstrj')
        elif current_sim == "genomics":
            dfile.setText(os.getcwd() + '/Genome/chrom_dumpfile.lammpstrj')
        else:
            dfile.setText(os.getcwd()+'/dumpfile.lammpstrj')
        dfile.setMinimumSize(dfile.sizeHint())

        dfilebtn = QtGui.QPushButton('Choose Output Location',paramsWidget)
        dfilebtn.setMinimumSize(dfilebtn.sizeHint())
        dfilebtn.clicked.connect(lambda:self.write_to(dfile,False,'Dumpfile'))

        dfilefreqLabel = QtGui.QLabel('Write Freq:')

        dfilefreq = QtGui.QLineEdit()
        dfilefreq.setValidator(QtGui.QIntValidator())
        dfilefreq.setAlignment(QtCore.Qt.AlignLeft)
        dfilefreq.setText('100000')
        dfilefreq.setMaximumSize(dfilefreq.sizeHint())


        dfilecheckLabel = QtGui.QLabel('Write File?')
        dfilecheck = QtGui.QComboBox()
        dfilecheck.addItems(["Yes","No","Maybe"])
        dfilecheck.currentIndexChanged.connect(lambda:self.switch(dfilecheck.currentIndex(),dfile,dfilebtn,dfilefreq))

        infileLabel = QtGui.QLabel('Simulation Input Datafile:')
        infile = QtGui.QLineEdit()
        infile.setAlignment(QtCore.Qt.AlignLeft)
        if current_sim == 'equil':
            infile.setText(os.getcwd() + '/Equil/equil.initial')
        elif current_sim == 'protein':
            infile.setText(os.getcwd() + '/Proteins/10proteins.initial')
        elif current_sim == 'genomics':
            infile.setText(os.getcwd() + '/Genome/chr12.initial')
        else:
            infile.setText(os.getcwd()+'/inputfile.initial')
        infile.setMinimumSize(infile.sizeHint())

        infilebtn = QtGui.QPushButton('Choose Inputfile Location', paramsWidget)
        infilebtn.setMinimumSize(infilebtn.sizeHint())
        infilebtn.clicked.connect(lambda: self.write_to(infile,True,'Inputfile'))

        infileType = QtGui.QComboBox()
        infileType.addItems(['Datafile','Restartfile'])
        infileTypeLabel = QtGui.QLabel('Inputfile Type:')

        libLabel = QtGui.QLabel('LAMMPS Library:')
        libChoice = QtGui.QComboBox()

        # Try searching src folder for libraries. If it doesn't exist list default libs.
        rootdir = os.path.dirname(os.path.realpath(__file__))
        libdirList = os.listdir(rootdir+'/lammps/src/')
        libList = []
        if libdirList != []:
            for i in range(len(libdirList)):
                if libdirList[i][:9] == 'liblammps' and libdirList[i][-3:] == '.so':
                    if libdirList[i][10:-3] == '':
                        pass
                    else:
                        libList.append(libdirList[i][10:-3])
        if libList != []:
            libChoice.addItems(libList)
        else:
            libChoice.addItems(['g++_openmpi','g++_serial','g++_mpich'])
        #libChoice.currentIndexChanged.connect(lambda:self.switch(libChoice.currentIndex(),mpi))
        libChoice.currentIndexChanged.connect(lambda:self.check_serial(libChoice.currentText(),mpi))
        libChoice.setMaximumSize(libChoice.sizeHint())

        mpiLabel = QtGui.QLabel('No. Procs:')
        mpi = QtGui.QLineEdit()
        mpi.setValidator(QtGui.QIntValidator())
        mpi.setAlignment(QtCore.Qt.AlignLeft)

        if libList != []:
            if 'serial' in libList[0]:
                mpi.setText('1')
                mpi.setDisabled(True)
            else:
                mpi.setText('4')
            mpi.setMaxLength(2)
        mpi.setMaximumSize(mpi.sizeHint())

        angLabel = QtGui.QLabel('DNA Angle Coeff:')
        ang = QtGui.QLineEdit()
        ang.setValidator(QtGui.QDoubleValidator())
        ang.setAlignment(QtCore.Qt.AlignLeft)
        ang.setMaximumSize(ang.sizeHint())
        ang.setText('20.0')
        
        softCheckBox = QtGui.QCheckBox('Do pre-equilibration with soft \n potential, harmonic bonds.')
        softCheckBox.setChecked(False)
        softLabel = QtGui.QLabel('No. Steps:')
        softSteps = QtGui.QLineEdit()
        softSteps.setValidator(QtGui.QIntValidator())
        softSteps.setAlignment(QtCore.Qt.AlignLeft)
        softSteps.setMaximumSize(softSteps.sizeHint())
        softSteps.setDisabled(True)
        softSteps.setText('0')

        softAng = QtGui.QLabel('Initial Angle Coeff:')
        softAngField = QtGui.QLineEdit()
        softAngField.setText('20.0')
        softAngField.setValidator(QtGui.QDoubleValidator())
        softAngField.setDisabled(True)
        softAngField.setMinimumSize(softAngField.sizeHint())

        softCheckBox.clicked.connect(lambda: self.switch(abs(int(softCheckBox.isChecked()) - 1), softSteps, softAngField))

        paramsWidget.layout = QtGui.QGridLayout()

        paramsWidget.layout.addWidget(stepsLabel, 0, 0)
        paramsWidget.layout.addWidget(steps, 0, 1)
        paramsWidget.layout.addWidget(stepsizeLabel, 0, 3)
        paramsWidget.layout.addWidget(stepsize, 0, 4)

        paramsWidget.layout.addWidget(infileLabel, 1, 0)
        paramsWidget.layout.addWidget(infile, 1, 1)
        paramsWidget.layout.addWidget(infilebtn, 1, 2)
        paramsWidget.layout.addWidget(infileTypeLabel, 1, 3)
        paramsWidget.layout.addWidget(infileType, 1, 4)

        paramsWidget.layout.addWidget(dfileLabel, 2, 0)
        paramsWidget.layout.addWidget(dfile, 2, 1)
        paramsWidget.layout.addWidget(dfilebtn, 2, 2)
        paramsWidget.layout.addWidget(dfilefreqLabel, 2, 3)
        paramsWidget.layout.addWidget(dfilefreq, 2, 4)
        paramsWidget.layout.addWidget(dfilecheckLabel, 2, 5)
        paramsWidget.layout.addWidget(dfilecheck, 2, 6)

        paramsWidget.layout.addWidget(libLabel, 3, 0)
        paramsWidget.layout.addWidget(libChoice, 3, 1)
        paramsWidget.layout.addWidget(mpiLabel, 3, 3)
        paramsWidget.layout.addWidget(mpi, 3, 4)

        paramsWidget.layout.addWidget(angLabel, 4, 0)
        paramsWidget.layout.addWidget(ang, 4, 1)
        
        paramsWidget.layout.addWidget(softCheckBox, 5, 0)
        paramsWidget.layout.addWidget(softLabel, 5, 1)
        paramsWidget.layout.addWidget(softSteps, 5, 2)
        paramsWidget.layout.addWidget(softAng, 5, 3)
        paramsWidget.layout.addWidget(softAngField, 5, 4)

        paramsWidget.setLayout(paramsWidget.layout)


        return paramsWidget

    def switch(self,index,*args):
        """Switch things off if on, on if off"""
        for arg in args:
            if index == 1:
                arg.setDisabled(True)
            else:
                arg.setEnabled(True)

    def check_serial(self,currentLibrary,mpi):
        """Check if a library should only be run using one processor.
        This could be extended to actually performing the check by launching a
        process."""
        if 'serial' in currentLibrary:
            mpi.setText('1')
            mpi.setDisabled(True)
        else:
            mpi.setText('4')
            mpi.setEnabled(True)

    def write_to(self,store_to,open,filestr):
        """Open a file selection dialog and store the result in store_to, which should be of type QtGui.LineEdit"""
        if open == False:
            fname = QtGui.QFileDialog.getSaveFileName(self, 'Write {0} At..'.format(filestr),os.getcwd())
        elif open == True:
            fname = QtGui.QFileDialog.getOpenFileName(self, 'Use {0} At..'.format(filestr),os.getcwd())
        store_to.setText(fname)

    def make_inputfile_unused(self):
        tempfile = open(os.getcwd() + '/temp.inputbuild', 'w')
        globalData = self.getglobalsimArgs(self.inputWidget.layout.itemAt(0))
        tempfile.write(
            'all boundaries={},{},{},{},{},{}\n'.format(globalData['Xmin'], globalData['Xmax'], globalData['Ymin'],
                                                        globalData['Ymax'], globalData['Zmin'], globalData['Zmax']))
        for i in range(1,self.inputWidget.layout.count()):
            widget = self.inputWidget.layout.itemAt(i).widget()
            if widget.layout.itemAt(0).itemAt(0).itemAt(0).widget().text() == 'DNA':
                dnaArgs = self.getDNAWidgetargs(widget)


            elif widget.layout.itemAt(0).itemAt(0).itemAt(0).widget().text() == 'Proteins':
                protArgs = self.getProtWidgetargs(widget)

    def make_inputfile(self):
        # Create temporary inputfile
        tempfile = open(os.getcwd() + '/temp.inputbuild','w')

        # global conditions
        sim_box = [float(self.inputWidget.layout.itemAt(0).itemAt(i).widget().text()) for i in [3,5,7,9,11,13]]
        tempfile.write('all boundaries={},{},{},{},{},{}\n'.format(sim_box[0],sim_box[1],sim_box[2],sim_box[3],sim_box[4],sim_box[5]))
        for i in range(1,self.inputWidget.layout.count()):
            #print self.inputWidget.layout.itemAt(i).widget().layout.itemAt(0),self.inputWidget.layout.itemAt(i).widget().layout.itemAt(0).itemAt(0).count(),self.inputWidget.layout.itemAt(i).widget().layout.count()
            hbox = self.inputWidget.layout.itemAt(i).widget().layout.itemAt(0).itemAt(0)
            if hbox.itemAt(0).widget().text() == 'DNA':
                # Required Parameters
                tempfile.write('name={} '.format(i))
                if hbox.itemAt(3).widget().currentText() == 'Loop':
                    tempfile.write('style={} '.format('loop'))
                elif hbox.itemAt(3).widget().currentText() == 'Linear':
                    tempfile.write('style={} '.format('linear'))
                elif hbox.itemAt(3).widget().currentText() == 'Supercoil-Linear':
                    tempfile.write('style={} '.format('twistedlinear'))
                elif hbox.itemAt(3).widget().currentText() == 'Supercoil-Loop':
                    tempfile.write('style={} '.format('twistedloop'))
                tempfile.write('atom_no={} '.format(hbox.itemAt(5).widget().text()))
                atom_no = int(hbox.itemAt(5).widget().text())
                tempfile.write('atom_type={} '.format(hbox.itemAt(7).widget().value()))
                tempfile.write('atom_diameter={} '.format(hbox.itemAt(9).widget().text()))
                if hbox.itemAt(10).widget().isChecked():
                    try:
                        # Test if the next section 1) Exists 2) is a DNA section
                        nexthbox = self.inputWidget.layout.itemAt(i+1).widget().layout.itemAt(0).itemAt(0)
                        if nexthbox.itemAt(0).widget().text() == 'DNA':
                            print int(nexthbox.itemAt(5).widget().text())
                            if int(nexthbox.itemAt(5).widget().text()) == 1:
                                try:
                                    nexthbox2 = self.inputWidget.layout.itemAt(i+2).widget().layout.itemAt(0).itemAt(0)
                                    if nexthbox2.itemAt(0).widget().text() == 'DNA':
                                        tempfile.write('connect=True ')
                                        print 'path1'
                                    else:
                                        tempfile.write('connect=1 ')
                                        print 'path2'
                                except:
                                    tempfile.write('connect=1 ')
                                    print 'path3'
                            else:
                                tempfile.write('connect=True ')
                                print 'path4'
                        else:
                            tempfile.write('connect=False ')
                    except:
                        tempfile.write('connect=False ')
                else:
                    tempfile.write('connect=False ')
                # If the DNA has a start point
                if hbox.itemAt(11).widget().isVisible():
                    tempfile.write('start_point={},{},{} '.format(hbox.itemAt(12).widget().text(),
                                                                  hbox.itemAt(13).widget().text(),
                                                                  hbox.itemAt(14).widget().text()))
                if hbox.itemAt(16).widget().isVisible():
                    tempfile.write('twists={} '.format(hbox.itemAt(16).widget().text()))
                    
                if hbox.itemAt(17).widget().isVisible() and hbox.itemAt(18).widget().isVisible():
                    tempfile.write('supercoil_boundary={},{} '.format(int(hbox.itemAt(17).widget().isChecked()), int(hbox.itemAt(18).widget().isChecked())))
                
                for j in range(1,self.inputWidget.layout.itemAt(i).widget().layout.count()):
                    extrahbox = self.inputWidget.layout.itemAt(i).widget().layout.itemAt(j)
                    if extrahbox.itemAt(0).widget().text() == 'Set Section Size:' and extrahbox.itemAt(0).widget().isVisible():
                        tempfile.write('section_length={} '.format(extrahbox.itemAt(1).widget().text()))
                    else:
                        for sec_length_test in range(5,0,-1):
                            if atom_no%sec_length_test == 0:
                                tempfile.write('section_length={} '.format(sec_length_test))
                                break
                    if extrahbox.itemAt(0).widget().text() == 'Read Atom Types From File:' and extrahbox.itemAt(0).widget().isVisible():
                        tempfile.write('switch_data={} '.format(extrahbox.itemAt(1).widget().text()))
                    if extrahbox.itemAt(0).widget().text() == 'Boundary Type:' and extrahbox.itemAt(0).widget().isVisible():
                        if extrahbox.itemAt(1).widget().currentText() == 'Rectangular':
                            tempfile.write('boundary_style=rectangular ')
                            dna_box = [extrahbox.itemAt(k).widget().text() for k in [3,5,7,9,11,13]]
                            tempfile.write('boundaries={},{},{},{},{},{} '.format(dna_box[0],dna_box[1],dna_box[2],dna_box[3],dna_box[4],dna_box[5]))
                        elif extrahbox.itemAt(1).widget().currentText() == 'Cylindrical':
                            dna_cyl = [extrahbox.itemAt(k).widget().text() for k in [3, 5, 7]]
                            tempfile.write('boundaries={},{},{} '.format(dna_cyl[0], dna_cyl[1], dna_cyl[2]))
            elif hbox.itemAt(0).widget().text() == 'Proteins':
                tempfile.write('name={} '.format(i))
                tempfile.write('style={} '.format('prot'))
                tempfile.write('atom_no={} '.format(hbox.itemAt(3).widget().text()))
                tempfile.write('atom_type={} '.format(hbox.itemAt(5).widget().value()))
                tempfile.write('atom_diameter={} '.format(hbox.itemAt(7).widget().text()))
                for j in range(1,self.inputWidget.layout.itemAt(i).widget().layout.count()):
                    extrahbox = self.inputWidget.layout.itemAt(i).widget().layout.itemAt(j)
                    if extrahbox.itemAt(0).widget().text() == 'Read Atom Types From File:' and extrahbox.itemAt(0).widget().isVisible():
                        tempfile.write('switch_data={} '.format(extrahbox.itemAt(1).widget().text()))
                    if extrahbox.itemAt(0).widget().text() == 'Boundary Type:' and extrahbox.itemAt(0).widget().isVisible():
                        if extrahbox.itemAt(1).widget().currentText() == 'Rectangular':
                            tempfile.write('boundary_style=rectangular ')
                            dna_box = [extrahbox.itemAt(k).widget().text() for k in [3,5,7,9,11,13]]
                            tempfile.write('boundaries={},{},{},{},{},{} '.format(dna_box[0],dna_box[1],dna_box[2],dna_box[3],dna_box[4],dna_box[5]))
                        elif extrahbox.itemAt(1).widget().currentText() == 'Cylindrical':
                            dna_cyl = [extrahbox.itemAt(k).widget().text() for k in [3, 5, 7]]
                            tempfile.write('boundaries={},{},{} '.format(dna_cyl[0], dna_cyl[1], dna_cyl[2]))
            tempfile.write('\n')
            tempfile.flush()
        tempfile.close()
        """hbox = self.inputWidget.layout.itemAt(i).widget().layout.itemAt(0).itemAt(0)
        if hbox.itemAt(0).widget().text() == 'DNA':
            dnaargs = self.getDNAWidgetargs(self.inputWidget.layout.itemAt(i).widget())
            print dnaargs
        elif hbox.itemAt(0).widget().text() == 'Proteins':
            protargs = self.getProtWidgetargs(self.inputWidget.layout.itemAt(i).widget())
            print protargs"""
        outfile_name = self.inputWidget.layout.itemAt(0).itemAt(17).widget().text()
        copies = int(self.inputWidget.layout.itemAt(0).itemAt(19).widget().value())

        outfile_strings = outfile_name.split('.')
        for i in range(1,copies+1):
            outfile_tokens = list(outfile_strings)
            if copies != 1:
                if len(outfile_tokens) == 1:
                    outfile_name = outfile_tokens[0]+str(i)
                else:
                    outfile_tokens[-2] += str(i)
                    outfile_name = ''
                    print (outfile_name,outfile_tokens,i)
                    for i,string in enumerate(outfile_tokens):
                        if i == len(outfile_tokens) - 1:
                            outfile_name+=string
                        else:
                            outfile_name += string+'.'

            subprocess.check_call(['python',os.path.dirname(os.path.realpath(__file__))+'/Input-git/Dnamain.py','-in',os.getcwd()+'/temp.inputbuild', '-out',outfile_name])

def main():

    app = QtGui.QApplication(sys.argv)
    interface = Interface()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()

